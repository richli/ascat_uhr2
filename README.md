# ASCAT UHR Reimplementation

This contains an experimental reimplementation of ultra-high resolution (UHR)
wind retrieval for the ASCAT scatterometer.

## Previous work

ASCAT UHR was previously implemented and described in a [journal
paper](https://doi.org/10.1109/TGRS.2016.2570245). It reads as input ASCAT L1B
files (containing SZF or full-resolution data) and a corresponding L2 file along
with a region of interest to process. The relevant L1B data are extracted and
(partially) reconstructed on a high-resolution swath grid. The high-resolution
backscatter data is processed with the
[AWDP](https://nwpsaf.eu/site/software/scatterometer/awdp/) to obtain UHR winds.
A simple ambiguity selection is run to maintain spatial consistency.

Although the results were good, its implementation and design had a few
short-comings:

- The L1B data are (partially) reconstructed on a high-resolution swath grid.
  This swath grid is meant to be aligned with the lower resolution WVC grid used
  for the operational L2 wind product. It is a non-trivial challenge to figure
  out how to map the SZF L1B measurements onto the UHR grid, and how the UHR
  grid maps to the L2 grid. There is a great deal of old code written for old
  scatterometers that is used to handle the geometry. We were able to get this
  code working for ASCAT, but it's always been troublesome (unexplained
  segfaults) and very inefficient to run.

- The reconstructed data are passed to AWDP. AWDP performs an empirical
  pre-processing step to calibrate the sigma0 values. This is implemented using
  lookup tables that depend on the L1B processor version. Because the
  calibration was performed using the L2 data, it's not really known if this
  same calibration step should be applied to UHR data.

- AWDP applies some other steps, such as normalizing the MLE term using a LUT
  and model wind speeds. The previous UHR implementation passed in the L2 winds
  as "model winds". I am unsure if this is appropriate since the LUT was not
  meant to be used this way. Besides, the MLE terms are unused in the UHR
  product.

- Ambiguity selection is very simple and could certainly be improved.

- No quality control (QC) is performed or rain flagging or sea ice flagging.

- There is a lot of old and inefficient code inherited from previous work. It
  often triggered a myriad of compiler warnings and would suffer from
  (seemingly) unpredictable memory violations.
  
Thus I would like to try a fresh reimplementation of ASCAT UHR.

## Desired changes

Here are some of the major items to change. Some of these may be experimental
and I may change my mind about them later.

- Use the HDF5 version of the ASCAT SZF L1B inputs. This obviates the need to
  mess with decoding the "native format" (aka PFS format aka EPS format) L1B
  files.

- Scrap the swath-oriented grid. This has always been a troublesome spot and
  there is no real need to exactly align the UHR grid with the L2 WVC grid.
  Instead, stick with an Earth-oriented grid. Probably a good default would be
  the [EASE-2](https://nsidc.org/data/ease/ease_grid2.html) global grid. It's a
  cylindrical equal-area projection. Geoprojection can be handled with the
  [PROJ.4](http://proj4.org/) library, obviating the need for the geometry
  routines from the [MERS SIR library](http://www.mers.byu.edu/SIR.html).

- The reconstruction of sigma0 and other parameters can probably be more
  efficiently computed. This is something to experiment with. Since AVE is
  basically a matrix/vector dot product using a very sparse matrix, perhaps BLAS
  or the Intel MKL could be used?

- Move away from AWDP. I would like to try wind retrieval on my own without
  requiring hooking into bits from AWDP. I could use the CMOD7 GMF from KNMI, or
  perhaps the C-2015 GMF from RSS.
  
- Try a different ambiguity selection approach. AWDP uses 2DVAR, perhaps the
  same algorithm (not the same implementation) may be used?

- Add some QC checks, notably including rain flagging. The rain information
  may have to come from an ancillary source.

- The LCR implementation was a little slow. I'd like to see if this can be
  improved.

## Design

Here is the general overview:

1) First, define an Earth-oriented grid at high resolution (by default, EASE-2
cylindrical at a grid spacing of 3.125 km, but it should be configurable) and a
lat/lon region of interest.

2) For an input ASCAT L1B file (in HDF5 format), scan through all the
measurements and extract those falling within the region. Geoproject each
measurement to find out which pixel on the map the center falls into. Also, the
measurement spatial response function (SRF) is sampled on a neighborhood around
the measurement center. The measurement, its geoprojection, and the sampled SRF
are all stored to an HDF5-format output file (perhaps named
`region-rev.prep.h5`). Land masking and computing the land contribution ratio
(LCR) is also performed.

3) The prepared measurements are read and (partial) reconstruction is performed
on each parameter (sigma0, incidence angle, azimuth angle, and measurement time)
for each of the three look directions (fore, mid, and aft) separately. The AVE
algorithm is used. The reconstructed data on the UHR grid is written to a netCDF
file (perhaps named `region-rev.avewr.nc`).

4) The reconstructed data is read and wind retrieval is performed on each UHR
pixel independently. The results are saved to a netCDF output file (perhaps
named `region-rev.uhr.nc`).

5) The UHR file is read and final QC checks (e.g., ancillary rain flagging) and
ambiguity selection is conducted. The netCDF file is updated with the results.
Also, for convenience some redundant variables are stored containing the
(ambiguity-selected) wind speeds and directions and the zonal and meridional
components.

## Implementation

This will first be prototyped in Python (with maybe some old bits in
C) to ensure the algorithms are sound. Once this is finished, I will
reimplement the code in Rust.

### Requirements

- [Python](https://www.python.org) 3.6 or higher
- Python modules
  - [numpy](http://www.numpy.org) and [scipy](https://www.scipy.org)
  - [h5py](http://www.h5py.org)
  - [netcdf4-python](https://unidata.github.io/netcdf4-python/)
  - [pyproj](https://pyproj4.github.io/pyproj)
  - [python-dateutil](https://dateutil.readthedocs.io/en/stable/)
  - [tqdm](https://pypi.python.org/pypi/tqdm)
  - [cffi](https://cffi.readthedocs.io/en/latest/)
- C libraries required for above Python modules
  - [HDF5](https://www.hdfgroup.org/solutions/hdf5/)
  - [netCDF](https://www.unidata.ucar.edu/software/netcdf/)
  - [PROJ](http://proj.org/)
- [GMT](http://gmt.soest.hawaii.edu/)
- [GSL](https://www.gnu.org/software/gsl/)

### Spatial Response Function

The ASCAT measurement spatial response function (SRF) is evaluated
using C code available from the [MERS
Lab](http://www.scp.byu.edu/software/ASCAT/). It is not the complete
evaluation but a good and fast approximation to the SRF. It depends on
the GNU Scientific Library to run. To access it from Python, the C
code is compiled into a shared library and then loaded from Python
using the `cffi` module.

### Building and running

For ease, everything is built and packaged into a container:

```bash
# Or, buildah bud
docker build -t richli/ascat_uhr2 .

# Or, podman run
docker run richli/ascat_uhr2 prepare_uhr.py ...
```

See [driver.sh](driver.sh) for an example of running UHR on a region.

## Inputs

The ASCAT SZF L1B input files are in HDF5 format, with each file containing an
orbit of data. The data may be [ordered
here](http://eoportal.eumetsat.int/discovery/Start/DirectSearch/Extended.do?f(r0)=EO:EUM:DAT:METOP:ASCSZF1B)
from the EUMETSAT Data Centre.

The model winds currently used are from
[MERRA-2](https://gmao.gsfc.nasa.gov/reanalysis/MERRA-2/), the
[tavg1_2d_ocn_Nx](https://doi.org/10.5067/Y67YQ1L3ZZ4R) product.

## Changelog

### 0.2.0 - 2018-04-28

- The L1B quality flags are checked and bad measurements are discarded
  from processing
- GMT is used to pre-compute a land mask and a distance-to-land map
- The land mask is optionally included in the AVEWR and UHR files, and
  wind retrieval skips over the land data
- The correct ASCAT SRF is used, from an approximation developed by
  the MERS Lab

### 0.1.0 - 2017-10-22

The initial rough implementation is ready. From the input L1B file and model
wind file, the corresponding AVEWR and UHR files are generated and a plot of the
UHR wind speeds is generated. It is inefficient and only a placeholder
measurement SRF is used, but it works from end to end.

## Future work

### Near-term

- [ ] Sometimes more than 4 ambiguities are found. Why? Examine
      retrieval a bit more closely.
- [ ] Port `retrieve_winds.py` and `prepare_uhr.py` (including the SRF
      computation) to Rust

### Long-term

- [ ] Profile everything to identify bottlenecks. Rewrite some or all
      in Cython, C, or Rust. Perhaps the Intel MKL may be helpful for
      BLAS and vector math support. Probably the first thing to start
      with is `retrieve_winds.py` since that takes the longest to
      compute.
- [ ] `prepare_uhr.py`
  - [ ] For ocean measurements, compute each measurement's land
        contribution ratio (use the pre-computed distance-to-land map
        from GMT)
- [ ] `process_avewr.py`
  - [ ] Maybe a more efficient AVE computation using the Intel MKL?
        But it's already quite fast.
  - [ ] What LCR rejection criteria should I use?
  - [ ] Use the measurement noise model to figure out the (scaled) AVE
        variance for each pixel. This error term could be passed to
        wind retrieval.
- [ ] `retrieve_winds.py`
  - [ ] Sometimes more than 4 ambiguities are found. Why?
  - [ ] Examine the optimization problem more closely. Maybe a
        different approach would be more efficient to compute? I need
        to make some contour plots of the objective function vs (spd,
        dir) as a polar plot. How does it change when I add all three
        looks together? Since it's a multi-modal optimization without
        necessarily a global optimum, try evolutionary multimodal
        methods, like particle swarm optimization.
  - [ ] If I have some error terms in the AVEWR data, I can weight the
        MLE process.
- [ ] `post_process.py`
  - [ ] Try 2DVAR for ambiguity selection?
  - [ ] What QC checks should I do?
  - [ ] Rain flagging (ancillary data)
  - [ ] What about a rain model, using Nie and Owen's work?
- [ ] `plot_uhr.py`
  - [ ] Support AVEWR inputs
  - [ ] Plot more quantities (model winds, wind components,
        streamlines, etc)
  - [ ] Finish plotting with GMT?
- [ ] Switch from netCDF4 files to vanilla HDF5?

## License

This work is licensed under the [MIT license](https://en.wikipedia.org/wiki/MIT_License).
