#!/usr/bin/env bash
set -euo pipefail

# This is a test driver to run the whole UHR processing for a region.
# There's a good ASCAT-A pass over Hurricane Harvey in rev 56266 (on
# 2017-08-23).

# Here's the L2 wind file, for comparison:
# 
# https://podaac-opendap.jpl.nasa.gov/opendap/allData/ascat/preview/L2/metop_a/coastal_opt/2017/235/ascat_20170823_033600_metopa_56266_eps_o_coa_2401_ovw.l2.nc.gz.html

# Here's the MERRA-2 data for the modeled winds:
#
# https://goldsmr4.gesdisc.eosdis.nasa.gov/data/MERRA2/M2T1NXOCN.5.12.4/2017/08/MERRA2_400.tavg1_2d_ocn_Nx.20170823.nc4

# Anaconda (or miniconda) is used to ensure Python has the dependant
# modules correct and up-to-date: https://conda.io/miniconda.html

region_lat_min=18
region_lat_max=30
region_lon_min=-93
region_lon_max=-88

l1b_file="$HOME/uhr/data/ASCA_SZF_1B_M02_20170823022400Z_20170823040559Z_N_O_20170823040603Z.h5"
model_file="$HOME/uhr/data/MERRA2_400.tavg1_2d_ocn_Nx.20170823.nc4"
land_mask="$HOME/uhr/data/land_mask.nc"
land_dist_mask="$HOME/uhr/data/land_dist_mask.nc"

prep_file="$HOME/uhr/data/harvey-56266.prep.h5"
avewr_file="$HOME/uhr/data/harvey-56266.avewr.nc"
uhr_file="$HOME/uhr/data/harvey-56266.uhr.nc"
plot_dir="$HOME/uhr/data"

conda="$HOME/miniconda3/bin/conda"
python="$HOME/miniconda3/envs/uhr/bin/python3.7"

# region_lat_min=27
# region_lat_max=30
# region_lon_min=-97
# region_lon_max=-89
# 
# l1b_file="$HOME/ASCA_SZF_1B_M02_20160528013600Z_20160528031759Z_N_O_20160528031832Z.h5"
# prep_file="$HOME/GoM-49844.prep.h5"
# avewr_file="$HOME/GoM-49844.avewr.nc"
# uhr_file="$HOME/GoM-49844.uhr.nc"

# region_lat_min=27
# region_lat_max=30
# region_lon_min=-97
# region_lon_max=-89
# 
# l1b_file="$HOME/ASCA_SZF_1B_M01_20160528023000Z_20160528041159Z_N_O_20160528031904Z.h5"
# prep_file="$HOME/GoM-19158.prep.h5"
# avewr_file="$HOME/GoM-19158.avewr.nc"
# uhr_file="$HOME/GoM-19158.uhr.nc"

# Set up the Python environment
env() {
	if [[ $($conda info -e | grep --quiet --invert-match uhr) ]]; then
		$conda create -n uhr python=3.7 numpy scipy h5py netcdf4 pyproj python-dateutil tqdm matplotlib cartopy
	else
		$conda install -n uhr python=3.7 numpy scipy h5py netcdf4 pyproj python-dateutil tqdm matplotlib cartopy
	fi
}

# Generate auxiliary support files
aux() {
	# 3 km landmask on a lat/lon grid. 0 is ocean, 1 is land. This is
	# fast. I have to extend the region slightly to ensure nothing
	# gets cut off.
	gmt grdlandmask -Vl -G"$land_mask" \
		-R"$((region_lon_min-1))"/"$((region_lon_max+1))"/"$((region_lat_min-1))"/"$((region_lat_max+1))" \
		-I3k -Da -A0/0/1

	# Also the distance-to-land map. This is much longer to compute,
	# so a courser grid is used instead. Also the "low-resolution"
	# GSHHG dataset is purposefully used.
	gmt grdmath -Vl -Dl -A0/0/1 -I0.125 \
		-R"$region_lon_min"/"$region_lon_max"/"$region_lat_min"/"$region_lat_max" \
		LDISTG = "$land_dist_mask"
}

# Scan through ASCAT measurements to prepare for UHR processing
prep() {
	$python ./uhr/prepare_uhr.py "$l1b_file" "$prep_file" \
		   --lat-min "$region_lat_min" --lat-max "$region_lat_max" \
		   --lon-min "$region_lon_min" --lon-max "$region_lon_max" \
		   --geoproj ease2-glob-3.125
}

# Create the AVEWR data from the prepared ASCAT measurements
avewr() {
	$python ./uhr/process_avewr.py "$prep_file" "$avewr_file" \
			--land-map "$land_mask"
}

# Create the UHR data from the AVEWR data
retrieve() {
	$python ./uhr/retrieve_winds.py "$avewr_file" "$uhr_file"
}

# Post-process the UHR data
postprocess() {
	$python ./uhr/post_process.py "$uhr_file" "$model_file"
}

# Plot the UHR data
plot() {
	$python ./uhr/plot_uhr.py "$uhr_file" --out-dir "$plot_dir" --plt-fmt png --backend cartopy
}

env
aux
prep
avewr
retrieve
postprocess
plot
