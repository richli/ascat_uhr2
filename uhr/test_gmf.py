"""Tests for the gmf module.

These require pytest and pytest-benchmark.

"""

import numpy as np

from . import gmf


def test_gmf_slow_and_fast_few() -> None:
    """Ensure the 'slow' and 'fast' GMFs are equal for a few values."""
    N = 20
    wspds = np.linspace(0, 20, N)
    wdirs = np.linspace(-180, 180, N)
    incs = np.full(N, 30)
    azis = np.full(N, 30)

    cmod5n = gmf.cmod5("cmod5n")

    sig0_slow = cmod5n.call(wspds, wdirs, incs, azis)
    sig0_fast = cmod5n.call_fast(wspds, wdirs, incs[0], azis[0])

    assert np.allclose(sig0_slow, sig0_fast)


def test_slow_gmf(benchmark):
    N = 5000
    wspds = np.linspace(0, 20, N)
    wdirs = np.linspace(-180, 180, N)
    incs = np.full(N, 30)
    azis = np.full(N, 30)
    cmod5n = gmf.cmod5("cmod5n")
    benchmark(cmod5n.call, wspds, wdirs, incs, azis)


def test_fast_gmf(benchmark):
    N = 5000
    wspds = np.linspace(0, 20, N)
    wdirs = np.linspace(-180, 180, N)
    inc = np.float64(30)
    azi = np.float64(30)
    cmod5n = gmf.cmod5("cmod5n")
    benchmark(cmod5n.call_fast, wspds, wdirs, inc, azi)
