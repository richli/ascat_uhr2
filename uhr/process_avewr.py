#!/usr/bin/env python3
"""Create AVEWR data for ASCAT UHR processing.

ASCAT measurements and their (sampled) SRF values are previously
extracted and mapped onto a high-resolution UHR grid using
prepare_uhr.py. This script performs a limited reconstruction using
the AVE (weighted average) algorithm over the various parameters:
sigma0, incidence angle, etc.

For wind retrieval purposes, the AVE data is created over each of the
three look directions (fore, mid, aft) separately. The AVEWR data is
stored in a netCDF file for later processing.

"""

import argparse
from pathlib import Path
from datetime import datetime
import logging
import typing

import numpy as np
from numpy import ma
import scipy.sparse as sps
from scipy.interpolate import RegularGridInterpolator
import h5py
import netCDF4
import pyproj
from dateutil import tz

from version import __version__

__author__ = "Richard Lindsley"


PrepData = typing.NewType("PrepData", typing.Dict[str, typing.Any])
AvewrData = typing.NewType("AvewrData", typing.Dict[str, typing.Any])


def main() -> None:
    """Parse arguments and create AVEWR data."""
    parser = argparse.ArgumentParser(description="Create AVEWR data")
    parser.add_argument(
        "prep_file", type=Path, help="Input UHR preparation file, HDF5 format"
    )
    parser.add_argument(
        "avewr_file", type=Path, help="Output AVEWR file, netCDF format"
    )

    parser.add_argument(
        "--land-map", type=Path, help="Path to pre-computed land map (netCDF)"
    )

    verb_group = parser.add_mutually_exclusive_group()
    verb_group.add_argument(
        "--quiet", "-q", action="store_true", help="Reduce logging output"
    )
    verb_group.add_argument(
        "--verbose", "-v", action="store_true", help="Increase logging output"
    )

    parser.add_argument(
        "--version",
        action="version",
        version=f"%(prog)s version {__version__}",
    )
    args = parser.parse_args()

    # Set up logging
    if args.quiet:
        log_level = logging.WARNING
    elif args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    # log_fmt = "{asctime} {levelname}: {module}.{funcName} {message}"
    log_fmt = "{levelname}: {message}"
    log_datefmt = "%Y-%m-%d %H:%M:%S%z"
    logging.basicConfig(
        style="{", level=log_level, format=log_fmt, datefmt=log_datefmt
    )
    logger = logging.getLogger(__name__)

    # Read preparation file
    logger.info(f"Loading data from preparation file: {args.prep_file}")
    prep_data = load_prep_data(args.prep_file)

    # Geoproject the land map. It's previously created using GMT and
    # is in netCDF format. Well, it's a reverse projection because the
    # land map is in terms of lat/lon, not map coordinates. So for
    # each output cell on the map we lookup the nearest land map cell
    # and store whether it's land or ocean.
    if args.land_map is not None:
        logger.info(f"Reading and geoprojecting land map: {args.land_map}")
        land_mask = read_land_map(
            args.land_map,
            prep_data["map_crs"]["proj4text"],
            prep_data["map_bins"],
        )
    else:
        land_mask = None

    # Compute the AVEWR images
    logger.info("Reconstructing AVEWR data")
    avewr_data = compute_avewr(prep_data)

    # Write the output
    logger.info(f"Writing to avewr file: {args.avewr_file}")
    write_output(args.avewr_file, args.prep_file, avewr_data, land_mask)


def load_prep_data(prep_file: Path) -> PrepData:
    """Load the prepared data from the HDF5 file.

    The measurement data is segmented by look direction (fore, mid,
    aft) and shaped into 1D vectors (each vector is a separate
    parameter, such as sig0 or inc).

    The sampled SRF is used to construct a sparse 2D matrix. Each row
    of the matrix corresponds to a measurement and each column
    corresponds to a pixel in the output image. (The output image
    coordinates are vectorized to 1D indices). So for N measurements
    and M pixels, the SRF matrix is shaped as (N, M) and all the
    measurement vectors are (N, ).

    The vectors are (dense) numpy arrays but the SRF matrices (one for
    each look) are CSR scipy sparse matrices. All outputs are returned
    in a dict.

    """
    logger = logging.getLogger(__name__)
    logger.debug("Loading file data")
    with h5py.File(prep_file, "r") as f:
        # Just read everything from the file into memory
        all_meas = {
            "sig0": f["/meas/sig0"][:],
            "inc": f["/meas/inc"][:],
            "azi": f["/meas/azi"][:],
            "time": f["/meas/time"][:],
            "beam": f["/meas/beam"][:],
            "srf_start": f["/meas/srf_start"][:],
            "srf_stop": f["/meas/srf_stop"][:],
        }

        all_srf = {
            # 'bin_row': f['/srf/bin_row'][:],
            # 'bin_col': f['/srf/bin_col'][:],
            "bin_flat": f["/srf/bin_flat"][:],
            "meas_index": f["/srf/meas_index"][:],
            "val": f["/srf/val"][:],
        }

        bin_centers = {
            "x": f["/map/x"][:],
            "y": f["/map/y"][:],
        }

        map_crs = {key: val for (key, val) in f["/map/crs"].attrs.items()}
        region = {
            key: f.attrs[key]
            for key in (
                "geospatial_lat_min",
                "geospatial_lat_max",
                "geospatial_lon_max",
                "geospatial_lon_min",
            )
        }
        instrument = f.attrs["instrument"]
        platform = f.attrs["platform"]
        time_start = f.attrs["time_coverage_start"]
        time_end = f.attrs["time_coverage_end"]

    logger.debug("Post-processing file data")

    # Segment the measurements by look
    mask_looks = {
        "fore": (all_meas["beam"] == 1) | (all_meas["beam"] == 4),
        "mid": (all_meas["beam"] == 2) | (all_meas["beam"] == 5),
        "aft": (all_meas["beam"] == 3) | (all_meas["beam"] == 6),
    }

    prep_data = PrepData(
        {
            "map_crs": map_crs,
            "map_bins": bin_centers,
            "region": region,
            "instrument": instrument,
            "platform": platform,
            "time_coverage_start": time_start,
            "time_coverage_end": time_end,
        }
    )
    for look in ("fore", "mid", "aft"):
        mask = mask_looks[look]
        prep_data[f"meas_{look}"] = {
            key: all_meas[key][mask] for key in ("sig0", "inc", "azi", "time")
        }

    # Construct the global SRF matrix, then subset it by look. This is
    # just extracting the rows of interest for each look. Note that I
    # cannot use a boolean mask, but a list of indices is okay.
    tot_meas = len(all_meas["beam"])
    nx, ny = len(bin_centers["x"]), len(bin_centers["y"])
    tot_pix = nx * ny
    srf_mat = sps.coo_matrix(
        (all_srf["val"], (all_srf["meas_index"], all_srf["bin_flat"])),
        shape=(tot_meas, tot_pix),
    ).tocsr()

    for look in ("fore", "mid", "aft"):
        mask = mask_looks[look]
        row_inds = np.where(mask)[0]
        srf_mat_look = srf_mat[row_inds, :]
        prep_data[f"srf_{look}"] = srf_mat_look

        mat_size = srf_mat_look.shape[0] * srf_mat_look.shape[1]
        density = srf_mat_look.getnnz() / mat_size
        logger.info(
            (
                f"SRF matrix for {look}: "
                f"{srf_mat_look.shape}, "
                f"{density:0.4%} dense"
            )
        )

    return prep_data


def compute_avewr(prep_data: PrepData) -> AvewrData:
    """Compute AVE for each look direction separately."""
    out_data = AvewrData(
        {
            key: prep_data[key]
            for key in (
                "map_crs",
                "map_bins",
                "region",
                "instrument",
                "platform",
                "time_coverage_start",
                "time_coverage_end",
            )
        }
    )

    # AVE is very simple. For pixel index j, measurement index i,
    # values z_i, and SRF matrix h_ij, the weighted average is:
    #
    # a_j = sum_i (z_i * h_ij) / sum_i (h_ij)
    #
    # This can be expressed as a dot product between the SRF matrix H
    # and the vector of meaurements z:
    #
    # a = (H @ z) / (H @ 1)
    #
    # The SRF matrix H is very sparse, so we use the scipy sparse
    # package to handle the matrix/vector products.
    for look in ("fore", "mid", "aft"):
        srf_mat = prep_data[f"srf_{look}"].T
        meas = prep_data[f"meas_{look}"]

        # This is the normalization term. To prevent division by zero
        # errors (i.e., pixels that have no "hits") we use a masked
        # array.
        norm = ma.masked_values(srf_mat.sum(axis=1).A1, 0)

        # Compute AVE for all these terms
        sig_mean = srf_mat.dot(meas["sig0"]) / norm
        sig_sq = srf_mat.dot(meas["sig0"] ** 2) / norm
        inc = srf_mat.dot(meas["inc"]) / norm
        azi = srf_mat.dot(meas["azi"]) / norm
        time = srf_mat.dot(meas["time"]) / norm
        num = srf_mat.getnnz(axis=1)

        out_data[look] = {
            "sig0_mean": sig_mean,
            "sig0_std": (sig_sq - sig_mean ** 2) ** 0.5,
            "inc": inc,
            "azi": azi,
            "time": time,
            "num": num,
        }

    return out_data


def write_output(
    avewr_file: Path,
    prep_file: Path,
    avewr_data: AvewrData,
    land_mask: typing.Optional[np.ndarray],
) -> None:
    """Write the data to the output file."""
    timestamp = datetime.now(tz.tzlocal()).isoformat(" ", "seconds")

    with netCDF4.Dataset(avewr_file, "w") as f:
        # Set global attributes
        f.setncatts(
            {
                "title": "ASCAT AVEWR data",
                "Conventions": "CF-1.7,ACDD-1.3",
                "creator_name": "Richard Lindsley",
                "creator_type": "person",
                "netcdf_version_id": netCDF4.getlibversion().split()[0],
                "date_created": timestamp,
                "history": (
                    f"{timestamp} created by "
                    f"{Path(__file__).name} v{__version__}"
                ),
                "prep_file": prep_file.name,
                "instrument": avewr_data["instrument"],
                "platform": avewr_data["platform"],
                "time_coverage_start": avewr_data["time_coverage_start"],
                "time_coverage_end": avewr_data["time_coverage_end"],
            }
        )

        f.setncatts(avewr_data["region"])

        # Define dimensions and coordinate variables
        f.createDimension("x", len(avewr_data["map_bins"]["x"]))
        f.createDimension("y", len(avewr_data["map_bins"]["y"]))
        f.createDimension("look", 3)

        look_dict = {"fore": 1, "mid": 2, "aft": 3}
        look_type = f.createEnumType(np.int8, "look_t", look_dict)

        f.createVariable("x", np.float64, ("x",))
        f.createVariable("y", np.float64, ("y",))
        f.createVariable("look", look_type, ("look",))

        # Set coordinate variable attributes and data
        f["x"].setncatts(
            {
                "units": "meters",
                "standard_name": "projection_x_coordinate",
                "long_name": "x coordinate of projection",
                "axis": "X",
            }
        )
        f["y"].setncatts(
            {
                "units": "meters",
                "standard_name": "projection_y_coordinate",
                "long_name": "y coordinate of projection",
                "axis": "Y",
            }
        )
        f["look"].setncatts(
            {
                "long_name": "ASCAT look direction",
                "comment": "1: fore, 2: mid, 3: aft",
            }
        )

        f["x"][:] = avewr_data["map_bins"]["x"]
        f["y"][:] = avewr_data["map_bins"]["y"]
        f["look"][:] = list(sorted(look_dict.values()))

        # Set mapping data
        f.createVariable("crs", np.int32, ())
        f["crs"].setncatts(avewr_data["map_crs"])
        f["crs"][...] = 0

        # Panoply (due to a bug in netCDF-java, I think?) doesn't seem
        # to handle the EASE2 grid quite right: there's a northeast
        # offset for some reason. So we provide a fallback of lat/lon
        # variables.
        map_x, map_y = np.meshgrid(
            avewr_data["map_bins"]["x"],
            avewr_data["map_bins"]["y"],
            indexing="ij",
        )
        proj = pyproj.Proj(avewr_data["map_crs"]["proj4text"])
        map_latlon = proj(map_x, map_y, inverse=True, errcheck=True)

        f.createVariable("lat", np.float64, ("x", "y"))
        f.createVariable("lon", np.float64, ("x", "y"))
        f["lat"].setncatts(
            {"standard_name": "latitude", "units": "degrees_north",}
        )
        f["lon"].setncatts(
            {"standard_name": "longitude", "units": "degrees_east",}
        )
        f["lat"][:, :] = map_latlon[1]
        f["lon"][:, :] = map_latlon[0]

        # Define remaining variables
        f32_fill = netCDF4.default_fillvals["f4"]
        i32_fill = netCDF4.default_fillvals["i4"]
        i8_fill = netCDF4.default_fillvals["i1"]
        f.createVariable(
            "sig0_mean", np.float32, ("look", "x", "y"), fill_value=f32_fill
        )
        f.createVariable(
            "sig0_std", np.float32, ("look", "x", "y"), fill_value=f32_fill
        )
        f.createVariable(
            "inc", np.float32, ("look", "x", "y"), fill_value=f32_fill
        )
        f.createVariable(
            "azi", np.float32, ("look", "x", "y"), fill_value=f32_fill
        )
        f.createVariable(
            "time", np.int32, ("look", "x", "y"), fill_value=i32_fill
        )
        f.createVariable(
            "num", np.int32, ("look", "x", "y"), fill_value=i32_fill
        )

        f["sig0_mean"].setncatts(
            {
                "units": "decibel",
                "long_name": "normalized radar cross section",
                "valid_min": np.float32(-80),
                "valid_max": np.float32(0),
                "grid_mapping": "crs",
                "coordinates": "lat lon",
            }
        )
        f["sig0_std"].setncatts(
            {
                "units": "decibel",
                "long_name": (
                    "standard deviation of " "normalized radar cross section"
                ),
                # 'valid_min': np.float32(-80),
                # 'valid_max': np.float32(0),
                "grid_mapping": "crs",
                "coordinates": "lat lon",
            }
        )
        f["inc"].setncatts(
            {
                "units": "degree",
                "long_name": "incidence angle",
                "standard_name": "angle_of_incidence",
                "valid_min": np.float32(0),
                "valid_max": np.float32(90),
                "grid_mapping": "crs",
                "coordinates": "lat lon",
            }
        )
        f["azi"].setncatts(
            {
                "units": "degree",
                "long_name": "azimuth angle",
                "standard_name": "sensor_azimuth_angle",
                "valid_min": np.float32(-180),
                "valid_max": np.float32(180),
                "grid_mapping": "crs",
                "coordinates": "lat lon",
            }
        )
        f["time"].setncatts(
            {
                "units": "seconds since 2000-01-01 00:00:00Z",
                "long_name": "measurement time",
                "standard_name": "time",
                "axis": "T",
                "grid_mapping": "crs",
                "coordinates": "lat lon",
            }
        )
        f["num"].setncatts(
            {
                "long_name": "number of measurements",
                "grid_mapping": "crs",
                "coordinates": "lat lon",
            }
        )

        if land_mask is not None:
            f.createVariable("land", np.int8, ("x", "y"), fill_value=i8_fill)
            f["land"].setncatts(
                {
                    "long_name": "land mask",
                    "valid_min": np.int8(0),
                    "valid_max": np.int8(1),
                    "flag_values": np.array([0, 1], np.int8),
                    "flag_meanings": "ocean land",
                    "grid_mapping": "crs",
                    "coordinates": "lat lon",
                }
            )

        # Store the values
        for var in ("sig0_mean", "sig0_std", "inc", "azi", "time", "num"):
            f[var][0, :, :] = avewr_data["fore"][var]
            f[var][1, :, :] = avewr_data["mid"][var]
            f[var][2, :, :] = avewr_data["aft"][var]

        if land_mask is not None:
            f["land"][:, :] = land_mask


def read_land_map(
    land_map_file: Path,
    proj4str: str,
    map_bins: typing.Mapping[str, np.ndarray],
) -> np.ndarray:
    """Read in the land map file and inverse geoproject it to the map.

    The land map is a netCDF file created using GMT. It is stored in
    lat/lon coordinates, so it has to be geoprojected to the map
    coordinates. To avoid holes in the map, we perform a reverse
    geoprojection and iterate over the map cells and lookup the
    nearest lat/lon value in the land map and the store whether it's
    ocean or land.

    Return a land map array. It's a numpy boolean mask where True is
    land and False is ocean.

    """
    logger = logging.getLogger(__name__)

    # Read land map information
    with netCDF4.Dataset(land_map_file, "r") as f:
        if "GMT_version" not in f.ncattrs():
            raise Exception("Land mask file does not appear to be from GMT")
        else:
            gmt_ver = f.getncattr("GMT_version")
            logger.debug(f"Land map produced with GMT version: {gmt_ver}")

        land_lats = f["lat"][:]
        land_lons = f["lon"][:]
        land_vals = f["z"][:, :]

    # Convert map coordinates to lat/lon values
    map_x, map_y = np.meshgrid(map_bins["x"], map_bins["y"], indexing="ij")
    proj = pyproj.Proj(proj4str)
    map_latlon = proj(map_x, map_y, inverse=True, errcheck=True)

    # Use nearest neighbor interpolator. Note that it's indexed as
    # (lat, lon), but the proj result is indexed as (lon, lat).
    interp_land = RegularGridInterpolator(
        (land_lats, land_lons),
        land_vals,
        method="nearest",
        bounds_error=False,
        fill_value=-1,
    )
    return interp_land((map_latlon[1], map_latlon[0]))


if __name__ == "__main__":
    main()
