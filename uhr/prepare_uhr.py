#!/usr/bin/env python3
"""Prepare ASCAT data for UHR processing.

An ASCAT SZF L1B file in HDF5-format is scanned. All good measurements
within the region of interest are extracted and dumped to the output
file.

A high-resolution Earth-oriented grid is defined. Each measurement
center is geoprojected onto this grid and the spatial response
function (SRF) of each measurement is sampled nearby the center. The
sampled SRF values are also stored to the output file.

The output file is an HDF5 file. A dataset contains the extracted
measurement data, with each measurement's sigma0 value, incidence
angle, etc. Each measurement is a row in this dataset. Each
measurement has a variable number of nearby SRF values that are
non-zero. Thus a corresponding SRF dataset would be a ragged array,
since each row has a varying number of elements. Therefore the 2D
ragged array is vectorized into a 1D array. The start/stop indices in
the SRF dataset are stored in the measurement dataset for the
corresponding measurement.

"""

import argparse
from pathlib import Path
from datetime import datetime, timedelta
import logging
import typing

import numpy as np
import h5py
import pyproj
from dateutil import tz
from tqdm import tqdm

from version import __version__
import ascat_srf_c

__author__ = "Richard Lindsley"


class Region(typing.NamedTuple):
    """Define a region to extract."""

    lat_min: float
    lat_max: float
    lon_min: float
    lon_max: float


L1bData = typing.NewType("L1bData", np.ndarray)
SrfData = typing.NewType("SrfData", typing.Tuple[np.ndarray, np.ndarray])
MapBins = typing.NewType("MapBins", typing.Tuple[np.ndarray, np.ndarray])


def main() -> None:
    """Parse arguments and prepare the UHR data."""
    parser = argparse.ArgumentParser(description="Prepare UHR data")
    parser.add_argument(
        "l1b_file", type=Path, help="Input ASCAT SZF L1B file, HDF5 format"
    )
    parser.add_argument(
        "prep_file", type=Path, help="Output UHR preparation file, HDF5 format"
    )

    verb_group = parser.add_mutually_exclusive_group()
    verb_group.add_argument(
        "--quiet", "-q", action="store_true", help="Reduce logging output"
    )
    verb_group.add_argument(
        "--verbose", "-v", action="store_true", help="Increase logging output"
    )

    region_group = parser.add_argument_group(
        "region", "specify the region of interest"
    )
    region_group.add_argument(
        "--lat-min",
        type=float,
        default=-90,
        help=("Minimum latitude (in degrees North, between -90 and 90)"),
    )
    region_group.add_argument(
        "--lat-max",
        type=float,
        default=90,
        help=("Maximum latitude (in degrees North, between -90 and 90)"),
    )
    region_group.add_argument(
        "--lon-min",
        type=float,
        default=-180,
        help=("Minimum longitude (in degrees East, between -180 and 180)"),
    )
    region_group.add_argument(
        "--lon-max",
        type=float,
        default=180,
        help=("Maximum longitude (in degrees East, between -180 and 180)"),
    )

    parser.add_argument(
        "--geoproj",
        choices=["ease2-glob-3.125", "ease2-glob-6.25", "ease2-glob-12.5"],
        default="ease2-glob-3.125",
        help="Geoprojection to use and pixel size",
    )

    parser.add_argument(
        "--version",
        action="version",
        version=f"%(prog)s version {__version__}",
    )
    args = parser.parse_args()

    # Set up logging
    if args.quiet:
        log_level = logging.WARNING
    elif args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    # log_fmt = "{asctime} {levelname}: {module}.{funcName} {message}"
    log_fmt = "{levelname}: {message}"
    log_datefmt = "%Y-%m-%d %H:%M:%S%z"
    logging.basicConfig(
        style="{", level=log_level, format=log_fmt, datefmt=log_datefmt
    )
    logger = logging.getLogger(__name__)

    # Define region
    region = Region(
        lat_min=args.lat_min,
        lat_max=args.lat_max,
        lon_min=args.lon_min,
        lon_max=args.lon_max,
    )
    logger.info(f"Region of interest: {region}")

    # Set up geoprojection and define how the map bins
    logger.info(f"Setting up PROJ object for {args.geoproj}")
    proj, map_bins = prepare_projection(args.geoproj, region)

    # Read L1B file and extract all measurements
    logger.info(f"Scanning L1B file: {args.l1b_file}")
    l1b_data, sensor = extract_l1b_data(args.l1b_file, region)
    logger.info(f"{len(l1b_data)} valid measurements to use")

    # Geoproject each measurement and sample the SRF
    logger.info("Geoprojecting each measurement and sampling each SRF")
    srf_data = sample_srf(l1b_data, proj, map_bins)

    # Write the output
    logger.info(f"Writing to prep file: {args.prep_file}")
    write_output(
        args.prep_file,
        region,
        args.l1b_file,
        sensor,
        proj,
        map_bins,
        l1b_data,
        srf_data,
    )


def prepare_projection(
    geo_arg: str, region: Region
) -> typing.Tuple[pyproj.Proj, MapBins]:
    """Create a Proj object for projection location to a map.

    PROJ is used to convert from lat/lon to x/y coordinates on a map
    projection. Any arbitrary projection supported by PROJ can be
    used, but the only currently implemented one is the EASE-2.0
    global grid.

    https://nsidc.org/data/ease/ease_grid2.html

    https://epsg.io/6933

    """
    if geo_arg.startswith("ease2-glob"):
        proj = pyproj.Proj(
            proj="cea",
            lon_0=0,
            lat_ts=30,
            x_0=0,
            y_0=0,
            ellps="WGS84",
            datum="WGS84",
            units="m",
        )
        x_min = proj(-180, 0)[0]
        x_max = proj(180, 0)[0]
        y_min = proj(0, -90)[1]
        y_max = proj(0, 90)[1]

        # The grid spacing in meters. Note that for EASE-2
        # cylindrical, it's not exactly 25 km, but 25.02526 km (see
        # https://doi.org/10.3390/ijgi3031154)
        delta_x = 25025.26
        delta_y = 25025.26

        if geo_arg.endswith("-25"):
            pass
        elif geo_arg.endswith("-12.5"):
            delta_x /= 2
            delta_y /= 2
        elif geo_arg.endswith("-6.25"):
            delta_x /= 4
            delta_y /= 4
        elif geo_arg.endswith("-3.125"):
            delta_x /= 8
            delta_y /= 8
        else:
            raise Exception(f"Unknown geoprojection: {geo_arg}")

        # Find the region bounds in map coordinates
        corners_lon = (
            region.lon_min,
            region.lon_min,
            region.lon_max,
            region.lon_max,
        )
        corners_lat = (
            region.lat_min,
            region.lat_max,
            region.lat_min,
            region.lat_max,
        )
        corners_map = proj(corners_lon, corners_lat, errcheck=True)
        region_min_x = min(corners_map[0])
        region_max_x = max(corners_map[0])
        region_min_y = min(corners_map[1])
        region_max_y = max(corners_map[1])

        def generate_bins(
            map_min: float,
            map_max: float,
            delta: float,
            region_min: float,
            region_max: float,
        ) -> np.ndarray:
            """Generate bins for the x or y coordinates.

            map_min/map_max: the min/max valid values in map coordinates
            delta: the grid spacing in map coordinates (e.g., meters)
            region_min/region_max: the minimum/maximum map coordinate values

            Return a 1d array for the map bins. It's a subset of the full map
            coordinates that still contains the region.

            """
            # Check invariants
            if (
                map_max < map_min
                or region_max < region_min
                or region_min < map_min
                or region_max > map_max
            ):
                raise Exception("Bad map/region boundaries")

            # Compute the number of x/y pixels in the map
            n = int((map_max - map_min) / delta)

            # Lat/lon 0, 0 is *between* pixels, and the pixels represent
            # the *centers* of the cells. Thus, for N bins along x, pixel
            # N/2 is -delta_x/2 and pixel (N/2 + 1) is +delta_x/2. The 0.5
            # shift is to get from the left (or bottom) edge of the pixel
            # to the pixel center.
            bins = (np.arange(n) - (n // 2) + 0.5) * delta

            # Shrink bins to contain the region
            low, high = np.searchsorted(bins, (region_min, region_max))
            low = max(low - 1, 0)
            high = min(high + 1, len(bins) - 1)

            return bins[low:high]

        x_bins = generate_bins(
            x_min, x_max, delta_x, region_min_x, region_max_x
        )
        y_bins = generate_bins(
            y_min, y_max, delta_y, region_min_y, region_max_y
        )

    else:
        raise Exception(f"Unknown geoprojection: {geo_arg}")

    return proj, MapBins((x_bins, y_bins))


def read_l1b_meta(mphr: h5py.Dataset) -> typing.Tuple[str, str]:
    """Read the L1B MPHR to determine some information.

    The MPHR is an opened HDF5 Dataset.

    Return the sensor name (e.g., ASCAT-A) and the format version.

    """
    logger = logging.getLogger(__name__)
    sensor, format_version = None, None
    for row in mphr:
        row_key = row[0].decode().strip()
        row_val = row[1].decode().strip()
        logger.debug(f"MPHR: {row_key}={row_val}")

        if row_key == "SPACECRAFT_ID":
            if row_val == "M02":
                sensor = "ASCAT-A"
            elif row_val == "M01":
                sensor = "ASCAT-B"
            elif row_val == "M03":
                sensor = "ASCAT-C"
            logger.info(f"{sensor} file detected")

        if row_key == "PROCESSING_LEVEL":
            if row_val != "1B":
                raise Exception("ASCAT L1B file not detected")

        if row_key == "PRODUCT_TYPE":
            if row_val != "SZF":
                raise Exception("ASCAT SZF product not detected")

        # Only format versions 11 and 12 have been tested
        if row_key == "FORMAT_MAJOR_VERSION":
            format_version = row_val
            if format_version not in ("11", "12"):
                raise Exception(
                    f"L1B is an unsupported format version: {row_val}"
                )

    # If we haven't read these from the MPHR, we're in trouble
    if sensor is None:
        raise Exception("Didn't read the sensor from the metadata")
    if format_version is None:
        raise Exception("Didn't read the L1B format version from the metadata")
    return sensor, format_version


def read_l1b_data_v12(
    mdr_meta: h5py.Dataset, mdr_data: h5py.Dataset, region: Region
) -> L1bData:
    """Read (a subset of) the version 12 L1B data."""
    logger = logging.getLogger(__name__)
    logger.debug(f"MDR has shape: {mdr_data.shape}")

    # Some sanity checking
    scale_factor_strs = {
        b"SIGMA0_FULL": b"6",
        b"INC_ANGLE_FULL": b"2",
        b"AZI_ANGLE_FULL": b"2",
        b"LATITUDE_FULL": b"6",
        b"LONGITUDE_FULL": b"6",
    }
    for key, val in scale_factor_strs.items():
        mdr_meta_entry = mdr_meta[mdr_meta["EntryName"] == key]
        if mdr_meta_entry["Scale Factor"] != val:
            raise Exception(f"Unexpected scale factor for {key!r}: {val!r}")

    # Load all the measurements into memory and apply the scaling factors. In
    # version 12 format data, each MDR row is for a single one of the six ASCAT
    # beams, and a subset of only 192 samples from each 256-sample beam are stored.
    #
    # Thus, for version 12, each of the arrays below should have shape (N,
    # 192) where N is the number of MDR rows
    meas_sig0 = mdr_data["SIGMA0_FULL"].astype(np.float32) * 1e-6
    meas_inc = mdr_data["INC_ANGLE_FULL"].astype(np.float32) * 1e-2
    meas_azi = mdr_data["AZI_ANGLE_FULL"].astype(np.float32) * 1e-2
    meas_lat = mdr_data["LATITUDE_FULL"].astype(np.float32) * 1e-6
    meas_lon = mdr_data["LONGITUDE_FULL"].astype(np.float32) * 1e-6

    # These have shape (N, ) since each applies to all values within
    # the beam (or, row of data)
    meas_beam = mdr_data["BEAM_NUMBER"]
    meas_time_days = mdr_data["UTC_LOCALISATION-days"]
    meas_time_msec = mdr_data["UTC_LOCALISATION-milliseconds"]
    meas_asc = mdr_data["AS_DES_PASS"]

    # Apply quality masking.
    #
    # According to Craig Anderson at EUMETSAT, the flags may be
    # categorized into "red" and "amber" to indicate severity.
    # Amber flags indicate that the data is slightly degraded but
    # still usable. Red flags indicate that the data is severely
    # degraded and should be discarded or used with caution.
    #
    # The following are red flags: v_pg, v_filter, f_np, f_orbit,
    # f_attitude, f_omega, f_man, f_tel_ir, f_geo, f_pgp. The
    # following are amber flags: f_noise, f_pg, f_filter, f_sol.
    # These are warnings since they may mean other flags are
    # incorrect: f_osv, f_e_tel_pres. He didn't mention this one
    # but I interpret it as warning: f_pgp_drop.
    #
    # All the flagfields have shape (N, ) expect for
    # FLAGFIELD_GEN2 which is (N, 192).
    flagfield_rf1 = mdr_data["FLAGFIELD_RF1"]
    f_noise = (flagfield_rf1 & 1) > 0
    f_pg = (flagfield_rf1 & 2) > 0
    v_pg = (flagfield_rf1 & 4) > 0
    f_filter = (flagfield_rf1 & 8) > 0
    v_filter = (flagfield_rf1 & 16) > 0

    flagfield_rf2 = mdr_data["FLAGFIELD_RF2"]
    f_pgp = (flagfield_rf2 & 1) > 0
    f_np = (flagfield_rf2 & 2) > 0
    f_pgp_drop = (flagfield_rf2 & 4) > 0

    flagfield_pl = mdr_data["FLAGFIELD_PL"]
    f_orbit = (flagfield_pl & 1) > 0
    f_attitude = (flagfield_pl & 2) > 0
    f_omega = (flagfield_pl & 4) > 0
    f_man = (flagfield_pl & 8) > 0
    f_osv = (flagfield_pl & 16) > 0

    flagfield_gen1 = mdr_data["FLAGFIELD_GEN1"]
    f_e_tel_pres = (flagfield_gen1 & 1) > 0
    f_tel_ir = (flagfield_gen1 & 2) > 0

    flagfield_gen2 = mdr_data["FLAGFIELD_GEN2"]
    f_sol = (flagfield_gen2 & 1) > 0
    f_geo = (flagfield_gen2 & 4) > 0

    # Collect the 1D flags first
    qc_red = (
        v_pg
        | v_filter
        | f_np
        | f_orbit
        | f_attitude
        | f_omega
        | f_man
        | f_tel_ir
        | f_pgp
    )
    qc_amber = f_noise | f_pg | f_filter | f_osv | f_e_tel_pres | f_pgp_drop

    # Broadcast them to (N, 192) and then apply the remaining flags
    qc_red = qc_red[:, np.newaxis] | f_geo
    qc_amber = qc_amber[:, np.newaxis] | f_sol

    if qc_red.any():
        logger.info(
            (f"{qc_red.sum()}/{qc_red.size} measurements rejected using QC")
        )
    if qc_amber.any():
        logger.warning(
            (
                f"{qc_amber.sum()}/{qc_amber.size} "
                "measurements are questionable using QC"
            )
        )

    # The longitudes are in the range of (0, 360), so convert to
    # (-180, 180)
    meas_lon[meas_lon > 180] -= 360

    # Convert time from days since 2000-01-01 and milliseconds since start of
    # day to second since 2000-01-01
    days_unique = np.unique(meas_time_days)
    if len(days_unique) > 1:
        raise Exception("Day boundary not yet handled")
    day_edge = timedelta(days=days_unique.item())
    meas_time = int(day_edge.total_seconds()) + (meas_time_msec // 1000)

    # Apply the region mask to subset the measurements
    region_mask = meas_lat <= region.lat_max
    region_mask &= meas_lat >= region.lat_min
    region_mask &= meas_lon <= region.lon_max
    region_mask &= meas_lon >= region.lon_min

    # Add the QC mask to (possibly) further subset the measurements
    qc_mask = region_mask & ~qc_red

    out_dtype = np.dtype(
        [
            ("sig0", np.float32),
            ("inc", np.float32),
            ("azi", np.float32),
            ("lat", np.float32),
            ("lon", np.float32),
            ("beam", np.int8),
            ("time", np.uint32),
            ("asc", np.int8),
            ("node", np.uint8),
        ]
    )
    out_meas = L1bData(np.zeros(qc_mask.sum(), dtype=out_dtype))

    out_meas["sig0"] = meas_sig0[qc_mask]
    out_meas["inc"] = meas_inc[qc_mask]
    out_meas["azi"] = meas_azi[qc_mask]
    out_meas["lat"] = meas_lat[qc_mask]
    out_meas["lon"] = meas_lon[qc_mask]

    # Use numpy broadcasting to treat the (N, ) arrays as (N, 192) to
    # match the (N, 192) shape of the region mask. Also note that asc
    # is 0 for ascending, 1 for descending.
    beams = np.broadcast_to(meas_beam[:, np.newaxis], qc_mask.shape)
    ascends = np.broadcast_to(meas_asc[:, np.newaxis], qc_mask.shape)
    times = np.broadcast_to(meas_time[:, np.newaxis], qc_mask.shape)
    out_meas["beam"] = beams[qc_mask]
    out_meas["asc"] = ascends[qc_mask]
    out_meas["time"] = times[qc_mask]

    # For the node number (or along-beam node index), the SRF library
    # expects a value from 0 to 255. However in the format 12 (and
    # higher?) format version data, only 192 values are reported,
    # where 64 edge values are dropped. So we have to translate the
    # 192-index to 256-index. It differs between mid and side beams.
    nodes = np.tile(np.arange(192), (qc_mask.shape[0], 1))
    mid_beam_mask = (beams == 2) | (beams == 5)
    nodes[mid_beam_mask] += 33
    nodes[~mid_beam_mask] += 39
    out_meas["node"] = nodes[qc_mask]

    return out_meas


def read_l1b_data_v11(
    mdr_meta: h5py.Dataset, mdr_data: h5py.Dataset, region: Region
) -> L1bData:
    """Read (a subset of) the version 11 L1B data."""
    logger = logging.getLogger(__name__)
    logger.debug(f"MDR has shape: {mdr_data.shape}")

    # Some sanity checking
    scale_factor_strs = {
        key: b"6"
        for key in (
            b"SIGMA0_FULL",
            b"INC_ANGLE_FULL",
            b"AZI_ANGLE_FULL",
            b"LATITUDE_FULL",
            b"LONGITUDE_FULL",
        )
    }
    for key, val in scale_factor_strs.items():
        mdr_meta_entry = mdr_meta[mdr_meta["EntryName"] == key]
        if mdr_meta_entry["Scale Factor"] != val:
            raise Exception(f"Unexpected scale factor for {key!r}: {val!r}")

    # Load all the measurements into memory and apply the scaling factors. In
    # version 11 format data, each MDR row contains all six beams in sequence.
    # Also all 256 samples from each beam are stored, rather than a subset.
    #
    # For version 11, each array should have shape (N, 256 * 6) where N is the
    # number of MDR rows (and N * 6 is the total number of beam sequences). (And
    # note that the scaling factors are slightly different.)
    meas_sig0 = mdr_data["SIGMA0_FULL"].astype(np.float32) * 1e-6
    meas_inc = mdr_data["INC_ANGLE_FULL"].astype(np.float32) * 1e-6
    meas_azi = mdr_data["AZI_ANGLE_FULL"].astype(np.float32) * 1e-6
    meas_lat = mdr_data["LATITUDE_FULL"].astype(np.float32) * 1e-6
    meas_lon = mdr_data["LONGITUDE_FULL"].astype(np.float32) * 1e-6

    # These have shape (N, 6)
    meas_beam = mdr_data["BEAM_NUMBER"]
    meas_time_days = mdr_data["UTC_LOCALISATION-days"]
    meas_time_msec = mdr_data["UTC_LOCALISATION-milliseconds"]
    meas_asc = mdr_data["AS_DES_PASS"]

    # Note that no quality masking is applied to the version 11 data.

    # The longitudes are in the range of (0, 360), so convert to
    # (-180, 180)
    meas_lon[meas_lon > 180] -= 360

    # Convert time from days since 2000-01-01 and milliseconds since start of
    # day to second since 2000-01-01
    days_unique = np.unique(meas_time_days)
    if len(days_unique) > 1:
        raise Exception("Day boundary not yet handled")
    day_edge = timedelta(days=days_unique.item())
    meas_time = int(day_edge.total_seconds()) + (meas_time_msec // 1000)

    # For the node number (or along-beam node index), the SRF library expects a
    # value from 0 to 255. For the format version 11 data, this is pretty
    # straightforward to obtain.
    nodes_per_mdr = np.tile(np.arange(256), 6)
    meas_nodes = np.broadcast_to(nodes_per_mdr[np.newaxis, :], meas_sig0.shape)

    # Apply the region mask to subset the measurements
    region_mask = meas_lat <= region.lat_max
    region_mask &= meas_lat >= region.lat_min
    region_mask &= meas_lon <= region.lon_max
    region_mask &= meas_lon >= region.lon_min

    # We need to duplicate some content to treat the (N, 6) arrays as (N, 1536).
    # This cannot be done with broadcasting.
    beams = np.repeat(meas_beam, 256, axis=1)
    ascends = np.repeat(meas_asc, 256, axis=1)
    times = np.repeat(meas_time, 256, axis=1)

    # Here's a simple QC mask: only keep sigma0 values that are within -50 and 0
    # dB. Also, omit swath edges.
    qc_mask = region_mask & (meas_sig0 >= -50) & (meas_sig0 <= 0)
    mid_beams = (beams == 2) | (beams == 5)
    side_beams = (beams == 1) | (beams == 3) | (beams == 4) | (beams == 6)
    mid_edges = mid_beams & ((meas_nodes < 33) | (meas_nodes > 224))
    side_edges = side_beams & ((meas_nodes < 39) | (meas_nodes > 230))
    qc_mask &= ~(side_edges | mid_edges)

    out_dtype = np.dtype(
        [
            ("sig0", np.float32),
            ("inc", np.float32),
            ("azi", np.float32),
            ("lat", np.float32),
            ("lon", np.float32),
            ("beam", np.int8),
            ("time", np.uint32),
            ("asc", np.int8),
            ("node", np.uint8),
        ]
    )
    out_meas = L1bData(np.zeros(qc_mask.sum(), dtype=out_dtype))

    out_meas["sig0"] = meas_sig0[qc_mask]
    out_meas["inc"] = meas_inc[qc_mask]
    out_meas["azi"] = meas_azi[qc_mask]
    out_meas["lat"] = meas_lat[qc_mask]
    out_meas["lon"] = meas_lon[qc_mask]
    out_meas["node"] = meas_nodes[qc_mask]
    out_meas["beam"] = beams[qc_mask]
    out_meas["asc"] = ascends[qc_mask]
    out_meas["time"] = times[qc_mask]

    return out_meas


def extract_l1b_data(
    l1b_file: Path, region: Region
) -> typing.Tuple[L1bData, str]:
    """Scan the ASCAT SZF L1B file and extract measurements within the region.

    The L1B file is an HDF5-format file. The valid measurements within
    the region are extract and stored in a numpy structured array.

    Return the measurement array and a string for 'ASCAT-A',
    'ASCAT-B', or 'ASCAT-C'.

    """
    logger = logging.getLogger(__name__)

    with h5py.File(l1b_file, "r") as f:
        # Scan the metadata
        mphr = f["/U-MARF/EPS/ASCA_SZF_1B/METADATA/MPHR/MPHR_TABLE"]
        sensor, format_version = read_l1b_meta(mphr)

        # Read from the actual data
        data_grp = f["/U-MARF/EPS/ASCA_SZF_1B/DATA"]
        if format_version == "12":
            mdr_meta = data_grp["MDR_1B_FULL_ASCA_Level_1_DESCR"]
            mdr_data = data_grp["MDR_1B_FULL_ASCA_Level_1_ARRAY_000001"]
            out_meas = read_l1b_data_v12(mdr_meta, mdr_data, region)
        elif format_version == "11":
            mdr_meta = data_grp["MDR_1B_FULL_ASCAT_L1_DESCR"]
            mdr_data = data_grp["MDR_1B_FULL_ASCAT_L1_ARRAY_000001"]
            out_meas = read_l1b_data_v11(mdr_meta, mdr_data, region)

    return out_meas, sensor


def sample_srf(
    l1b_data: L1bData, proj: pyproj.Proj, map_bins: MapBins
) -> SrfData:
    """Sample the spatial response function near each measurement.

    It isn't possible to know a priori how many non-negligible map
    bins are "hit" by the SRF for each measurement. So this iterates
    through each measurement, samples the SRF on nearby points, and
    stores the values in a list of variably sized arrays. (Due to this
    iteration, this function would be perform much better in a
    compiled language than in Python.)

    The ragged 2D array of SRF values per measurement is linearized
    into a long 1D array.

    """
    # Find map coordinates for measurement centers
    meas_cens = proj(l1b_data["lon"], l1b_data["lat"], errcheck=True)

    # Find bins of measurement centers
    cols = np.digitize(meas_cens[0], map_bins[0])
    rows = np.digitize(meas_cens[1], map_bins[1])

    nx, ny = len(map_bins[0]), len(map_bins[1])

    # We should never be out of bounds, but let's make sure
    if (
        np.any(cols == 0)
        or np.any(cols == nx)
        or np.any(rows == 0)
        or np.any(rows == ny)
    ):
        raise Exception("Location out of bounds?!")

    # Because digitize returns the bin index for bins[i-1] <= x <
    # bins[i], we have to shift it down by one
    cols -= 1
    rows -= 1

    # Initialize the C SRF
    srf_lib = Path(__file__).parent / "../srf/ascat_srf_v4.so"
    srf_v4 = ascat_srf_c.SRF_v4(srf_lib)

    srf_dtype = np.dtype(
        [
            ("val", np.float32),
            ("bin_row", np.int32),
            ("bin_col", np.int32),
            ("bin_flat", np.int32),
            ("meas_index", np.int32),
        ]
    )

    srf_inds = np.zeros(
        len(l1b_data), dtype=[("start", np.int32), ("stop", np.int32)]
    )
    # Loop over each measurement
    srf_entries = []
    srf_rows_used = 0
    for meas_i in tqdm(range(len(l1b_data)), unit="meas"):
        meas = l1b_data[meas_i]

        # Note that 'asc' is 0 for ascending and 1 for descending, but
        # that's the reverse of how the SRF library treats it.
        if meas["asc"] == 0:
            asc_flag = 1
        elif meas["asc"] == 1:
            asc_flag = 0
        else:
            raise Exception("Unexpected asc/desc pass value")

        srf_v4.meas(
            meas["lat"],
            meas["lon"],
            meas["inc"],
            meas["azi"],
            meas["beam"],
            asc_flag,
            meas["node"],
        )

        # Measurement center (x, y) in map coordinates
        meas_cen_x = meas_cens[0][meas_i]
        meas_cen_y = meas_cens[1][meas_i]

        # Just sample the 5x5 neighborhood around each measurement
        neigh_cols = np.s_[
            max(cols[meas_i] - 2, 0) : min(cols[meas_i] + 3, nx)
        ]
        neigh_rows = np.s_[
            max(rows[meas_i] - 2, 0) : min(rows[meas_i] + 3, ny)
        ]

        neigh_x = map_bins[0][neigh_cols]
        neigh_y = map_bins[1][neigh_rows]

        # The difference in km from the measurement center (x, y) and
        # the neighboring map coordinates (x, y) is used to query the
        # SRF
        delta_x = (meas_cen_x - neigh_x) / 1e3
        delta_y = (meas_cen_y - neigh_y) / 1e3

        neigh_grid_x, neigh_grid_y = np.meshgrid(
            delta_x, delta_y, indexing="ij"
        )
        neigh_grid_inds = np.mgrid[neigh_cols, neigh_rows]
        neigh_srf_vals = np.zeros_like(neigh_grid_x)
        for i in range(neigh_x.size):
            for j in range(neigh_y.size):
                neigh_srf_vals[i, j] = srf_v4.srf_eval_xy(
                    neigh_grid_x[i, j], neigh_grid_y[i, j]
                )

        srf_entry = np.zeros(neigh_x.size * neigh_y.size, srf_dtype)
        srf_entry["meas_index"][:] = meas_i
        srf_entry["val"][:] = neigh_srf_vals.flat
        srf_entry["bin_col"][:] = neigh_grid_inds[0].flat
        srf_entry["bin_row"][:] = neigh_grid_inds[1].flat

        inds_2d = (srf_entry["bin_col"], srf_entry["bin_row"])
        shape_2d = (len(map_bins[0]), len(map_bins[1]))
        srf_entry["bin_flat"][:] = np.ravel_multi_index(inds_2d, shape_2d)

        srf_entries.append(srf_entry)
        srf_inds[meas_i]["start"] = srf_rows_used
        srf_inds[meas_i]["stop"] = srf_rows_used + len(srf_entry)
        srf_rows_used += len(srf_entry)

    # Concatenate all SRF entries together
    srf_dat = np.concatenate(srf_entries)

    return SrfData((srf_dat, srf_inds))


def write_output(
    prep_file: Path,
    region: Region,
    l1b_file: Path,
    sensor: str,
    proj: pyproj.Proj,
    map_bins: MapBins,
    l1b_data: L1bData,
    srf_data: SrfData,
) -> None:
    """Write the data to the output file."""
    timestamp = datetime.now(tz.tzlocal()).isoformat(" ", "seconds")

    with h5py.File(prep_file, "w") as f:
        # Set global attributes
        f.attrs["title"] = "ASCAT UHR sensor preparation data"
        f.attrs["Conventions"] = "CF-1.7,ACDD-1.3"
        f.attrs["creator_name"] = "Richard Lindsley"
        f.attrs["creator_type"] = "person"
        f.attrs["hdf5_version_id"] = h5py.version.hdf5_version
        f.attrs["date_created"] = timestamp
        f.attrs["history"] = (
            f"{timestamp} created by " f"{Path(__file__).name} v{__version__}"
        )

        f.attrs["l1b_file"] = l1b_file.name
        f.attrs["proj4str"] = proj.srs

        f.attrs["instrument"] = "ASCAT"
        if sensor == "ASCAT-A":
            f.attrs["platform"] = "MetOp-A"
        elif sensor == "ASCAT-B":
            f.attrs["platform"] = "MetOp-B"
        elif sensor == "ASCAT-C":
            f.attrs["platform"] = "MetOp-C"

        f.attrs.create("geospatial_lat_min", region.lat_min, dtype=np.float32)
        f.attrs.create("geospatial_lat_max", region.lat_max, dtype=np.float32)
        f.attrs.create("geospatial_lon_min", region.lon_min, dtype=np.float32)
        f.attrs.create("geospatial_lon_max", region.lon_max, dtype=np.float32)

        epoch = datetime(2000, 1, 1)
        time_start = epoch + timedelta(seconds=l1b_data["time"].min().item())
        time_end = epoch + timedelta(seconds=l1b_data["time"].max().item())
        f.attrs["time_coverage_start"] = time_start.isoformat() + "Z"
        f.attrs["time_coverage_end"] = time_end.isoformat() + "Z"

        # Define datasets and groups
        meas_grp = f.create_group("meas")
        srf_grp = f.create_group("srf")
        map_grp = f.create_group("map")

        meas_grp.attrs["long_name"] = "measurements"
        srf_grp.attrs["long_name"] = "sampled SRF values"

        meas_shape = l1b_data.shape
        meas_dsets = {
            "sig0": meas_grp.create_dataset("sig0", meas_shape, np.float32),
            "inc": meas_grp.create_dataset("inc", meas_shape, np.float32),
            "azi": meas_grp.create_dataset("azi", meas_shape, np.float32),
            "lat": meas_grp.create_dataset("lat", meas_shape, np.float32),
            "lon": meas_grp.create_dataset("lon", meas_shape, np.float32),
            "beam": meas_grp.create_dataset("beam", meas_shape, np.int8),
            "time": meas_grp.create_dataset("time", meas_shape, np.uint32),
            "srf_start": meas_grp.create_dataset(
                "srf_start", meas_shape, np.int32
            ),
            "srf_stop": meas_grp.create_dataset(
                "srf_stop", meas_shape, np.int32
            ),
        }

        srf_shape = srf_data[0].shape
        srf_dsets = {
            "bin_row": srf_grp.create_dataset("bin_row", srf_shape, np.int32),
            "bin_col": srf_grp.create_dataset("bin_col", srf_shape, np.int32),
            "bin_flat": srf_grp.create_dataset(
                "bin_flat", srf_shape, np.int32
            ),
            "meas_index": srf_grp.create_dataset(
                "meas_index", srf_shape, np.int32
            ),
            "val": srf_grp.create_dataset("val", srf_shape, np.float32),
        }

        map_x = map_grp.create_dataset("x", map_bins[0].shape, np.float64)
        map_y = map_grp.create_dataset("y", map_bins[1].shape, np.float64)
        map_crs = map_grp.create_dataset("crs", (1,), np.int32)

        nx = len(map_bins[0])
        ny = len(map_bins[1])

        # Define the dimension scales
        for d in meas_dsets.values():
            d.dims[0].label = "measurement index"
        for d in srf_dsets.values():
            d.dims[0].label = "sampled srf index"

        # Define attributes on the datasets
        meas_dsets["sig0"].attrs["units"] = "decibel"
        meas_dsets["inc"].attrs["units"] = "degree"
        meas_dsets["azi"].attrs["units"] = "degree"
        meas_dsets["lat"].attrs["units"] = "degrees_north"
        meas_dsets["lon"].attrs["units"] = "degrees_east"
        meas_dsets["time"].attrs["units"] = (
            "seconds since " "2000-01-01 00:00:00Z"
        )

        meas_dsets["sig0"].attrs["long_name"] = (
            "normalized radar " "cross section"
        )
        meas_dsets["inc"].attrs["long_name"] = "incidence angle"
        meas_dsets["azi"].attrs["long_name"] = "azimuth angle"
        meas_dsets["lat"].attrs["long_name"] = "latitude"
        meas_dsets["lon"].attrs["long_name"] = "longitude"
        meas_dsets["beam"].attrs["long_name"] = "beam number"
        meas_dsets["time"].attrs["long_name"] = "measurement time"
        meas_dsets["srf_start"].attrs["long_name"] = "srf start index"
        meas_dsets["srf_stop"].attrs["long_name"] = "srf stop index"

        srf_dsets["bin_row"].attrs["long_name"] = (
            "vertical index for " "output grid"
        )
        srf_dsets["bin_col"].attrs["long_name"] = (
            "horizontal index for " "output grid"
        )
        srf_dsets["bin_flat"].attrs["long_name"] = (
            "linearized index for " "output grid"
        )
        srf_dsets["meas_index"].attrs["long_name"] = "measurement index"
        srf_dsets["val"].attrs["long_name"] = "SRF value"

        meas_dsets["inc"].attrs["standard_name"] = "angle_of_incidence"
        meas_dsets["azi"].attrs["standard_name"] = "sensor_azimuth_angle"
        meas_dsets["lat"].attrs["standard_name"] = "latitude"
        meas_dsets["lon"].attrs["standard_name"] = "longitude"
        meas_dsets["time"].attrs["standard_name"] = "time"

        meas_dsets["sig0"].attrs.create("valid_min", -80, dtype=np.float32)
        meas_dsets["sig0"].attrs.create("valid_max", 0, dtype=np.float32)
        meas_dsets["inc"].attrs.create("valid_min", 0, dtype=np.float32)
        meas_dsets["inc"].attrs.create("valid_max", 90, dtype=np.float32)
        meas_dsets["azi"].attrs.create("valid_min", -180, dtype=np.float32)
        meas_dsets["azi"].attrs.create("valid_max", 180, dtype=np.float32)
        meas_dsets["lat"].attrs.create("valid_min", -90, dtype=np.float32)
        meas_dsets["lat"].attrs.create("valid_max", 90, dtype=np.float32)
        meas_dsets["lon"].attrs.create("valid_min", -180, dtype=np.float32)
        meas_dsets["lon"].attrs.create("valid_max", 180, dtype=np.float32)
        meas_dsets["beam"].attrs.create("valid_min", 1, dtype=np.int8)
        meas_dsets["beam"].attrs.create("valid_max", 6, dtype=np.int8)

        srf_dsets["bin_row"].attrs.create("valid_min", 0, dtype=np.int32)
        srf_dsets["bin_row"].attrs.create("valid_max", ny - 1, dtype=np.int32)
        srf_dsets["bin_col"].attrs.create("valid_min", 0, dtype=np.int32)
        srf_dsets["bin_col"].attrs.create("valid_max", nx - 1, dtype=np.int32)
        srf_dsets["bin_flat"].attrs.create("valid_min", 0, dtype=np.int32)
        srf_dsets["bin_flat"].attrs.create(
            "valid_max", nx * ny - 1, dtype=np.int32
        )
        srf_dsets["meas_index"].attrs.create("valid_min", 0, dtype=np.int32)
        srf_dsets["meas_index"].attrs.create(
            "valid_max", len(l1b_data), dtype=np.int32
        )
        srf_dsets["val"].attrs.create("valid_min", 0, dtype=np.float32)
        srf_dsets["val"].attrs.create("valid_max", 1, dtype=np.float32)

        map_x.attrs["units"] = "meters"
        map_y.attrs["units"] = "meters"
        map_x.attrs["standard_name"] = "projection_x_coordinate"
        map_y.attrs["standard_name"] = "projection_y_coordinate"
        map_x.attrs["long_name"] = "x coordinate of projection"
        map_y.attrs["long_name"] = "y coordinate of projection"

        # This assumes the EASE2 global mapping
        map_crs.attrs["grid_mapping_name"] = "cylindrical_equal_area"
        map_crs.attrs.create(
            "latitude_of_projection_origin", 0, dtype=np.float32
        )
        map_crs.attrs.create(
            "longitude_of_projection_origin", 0, dtype=np.float32
        )
        map_crs.attrs.create("standard_parallel", 30, dtype=np.float32)
        map_crs.attrs.create("false_easting", 0, dtype=np.float32)
        map_crs.attrs.create("false_northing", 0, dtype=np.float32)
        map_crs.attrs.create("semi_major_axis", 6_378_137, dtype=np.float64)
        map_crs.attrs.create(
            "inverse_flattening", 298.257_223_563, dtype=np.float64
        )
        map_crs.attrs["proj4text"] = proj.srs
        map_crs.attrs["srid"] = "urn:ogc:def:crs:EPSG::6933"

        # Write the data
        for key, ds in meas_dsets.items():
            if key in l1b_data.dtype.fields:
                ds[:] = l1b_data[key]

        map_x[:] = map_bins[0]
        map_y[:] = map_bins[1]

        srf_dsets["val"][:] = srf_data[0]["val"]
        srf_dsets["bin_row"][:] = srf_data[0]["bin_row"]
        srf_dsets["bin_col"][:] = srf_data[0]["bin_col"]
        srf_dsets["bin_flat"][:] = srf_data[0]["bin_flat"]
        srf_dsets["meas_index"][:] = srf_data[0]["meas_index"]
        meas_dsets["srf_start"][:] = srf_data[1]["start"]
        meas_dsets["srf_stop"][:] = srf_data[1]["stop"]


if __name__ == "__main__":
    main()
