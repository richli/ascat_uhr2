"""ASCAT geophysical model functions.

The GMF relates ocean surface wind speed and direction (at a height of
10 meters, neutral stability conditions) to the observed normalized
radar cross section, incidence angle, and azimuth angle.

The GMF generally takes the form of a lookup table.

Currently only the CMOD5 and CMOD5.n GMFs are implemented, since they
are described in the open literature.

"""
import numpy as np
import numexpr as ne

# flake8: noqa


class cmod5:
    """CMOD5 and CMOD5.n ASCAT geophysical model functions.

    CMOD5 is described by [1], which is available for download [2].
    CMOD5.n is described by [3], which is available for download [4].

    Note that these two GMFs share identical forms but differ only in
    the model coefficient values.

    [1] "CMOD5: An improved geophysical model function for ERS C-band
    scatterometry", H. Hersbach, ECMWF Tech. Memo #395, 2003.

    [2]
    https://www.ecmwf.int/sites/default/files/elibrary/2003/9861-cmod5-improved-geophysical-model-function-ers-c-band-scatterometry.pdf

    [3] "CMOD5.N: A C-band geophysical model function for equivalent
    neutral wind", H. Hersbach, ECMWF Tech. Memo #554, 2008.

    [4]
    https://www.ecmwf.int/sites/default/files/elibrary/2008/9873-cmod5n-c-band-geophysical-model-function-equivalent-neutral-wind.pdf

    This is an indendent implementation using the form of the GMF
    described in the appendix of [1].

    """

    # These are the model coefficients, from c1 to c28
    coefs_5 = np.array(
        (
            -0.688,
            -0.793,
            0.338,
            -0.173,
            0.0,
            0.004,
            0.111,
            0.0162,
            6.34,
            2.57,
            -2.18,
            0.4,
            -0.6,
            0.045,
            0.007,
            0.33,
            0.012,
            22.0,
            1.95,
            3.0,
            8.39,
            -3.44,
            1.36,
            5.35,
            1.99,
            0.29,
            3.80,
            1.53,
        )
    )
    coefs_5n = np.array(
        (
            -0.6878,
            -0.7957,
            0.338,
            -0.1728,
            0.0,
            0.004,
            0.1103,
            0.0159,
            6.7329,
            2.7713,
            -2.2885,
            0.4971,
            -0.725,
            0.045,
            0.0066,
            0.3222,
            0.012,
            22.7,
            2.0813,
            3.0,
            8.3659,
            -3.3428,
            1.3236,
            6.2437,
            2.3893,
            0.3249,
            4.159,
            1.693,
        )
    )

    def __init__(self, form: str) -> None:
        """Initialize the GMF by specifying which one to use.

        form: either 'cmod5' or 'cmod5n'

        """
        if form in ("cmod5", "cmod5n"):
            self.form = form
        else:
            raise Exception(f"Unknown CMOD form: {form}")

    def call(
        self,
        wspd: np.ndarray,
        wdir: np.ndarray,
        inc: np.ndarray,
        azi: np.ndarray,
    ) -> np.ndarray:
        """Evaluate the GMF for the input values.

        The inputs are all numpy arrays of the same shape/size.

        wspd: the wind speed, in m/s

        wdir: the wind direction, in degrees clockwise from North
        (oceanographic convention)

        inc: the observation incidence angle, in degrees

        azi: the observation azimuth angle, in degrees clockwise from
        North

        Return the predicted sigma0 (normalized radar cross section)
        in dB.

        """
        if (
            wspd.shape != wdir.shape
            or wdir.shape != inc.shape
            or inc.shape != azi.shape
        ):
            raise Exception("Misshapen inputs")

        # Note that for the variable names, I'm largely using the
        # convention in the EMCWF tech report appendix. Note that the
        # appendix uses Fortran (1-based) indexing, but this is using
        # Python (0-based) indexing!
        phi = np.deg2rad(wdir - azi)
        x = (inc - 40) / 25

        if self.form == "cmod5":
            c = self.coefs_5
        elif self.form == "cmod5n":
            c = self.coefs_5n

        # a0 = c[0] + c[1] * x + c[2] * x**2 + c[3] * x**3
        # a1 = c[4] + c[5] * x
        # a2 = c[6] + c[7] * x
        # γ = c[8] + c[9] * x + c[10] * x**2
        # s0 = c[11] + c[12] * x

        a0 = np.polyval(c[[3, 2, 1, 0]], x)
        a1 = np.polyval(c[[5, 4]], x)
        a2 = np.polyval(c[[7, 6]], x)
        gam = np.polyval(c[[10, 9, 8]], x)
        s0 = np.polyval(c[[12, 11]], x)

        s = a2 * wspd
        a3 = np.reciprocal(1 + np.exp(-np.fmax(s, s0)))

        l = s < s0
        a3[l] *= (s[l] / s0[l]) ** (s0[l] * (1 - a3[l]))

        b0 = a3 ** gam * 10 ** (a0 + a1 * wspd)
        b1_part = (
            c[14] * wspd * (0.5 + x - np.tanh(4 * (x + c[15] + c[16] * wspd)))
        )
        b1 = (c[13] * (1 + x) - b1_part) / (1 + np.exp(0.34 * (wspd - c[17])))

        # v0 = c[20] + c[21] * x + c[22] * x**2
        # d1 = c[23] + c[24] * x + c[25] * x**2
        # d2 = c[26] + c[27] * x

        v0 = np.polyval(c[[22, 21, 20]], x)
        d1 = np.polyval(c[[25, 24, 23]], x)
        d2 = np.polyval(c[[27, 26]], x)

        y0, n = c[18], c[19]
        a = y0 - (y0 - 1) / n
        b = np.reciprocal(n * (y0 - 1) ** (n - 1))

        y = (wspd / v0) + 1
        v2 = y
        l = v2 < y0
        v2[l] = a + b * (y[l] - 1) ** n

        b2 = (-d1 + d2 * v2) * np.exp(-v2)

        sig0 = b0 * (1 + b1 * np.cos(phi) + b2 * np.cos(2 * phi)) ** 1.6
        return sig0

    def call_fast(
        self,
        wspd: np.ndarray,
        wdir: np.ndarray,
        inc: np.ndarray,
        azi: np.ndarray,
    ) -> np.ndarray:
        """Evaluate the GMF for the input values.

        Numexpr is used to speed up the computation.

        The input winds are both numpy arrays of the same shape/size,
        but inc/azi are scalars.

        wspd: the wind speed, in m/s

        wdir: the wind direction, in degrees clockwise from North
        (oceanographic convention)

        inc: the observation incidence angle, in degrees

        azi: the observation azimuth angle, in degrees clockwise from
        North

        Return the predicted sigma0 (normalized radar cross section)
        in dB.

        """
        if wspd.shape != wdir.shape or azi.shape != () or inc.shape != ():
            raise Exception("Misshapen inputs")

        # Note that for the variable names, I'm largely using the
        # convention in the EMCWF tech report appendix. Note that the
        # appendix uses Fortran (1-based) indexing, but this is using
        # Python (0-based) indexing!

        phi = np.deg2rad(wdir - azi)
        x = (inc - 40) / 25

        if self.form == "cmod5":
            c = self.coefs_5
        elif self.form == "cmod5n":
            c = self.coefs_5n

        a0 = c[0] + c[1] * x + c[2] * x ** 2 + c[3] * x ** 3
        a1 = c[4] + c[5] * x
        a2 = c[6] + c[7] * x
        gam = c[8] + c[9] * x + c[10] * x ** 2
        s0 = c[11] + c[12] * x

        s = a2 * wspd
        a3 = np.reciprocal(1 + np.exp(-np.fmax(s, s0)))

        a3 *= ne.evaluate("where(s < s0, (s/s0)**(s0 * (1 - a3)), 1)")

        b0 = ne.evaluate("a3**gam * 10**(a0 + a1 * wspd)")
        c_13, c_14, c_15, c_16, c_17 = c[13:18]
        b1_part = ne.evaluate(
            "c_14 * wspd * (0.5 + x" "- tanh(4 * (x + c_15 + c_16 * wspd)))"
        )
        b1 = ne.evaluate(
            "(c_13 * (1 + x) - b1_part) " "/ (1 + exp(0.34 * (wspd - c_17)))"
        )

        v0 = c[20] + c[21] * x + c[22] * x ** 2
        d1 = c[23] + c[24] * x + c[25] * x ** 2
        d2 = c[26] + c[27] * x

        y0, n = c[18], c[19]
        a = y0 - (y0 - 1) / n
        b = np.reciprocal(n * (y0 - 1) ** (n - 1))

        y = ne.evaluate("(wspd / v0) + 1")
        v2 = ne.evaluate("where(y < y0, a + b * (y - 1)**n, y)")

        b2 = ne.evaluate("(-d1 + d2 * v2) * exp(-v2)")

        sig0 = ne.evaluate("b0 * (1 + b1 * cos(phi) + b2 * cos(2 * phi))**1.6")
        return sig0
