#!/usr/bin/env python3
"""Python bindings to ASCAT SRF v4."""

__author__ = "Richard Lindsley"

from pathlib import Path
import typing

from cffi import FFI
import numpy as np


class SRF_v4:
    """Interface for the v4 C approximate SRF."""

    def __init__(self, srf_lib: Path) -> None:
        """Define stuff for the FFI."""
        # Function prototypes, taken from: ascat_srf.h
        self.srf_ffi = FFI()
        self.srf_ffi.cdef(
            """
        void srf_init();
        void srf_done();
        void srf_meas(double lat, double lon, double inc, double azi,
                          int swath, int ascend, int node);
        void srf_latlon_extent(double * const tl_lat, double * const tl_lon,
                double * const tr_lat, double * const tr_lon,
                double * const br_lat, double * const br_lon,
                double * const bl_lat, double * const bl_lon, int *stat);
        double srf_eval_ll(double lat, double lon);
        double srf_eval_xy(double x, double y);
                    """
        )

        if srf_lib.exists():
            self.srf = self.srf_ffi.dlopen(str(srf_lib))
        else:
            raise Exception(f"{srf_lib} not found")

        # Call the C srf_init()
        self.srf.srf_init()

    def __del__(self) -> None:
        """Class destructor."""
        self.srf.srf_done()

    def meas(
        self,
        lat: float,
        lon: float,
        inc: float,
        azi: float,
        beam: int,
        ascend: int,
        node: int,
    ) -> None:
        """Initialize the SRF with measurement info.

        {lat,lon}: measurement location
        inc: incidence angle
        azi: azimuth angle
        beam: which beam number, 1-based (1-6)
        ascend: 1 (ascending) or 0 (descending)
        node: normally between 0 and 255, but now 31 to 223.

        """
        # Same check as in C, but I can handle it here better
        if abs(lat) > 89.1:
            print(f"WARNING: latitude is {lat}, skipping measurement")
            return

        self.srf.srf_meas(lat, lon, inc, azi, beam, ascend, node)

    def latlon_extent(self) -> typing.Tuple[typing.Tuple[float, float], ...]:
        """Compute the approximate extent in latitude/longitude.

        Returns the tuple of tuples (topleft, topright, bottomright,
        bottomleft, ret), where the first four elements is a tuple (lat,
        lon). The last element is the return code: 0 for bad, 4 for good.

        """
        tl_lat = self.srf_ffi.new("double *", 0)
        tl_lon = self.srf_ffi.new("double *", 0)
        tr_lat = self.srf_ffi.new("double *", 0)
        tr_lon = self.srf_ffi.new("double *", 0)
        bl_lat = self.srf_ffi.new("double *", 0)
        bl_lon = self.srf_ffi.new("double *", 0)
        br_lat = self.srf_ffi.new("double *", 0)
        br_lon = self.srf_ffi.new("double *", 0)
        ret = self.srf_ffi.new("int *")
        self.srf.srf_latlon_extent(
            tl_lat, tl_lon, tr_lat, tr_lon, br_lat, br_lon, bl_lat, bl_lon, ret
        )

        return (
            (tl_lat[0], tl_lon[0]),
            (tr_lat[0], tr_lon[0]),
            (br_lat[0], br_lon[0]),
            (bl_lat[0], bl_lon[0]),
            ret[0],
        )

    def srf_eval_ll(self, lat: float, lon: float) -> float:
        """Compute the SRF at the given location."""
        srf_val = self.srf.srf_eval_ll(lat, lon)
        return srf_val

    def srf_eval_xy(self, x: float, y: float) -> float:
        """Compute the SRF at the given location.

        x,y: km from measurement center on the northing/easting tangent
        plane.

        """
        srf_val = self.srf.srf_eval_xy(x, y)
        return srf_val

    def compute_srf_on_ll_grid(self, lat, lon) -> np.ndarray:
        """Python helper function to compute SRF over a grid.

        lat, lon are arrays.

        """
        lat = np.asanyarray(lat)
        lon = np.asanyarray(lon)

        if lat.shape != lon.shape:
            raise Exception("lat/lon shapes do not match")

        srf_grid = np.zeros_like(lat)
        it = np.nditer(
            srf_grid, flags=["multi_index",], op_flags=["readwrite"]
        )
        for idx in it:
            lat_val = lat[it.multi_index]
            lon_val = lon[it.multi_index]
            srf_val = self.srf_eval_ll(lat_val, lon_val)
            srf_grid[it.multi_index] = srf_val

        return srf_grid


def main() -> None:
    """Just a testbed for the wrapper."""
    cur_lat = 41.831920623779297
    cur_lon = -66.354766845703125
    swath = 1
    ascend = True
    azi = 20
    inc = 50.349998474121094
    node = 43

    print("Creating v4 SRF object")
    lib_file = Path("../srf/ascat_srf_v4.so")
    srf_v4 = SRF_v4(lib_file)

    print("Initializing SRF")
    srf_v4.meas(cur_lat, cur_lon, inc, azi, swath, ascend, node)

    rect_tl, rect_tr, rect_br, rect_bl, nc = srf_v4.latlon_extent()
    tl_lat, tl_lon = rect_tl[0], rect_tl[1]
    tr_lat, tr_lon = rect_tr[0], rect_tr[1]
    br_lat, br_lon = rect_br[0], rect_br[1]
    bl_lat, bl_lon = rect_bl[0], rect_bl[1]

    print("v4 lat/lon extent")
    print(f"topleft: ({tl_lat},{tl_lon})")
    print(f"topright: ({tr_lat},{tr_lon})")
    print(f"botright: ({br_lat},{br_lon})")
    print(f"botleft: ({bl_lat},{bl_lon})")
    print(f"return code: {nc}")


if __name__ == "__main__":
    main()
