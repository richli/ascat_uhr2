#!/usr/bin/env python3
"""Retrieve ASCAT UHR winds from AVEWR data.

The AVEWR input file contains the (reconstructed) measurement values
on a high-resolution grid. Wind retrieval is performed at each grid
point independently and written to an output UHR file in netCDF
format.

"""

import argparse
from pathlib import Path
from datetime import datetime
import logging
import typing

import numpy as np
from numpy import ma
import netCDF4
from dateutil import tz
from tqdm import tqdm

import gmf
from version import __version__

__author__ = "Richard Lindsley"


AvewrData = typing.NewType("AvewrData", typing.Dict[str, typing.Any])
UhrData = typing.NewType("UhrData", typing.Dict[str, typing.Any])


class WvcOut(typing.NamedTuple):
    """Retrieved output values for a wind vector cell."""

    wspds: ma.MaskedArray
    wdirs: ma.MaskedArray
    mles: ma.MaskedArray


def main() -> None:
    """Parse arguments and retrieve UHR winds."""
    parser = argparse.ArgumentParser(description="Retrieve UHR winds")
    parser.add_argument(
        "avewr_file", type=Path, help="Input AVEWR file, netCDF format"
    )
    parser.add_argument(
        "uhr_file", type=Path, help="Output UHR file, netCDF format"
    )

    verb_group = parser.add_mutually_exclusive_group()
    verb_group.add_argument(
        "--quiet", "-q", action="store_true", help="Reduce logging output"
    )
    verb_group.add_argument(
        "--verbose", "-v", action="store_true", help="Increase logging output"
    )

    parser.add_argument(
        "--version",
        action="version",
        version=f"%(prog)s version {__version__}",
    )
    args = parser.parse_args()

    # Set up logging
    if args.quiet:
        log_level = logging.WARNING
    elif args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    # log_fmt = "{asctime} {levelname}: {module}.{funcName} {message}"
    log_fmt = "{levelname}: {message}"
    log_datefmt = "%Y-%m-%d %H:%M:%S%z"
    logging.basicConfig(
        style="{", level=log_level, format=log_fmt, datefmt=log_datefmt
    )
    logger = logging.getLogger(__name__)

    # Read AVEWR data
    logger.info(f"Loading data from avewr file: {args.avewr_file}")
    avewr_data = load_avewr_data(args.avewr_file)

    # Perform wind retrieval
    logger.info("Retrieving winds")
    uhr_data = compute_uhr(avewr_data)

    # Write the output
    logger.info(f"Writing to uhr file: {args.uhr_file}")
    write_output(args.uhr_file, uhr_data)


def compute_uhr(avewr_data: AvewrData) -> UhrData:
    """Perform wind retrieval for each grid point."""
    logger = logging.getLogger(__name__)
    logger.debug("Preparing for wind retrieval")

    x = avewr_data["x"]
    y = avewr_data["y"]
    max_ambs = 4

    # We're going to do a brute-force optimization: for all possible
    # wind speeds/directions, compute the predicted sigma0 using the
    # GMF. Then use the least-squares method to find the
    # speed/direction arguments that minimize the sum-of-squared
    # residuals.
    #
    # For wind retrieval with the ASCAT geometry, there are generally
    # two local minima in the space of residual error versus wind
    # speed/wind direction. These are typically about the same wind
    # speed but the wind direction is spaced about 180 degrees apart.
    # In some conditions, three or four minima are possible.
    #
    # These minima are termed ambiguities and are stored for later
    # post-processing. We keep no more than four ambiguities.

    # Here are the winds to search over
    wspds_search = np.linspace(0.5, 30, 100)
    wdirs_search = np.linspace(-180, 180, 180, endpoint=False)
    gmf_wd, gmf_ws = np.meshgrid(wdirs_search, wspds_search, indexing="xy")

    # Initialize the GMF object
    logger.info("Using the CMOD5.n GMF")
    cmod5n = gmf.cmod5("cmod5n")

    # Initialize the output arrays
    out_shape = (max_ambs, len(x), len(y))
    wspds = ma.masked_all(out_shape, np.float32)
    wdirs = ma.masked_all(out_shape, np.float32)
    mles = ma.masked_all(out_shape, np.float32)

    # Ready the inputs
    sig = avewr_data["sig0_mean"]
    azi = avewr_data["azi"]
    inc = avewr_data["inc"]

    # If a land mask is in the AVEWR file then use it, otherwise just
    # assume the entire scene is ocean
    if "land" in avewr_data:
        land = avewr_data["land"]
    else:
        land = np.full((len(x), len(y)), False)

    # Iterate over each grid point
    for i, j in tqdm(
        np.ndindex(len(x), len(y)), total=len(x) * len(y), unit="pixel"
    ):
        wvc_out = retrieve_one_wvc(
            cmod5n,
            gmf_ws,
            gmf_wd,
            sig[:, i, j],
            azi[:, i, j],
            inc[:, i, j],
            land[i, j],
        )
        wspds[:, i, j] = wvc_out.wspds
        wdirs[:, i, j] = wvc_out.wdirs
        mles[:, i, j] = wvc_out.mles

    uhr_data = UhrData(
        {
            "x": avewr_data["x"],
            "y": avewr_data["y"],
            "max_ambs": max_ambs,
            "region": avewr_data["region"],
            "map_crs": avewr_data["map_crs"],
            "instrument": avewr_data["instrument"],
            "platform": avewr_data["platform"],
            "time_coverage_start": avewr_data["time_coverage_start"],
            "time_coverage_end": avewr_data["time_coverage_end"],
            "avewr_fname": avewr_data["avewr_fname"],
            "wspds": wspds,
            "wdirs": wdirs,
            "mles": mles,
            "time": avewr_data["time"].mean(axis=0),
            "gmf_kind": "cmod5n",
            "lat": avewr_data["lat"],
            "lon": avewr_data["lon"],
        }
    )
    if "land" in avewr_data:
        uhr_data["land"] = land
    return uhr_data


def retrieve_one_wvc(
    gmf: gmf.cmod5,
    gmf_ws: np.ndarray,
    gmf_wd: np.ndarray,
    sig: ma.MaskedArray,
    azi: ma.MaskedArray,
    inc: ma.MaskedArray,
    land: bool,
) -> WvcOut:
    """Retrieve one wind vector cell from the input data.

    gmf: the GMF object to use

    sig, azi, inc: the input sigma0, azimuth angle, and incidence
    angle for the WVC. These are each masked numpy arrays with shape
    (3, ).

    gmf_ws, gmf_ws: a meshgrid containing all wind speeds/directions
    to test

    land: whether this is land or not

    Return the output in a WvcOut tuple. It contains the wind speeds,
    wind directions, and MLE values for up to 4 ambiguities.

    """
    logger = logging.getLogger(__name__)

    if sig.shape != azi.shape or azi.shape != inc.shape:
        raise Exception("Inputs are misshapen")

    if sig.shape != (3,):
        raise Exception("Inputs are misshapen")

    # If the input is totally masked or if it's flagged as land,
    # there's nothing to do
    if sig.count() == 0 or land:
        return WvcOut(
            ma.masked_all(4, np.float32),
            ma.masked_all(4, np.float32),
            ma.masked_all(4, np.float32),
        )

    # Compute the GMF for all winds for each of the (valid) looks. The
    # squared sigma0 residual (in decibel space) is stored and
    # accumulated for the looks.
    mle_all = np.zeros(gmf_ws.shape)
    for l in np.arange(3)[~sig.mask]:
        # gmf_inc = np.full_like(gmf_ws, inc[l])
        # gmf_azi = np.full_like(gmf_ws, azi[l])
        # sig_m = gmf.call(gmf_ws, gmf_wd, gmf_inc, gmf_azi)
        sig_m = gmf.call_fast(gmf_ws, gmf_wd, inc[l], azi[l])
        sig_m_db = pow2db(sig_m)

        mle_all += (sig[l] - sig_m_db) ** 2

    # How do we find the local minima? The GMF seems to be well
    # behaved when wind speed varies but wind direction is held
    # constant. It's when the wind direction varies that multiple
    # local minima can occur. Usually just two, but sometimes 3 or 4
    # are possible.
    inds = [np.argmin(mle_all, axis=0), np.arange(mle_all.shape[1])]
    mle_1 = mle_all[inds]
    wspd_1 = gmf_ws[inds]
    wdir_1 = gmf_wd[inds]

    wspds_out = ma.masked_all(4, np.float32)
    wdirs_out = ma.masked_all(4, np.float32)
    mles_out = ma.masked_all(4, np.float32)

    # There's only one valid input. This isn't enough to retrieve both
    # speed and direction but we can at least use the GMF to find the
    # average wind speed (across all directions) that corresponds to
    # this input.
    if sig.count() == 1:
        wspds_out[0] = wspd_1.mean()
        wdirs_out[0] = ma.masked
        mles_out[0] = mle_1.mean()

    # We have 2 or 3 inputs, so we can continue with finding the local
    # minima. A local minimum (in the 1D sense, which is what we now
    # have) is defined as the value when an element is less than its
    # neighboring elements. Before finding this, prepend and append
    # values to wrap along the direction domain.
    else:
        mle_1_wrap = np.r_[mle_1[-1], mle_1, mle_1[0]]
        local_mask = (mle_1_wrap[1:-1] < mle_1_wrap[0:-2]) & (
            mle_1_wrap[1:-1] < mle_1_wrap[2:]
        )

        num_amb = local_mask.sum()
        if num_amb > 4:
            logger.warning(
                (f"{num_amb} local minima found " f"({sig.count()} inputs)")
            )
            # Just pick the 4 with lowest MLE
            top_inds = mle_1[local_mask].argsort()[:4]
            mles_out[:] = mle_1[local_mask][top_inds]
            wspds_out[:] = wspd_1[local_mask][top_inds]
            wdirs_out[:] = wdir_1[local_mask][top_inds]
        else:
            mles_out[:num_amb] = mle_1[local_mask]
            wspds_out[:num_amb] = wspd_1[local_mask]
            wdirs_out[:num_amb] = wdir_1[local_mask]

    return WvcOut(wspds_out, wdirs_out, mles_out)


# def pow2db(x: ma.MaskedArray) -> ma.MaskedArray:
#     """Convert from power to decibel space."""
#     return 10 * np.log10(x, where=(x > 0))


def pow2db(x: np.ndarray) -> np.ndarray:
    """Convert from power to decibel space."""
    return 10 * np.log10(x, where=(x > 0))


def load_avewr_data(avewr_file: Path) -> AvewrData:
    """Read the data from the avewr file."""
    in_data = AvewrData({"avewr_fname": avewr_file.name,})
    region_atts = (
        "geospatial_lat_min",
        "geospatial_lat_max",
        "geospatial_lon_max",
        "geospatial_lon_min",
    )

    with netCDF4.Dataset(avewr_file, "r") as f:
        in_data["x"] = f["x"][:]
        in_data["y"] = f["y"][:]

        for var in ("sig0_mean", "inc", "azi", "time"):
            in_data[var] = f[var][:, :, :]

        for var in ("lat", "lon"):
            in_data[var] = f[var][:, :]

        # The land map is optional
        if "land" in f.variables:
            in_data["land"] = f["land"][:, :]

        in_data["map_crs"] = {
            key: f["crs"].getncattr(key) for key in f["crs"].ncattrs()
        }
        in_data["region"] = {key: f.getncattr(key) for key in region_atts}

        for att in (
            "instrument",
            "platform",
            "time_coverage_start",
            "time_coverage_end",
        ):
            in_data[att] = f.getncattr(att)

    return in_data


def write_output(uhr_file: Path, uhr_data: UhrData) -> None:
    """Write the data to the output file."""
    timestamp = datetime.now(tz.tzlocal()).isoformat(" ", "seconds")

    with netCDF4.Dataset(uhr_file, "w") as f:
        # Set global attributes
        f.setncatts(
            {
                "title": "ASCAT UHR data",
                "Conventions": "CF-1.7,ACDD-1.3",
                "creator_name": "Richard Lindsley",
                "creator_type": "person",
                "netcdf_version_id": netCDF4.getlibversion().split()[0],
                "date_created": timestamp,
                "history": (
                    f"{timestamp} created by "
                    f"{Path(__file__).name} v{__version__}"
                ),
                "avewr_file": uhr_data["avewr_fname"],
                "gmf": uhr_data["gmf_kind"],
                "instrument": uhr_data["instrument"],
                "platform": uhr_data["platform"],
                "time_coverage_start": uhr_data["time_coverage_start"],
                "time_coverage_end": uhr_data["time_coverage_end"],
            }
        )

        f.setncatts(uhr_data["region"])

        # Define dimensions and coordinate variables
        f.createDimension("x", len(uhr_data["x"]))
        f.createDimension("y", len(uhr_data["y"]))
        f.createDimension("amb", uhr_data["max_ambs"])

        f.createVariable("x", np.float64, ("x",))
        f.createVariable("y", np.float64, ("y",))
        f.createVariable("amb", np.int16, ("amb",))

        # Set coordinate variable attributes and data
        f["x"].setncatts(
            {
                "units": "meters",
                "standard_name": "projection_x_coordinate",
                "long_name": "x coordinate of projection",
                "axis": "X",
                "coverage_content_type": "coordinate",
            }
        )
        f["y"].setncatts(
            {
                "units": "meters",
                "standard_name": "projection_y_coordinate",
                "long_name": "y coordinate of projection",
                "axis": "Y",
                "coverage_content_type": "coordinate",
            }
        )
        f["amb"].setncatts(
            {
                "long_name": "retrieval ambiguity index",
                "coverage_content_type": "coordinate",
            }
        )

        f["x"][:] = uhr_data["x"]
        f["y"][:] = uhr_data["y"]
        f["amb"][:] = np.arange(uhr_data["max_ambs"])

        # Set mapping data
        f.createVariable("crs", np.int32, ())
        f["crs"].setncatts(uhr_data["map_crs"])
        f["crs"][...] = 0

        # Panoply (due to a bug in netCDF-java, I think?) doesn't seem
        # to handle the EASE2 grid quite right: there's a northeast
        # offset for some reason. So we copy the lat/lon variables
        # from the avewr to the uhr file.
        f.createVariable("lat", np.float64, ("x", "y"))
        f.createVariable("lon", np.float64, ("x", "y"))
        f["lat"].setncatts(
            {"standard_name": "latitude", "units": "degrees_north",}
        )
        f["lon"].setncatts(
            {"standard_name": "longitude", "units": "degrees_east",}
        )
        f["lat"][:, :] = uhr_data["lat"]
        f["lon"][:, :] = uhr_data["lon"]

        # Define remaining variables
        f32_fill = netCDF4.default_fillvals["f4"]
        i32_fill = netCDF4.default_fillvals["i4"]
        i8_fill = netCDF4.default_fillvals["i1"]
        f.createVariable(
            "ambig_wspds", np.float32, ("amb", "x", "y"), fill_value=f32_fill
        )
        f.createVariable(
            "ambig_wdirs", np.float32, ("amb", "x", "y"), fill_value=f32_fill
        )
        f.createVariable(
            "ambig_mles", np.float32, ("amb", "x", "y"), fill_value=f32_fill
        )
        f.createVariable("time", np.int32, ("x", "y"), fill_value=i32_fill)

        f["ambig_wspds"].setncatts(
            {
                "units": "m s-1",
                "long_name": "wind speed ambiguities",
                "standard_name": "wind_speed",
                "valid_min": np.float32(0),
                "valid_max": np.float32(50),
                "grid_mapping": "crs",
                "coordinates": "lat lon",
                "coverage_content_type": "image",
            }
        )
        f["ambig_wdirs"].setncatts(
            {
                "units": "degree",
                "long_name": "wind direction ambiguities",
                "standard_name": "wind_to_direction",
                "valid_min": np.float32(-180),
                "valid_max": np.float32(180),
                "grid_mapping": "crs",
                "coordinates": "lat lon",
                "coverage_content_type": "image",
            }
        )
        f["ambig_mles"].setncatts(
            {
                # 'units': '',
                "long_name": "max-likelihood estimator values",
                "valid_min": np.float32(0),
                "valid_max": np.float32(1e3),
                "grid_mapping": "crs",
                "coordinates": "lat lon",
                "coverage_content_type": "image",
            }
        )
        f["time"].setncatts(
            {
                "units": "seconds since 2000-01-01 00:00:00Z",
                "long_name": "measurement time",
                "standard_name": "time",
                "axis": "T",
                "grid_mapping": "crs",
                "coordinates": "lat lon",
                "coverage_content_type": "referenceInformation",
            }
        )

        if "land" in uhr_data:
            f.createVariable("land", np.int8, ("x", "y"), fill_value=i8_fill)
            f["land"].setncatts(
                {
                    "long_name": "land mask",
                    "valid_min": np.int8(0),
                    "valid_max": np.int8(1),
                    "flag_values": np.array([0, 1], np.int8),
                    "flag_meanings": "ocean land",
                    "grid_mapping": "crs",
                    "coordinates": "lat lon",
                }
            )

        # Store the values
        for var in ("time",):
            f[var][:] = uhr_data[var]
        for var in ("wspds", "wdirs", "mles"):
            f[f"ambig_{var}"][:] = uhr_data[var]
        if "land" in uhr_data:
            f["land"][:] = uhr_data["land"]


if __name__ == "__main__":
    main()
