#!/usr/bin/env python3
"""Post-process ASCAT UHR winds.

Model winds are read and interpolated to the UHR grid and saved to the
UHR file. The model winds are also used for ambiguity selection and
the UHR ambiguity selected is saved in the UHR file. Also, some
redundant variables are output to the UHR file that contain the
unambiguous UHR wind speed, wind direction, and zonal and meridional
wind components.

"""

import argparse
from pathlib import Path
from datetime import datetime
import logging
import typing

import numpy as np
from numpy import ma
from scipy.interpolate import RegularGridInterpolator
import netCDF4
import pyproj
from dateutil import tz

from version import __version__

__author__ = "Richard Lindsley"


UhrData = typing.NewType("UhrData", typing.Dict[str, typing.Any])
Merra2Data = typing.NewType("Merra2Data", typing.Dict[str, typing.Any])
ModelWinds = typing.NewType("ModelWinds", typing.Dict[str, typing.Any])
UnambWinds = typing.NewType("UnambWinds", typing.Dict[str, typing.Any])


def main() -> None:
    """Parse arguments and post-process UHR winds."""
    parser = argparse.ArgumentParser(description="Post-process UHR winds")
    parser.add_argument(
        "uhr_file", type=Path, help="UHR file to update, netCDF format"
    )
    parser.add_argument(
        "model_file", type=Path, help="Model winds file, netCDF format"
    )

    verb_group = parser.add_mutually_exclusive_group()
    verb_group.add_argument(
        "--quiet", "-q", action="store_true", help="Reduce logging output"
    )
    verb_group.add_argument(
        "--verbose", "-v", action="store_true", help="Increase logging output"
    )

    parser.add_argument(
        "--version",
        action="version",
        version=f"%(prog)s version {__version__}",
    )
    args = parser.parse_args()

    # Set up logging
    if args.quiet:
        log_level = logging.WARNING
    elif args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    # log_fmt = "{asctime} {levelname}: {module}.{funcName} {message}"
    log_fmt = "{levelname}: {message}"
    log_datefmt = "%Y-%m-%d %H:%M:%S%z"
    logging.basicConfig(
        style="{", level=log_level, format=log_fmt, datefmt=log_datefmt
    )
    logger = logging.getLogger(__name__)

    # Read UHR information
    logger.info(f"Reading data from UHR file: {args.uhr_file}")
    uhr_data = load_uhr_data(args.uhr_file)

    # Read model winds
    logger.info(f"Reading model data from file: {args.model_file}")
    model_data = load_merra2_data(args.model_file)

    # Trilinearly interpolate model winds to UHR data
    logger.info("Interpolating model winds to UHR data")
    model_interp = interp_merra2_uhr(uhr_data, model_data)

    # Perform ambiguity selection
    logger.info("Selecting ambiguities")
    amb_select = select_ambiguities(uhr_data, model_interp)

    # Apply ambiguity selection and also find unambiguous wind
    # components
    logger.info("Finding unambiguous wind variables")
    unamb_winds = apply_amb_sel(uhr_data, amb_select)

    # Write the output
    logger.info(f"Updating uhr file: {args.uhr_file}")
    write_output(args.uhr_file, model_interp, amb_select, unamb_winds)


def load_uhr_data(uhr_file: Path) -> UhrData:
    """Read the data from the UHR file.

    Return a dict with the relevant data.

    """
    in_data = UhrData({})
    region_atts = (
        "geospatial_lat_min",
        "geospatial_lat_max",
        "geospatial_lon_max",
        "geospatial_lon_min",
    )

    with netCDF4.Dataset(uhr_file, "r") as f:
        for var in ("x", "y", "amb", "ambig_wspds", "ambig_wdirs", "time"):
            in_data[var] = f[var][:]

        in_data["time_units"] = f["time"].getncattr("units")
        in_data["map_crs"] = {
            key: f["crs"].getncattr(key) for key in f["crs"].ncattrs()
        }
        in_data["region"] = {key: f.getncattr(key) for key in region_atts}

    return in_data


def load_merra2_data(merra2_file: Path) -> Merra2Data:
    """Read the data from the MERRA-2 file.

    The MERRA-2 product used is the "M2T1NXOCN" dataset, or the hourly
    ocean surface diagnostics.

    Return a dict with the relevant data.

    """
    in_data = Merra2Data({"merra2_fname": merra2_file.name,})

    with netCDF4.Dataset(merra2_file, "r") as f:
        # The MERRA-2 data seems to have bad 'valid_range' attributes,
        # so disable the netCDF auto scaling/masking. The data doesn't
        # need to be scaled.
        f.set_auto_maskandscale(False)
        for var in ("lat", "lon", "time", "U10M", "V10M"):
            in_data[var] = f[var][:]

        m2_time_units = f["time"].getncattr("units")

    # Convert MERRA-2 time units to UHR
    merra_times = netCDF4.num2date(in_data["time"], m2_time_units)
    uhr_time_units = "seconds since 2000-01-01 00:00:00Z"
    adj_times = netCDF4.date2num(merra_times, uhr_time_units)

    in_data["time"] = adj_times
    in_data["time_units"] = uhr_time_units

    return in_data


def interp_merra2_uhr(uhr_data: UhrData, model_data: Merra2Data) -> ModelWinds:
    """Interpolate the MERRA-2 winds to the UHR grid.

    A trilinear interpolation is used since the interpolation is over
    x/y/time. The wind zonal/meridional components are separately
    interpolated, then the equivalent wind speed/direction are also
    computed.

    Return a dict with the relevant data.

    """
    # Check that time units are the same
    if uhr_data["time_units"] != model_data["time_units"]:
        raise Exception("Mismatched time units")

    # Check that the MERRA-2 times cover the UHR times
    uhr_time_min = uhr_data["time"].min()
    uhr_time_max = uhr_data["time"].max()
    model_time_min = model_data["time"].min()
    model_time_max = model_data["time"].max()
    if uhr_time_min < model_time_min:
        raise Exception("UHR times exist before model")
    if uhr_time_max > model_time_max:
        raise Exception("UHR times exist after model")

    # Prepare the interpolators for the MERRA-2 data. The data is on a
    # regular grid, dimensioned as (time, lat, lon). Note that if we
    # require interpolation near the anti-meridian, the interpolation
    # does not wrap longitudes.
    model_dims = (model_data["time"], model_data["lat"], model_data["lon"])
    interp_u = RegularGridInterpolator(model_dims, model_data["U10M"])
    interp_v = RegularGridInterpolator(model_dims, model_data["V10M"])

    # Project the UHR map coordinates to lat/lon
    proj = pyproj.Proj(uhr_data["map_crs"]["proj4text"])
    uhr_X, uhr_Y = np.meshgrid(uhr_data["x"], uhr_data["y"], indexing="ij")
    uhr_lon, uhr_lat = proj(uhr_X, uhr_Y, inverse=True, errcheck=True)

    # Interpolate zonal/meridional winds. Note that while every UHR
    # grid point has an x/y value and therefore a lat/lon, some of the
    # time values may be masked. So we fill in the masked values
    # temporarily and then re-apply the mask on the output arrays.
    uhr_time = uhr_data["time"]
    uhr_time_fill = uhr_time.filled(uhr_time.mean())
    model_interp_u = interp_u((uhr_time_fill, uhr_lat, uhr_lon))
    model_interp_v = interp_v((uhr_time_fill, uhr_lat, uhr_lon))

    model_interp_wspd = np.hypot(model_interp_u, model_interp_v)
    model_interp_wdir = np.rad2deg(np.arctan2(model_interp_u, model_interp_v))

    masked_u = ma.array(model_interp_u, mask=uhr_time.mask)
    masked_v = ma.array(model_interp_v, mask=uhr_time.mask)
    masked_wspd = ma.array(model_interp_wspd, mask=uhr_time.mask)
    masked_wdir = ma.array(model_interp_wdir, mask=uhr_time.mask)

    return ModelWinds(
        {
            "merra2_fname": model_data["merra2_fname"],
            "model_u": masked_u,
            "model_v": masked_v,
            "model_wspd": masked_wspd,
            "model_wdir": masked_wdir,
        }
    )


def select_ambiguities(
    uhr_data: UhrData, model_interp: ModelWinds
) -> np.ndarray:
    """Perform ambiguity selection.

    For each UHR WVC, the wind vector ambiguity is chosen such that
    the difference between it and the model wind direction is
    minimized. That's it, no spatial filtering or consistency
    constraints are performed.

    Return a numpy array that represents the chosen ambiguity indices.
    It uses 0-based indexing, so with a maximum of 4 ambiguities to
    chose from, it takes on values from 0 to 3. If a WVC has no data
    then the ambiguity index is -1 to represent the no-data value.

    """
    # The model wdir is a 2d array shaped as (x, y). The amb_wdir is a
    # 3d array shaped as (amb, x, y).
    model_wdir = model_interp["model_wdir"]
    amb_wdir = uhr_data["ambig_wdirs"]

    # Find the wind direction difference, but note it's modular
    # arithmetic. Also since array broadcasting is used, this gives a
    # 3d array shaped as (amb, x, y). Due to the modulus, the values
    # will be from 0 to 360, so we have to adjust the large values to
    # "wind the other way" and go negative. So now it ranges from -180
    # to 180.
    diff_wdir = (amb_wdir - model_wdir + 360) % 360
    diff_wdir[diff_wdir > 180] -= 360

    # Pick the ambiguity with smallest absolute direction difference
    amb_select = abs(diff_wdir).argmin(axis=0)

    # If there was no data, flag it
    no_data = ma.getmaskarray(diff_wdir).all(axis=0)
    amb_select[no_data] = -1

    return amb_select


def apply_amb_sel(uhr_data: UhrData, amb_select: np.ndarray) -> UnambWinds:
    """Apply the ambiguity selection to obtain unambiguous winds.

    This computes these unambiguous wind quantities: wind speed, wind
    direction, zonal wind, meridional wind.

    Return the results in a dict.

    """
    # Shift the ambiguity select to range from 0 to 3 with -1 as
    # no-data to 1 to 4 with 0 as no-data.
    wspd = np.choose(
        amb_select + 1,
        (
            np.nan,
            uhr_data["ambig_wspds"][0, :, :],
            uhr_data["ambig_wspds"][1, :, :],
            uhr_data["ambig_wspds"][2, :, :],
            uhr_data["ambig_wspds"][3, :, :],
        ),
    )
    wdir = np.choose(
        amb_select + 1,
        (
            np.nan,
            uhr_data["ambig_wdirs"][0, :, :],
            uhr_data["ambig_wdirs"][1, :, :],
            uhr_data["ambig_wdirs"][2, :, :],
            uhr_data["ambig_wdirs"][3, :, :],
        ),
    )

    # Compute the wind components
    wu = wspd * np.sin(np.deg2rad(wdir))
    wv = wspd * np.cos(np.deg2rad(wdir))

    return UnambWinds(
        {
            "wspd": ma.fix_invalid(wspd),
            "wdir": ma.fix_invalid(wdir),
            "wu": ma.fix_invalid(wu),
            "wv": ma.fix_invalid(wv),
        }
    )


def write_output(
    uhr_file: Path,
    model_data: ModelWinds,
    amb_select: np.ndarray,
    unamb_winds: UnambWinds,
) -> None:
    """Update the UHR file with the post-processed data."""
    timestamp = datetime.now(tz.tzlocal()).isoformat(" ", "seconds")
    hist_entry = (
        f"{timestamp} updated by " f"{Path(__file__).name} v{__version__}"
    )

    with netCDF4.Dataset(uhr_file, "a") as f:
        # Set global attributes
        f.setncatts(
            {
                "model_winds_file": model_data["merra2_fname"],
                "date_modified": timestamp,
            }
        )
        old_hist = f.getncattr("history")
        f.setncattr("history", "\n".join((old_hist, hist_entry)))

        # Define new variables, if needed
        f32_fill = netCDF4.default_fillvals["f4"]
        amb_fill = np.int32(-1)
        if "model_u" not in f.variables:
            f.createVariable(
                "model_u", np.float32, ("x", "y"), fill_value=f32_fill
            )
            f.createVariable(
                "model_v", np.float32, ("x", "y"), fill_value=f32_fill
            )
            f.createVariable(
                "model_wspd", np.float32, ("x", "y"), fill_value=f32_fill
            )
            f.createVariable(
                "model_wdir", np.float32, ("x", "y"), fill_value=f32_fill
            )
            f.createVariable(
                "amb_select", np.int32, ("x", "y"), fill_value=amb_fill
            )
            f.createVariable(
                "uhr_u", np.float32, ("x", "y"), fill_value=f32_fill
            )
            f.createVariable(
                "uhr_v", np.float32, ("x", "y"), fill_value=f32_fill
            )
            f.createVariable(
                "uhr_wspd", np.float32, ("x", "y"), fill_value=f32_fill
            )
            f.createVariable(
                "uhr_wdir", np.float32, ("x", "y"), fill_value=f32_fill
            )

            # Set variable attributes
            f["model_wspd"].setncatts(
                {
                    "units": "m s-1",
                    "long_name": "model wind speed",
                    "standard_name": "wind_speed",
                    "source": "MERRA-2 M2T1NXOCN",
                    "valid_min": np.float32(0),
                    "valid_max": np.float32(50),
                    "grid_mapping": "crs",
                    "coordinates": "lat lon",
                    "coverage_content_type": "modelResult",
                }
            )
            f["uhr_wspd"].setncatts(
                {
                    "units": "m s-1",
                    "long_name": "unambiguous wind speed",
                    "standard_name": "wind_speed",
                    "valid_min": np.float32(0),
                    "valid_max": np.float32(50),
                    "grid_mapping": "crs",
                    "coordinates": "lat lon",
                    "coverage_content_type": "image",
                }
            )

            f["model_wdir"].setncatts(
                {
                    "units": "degree",
                    "long_name": "model wind direction",
                    "standard_name": "wind_to_direction",
                    "source": "MERRA-2 M2T1NXOCN",
                    "valid_min": np.float32(-180),
                    "valid_max": np.float32(180),
                    "grid_mapping": "crs",
                    "coordinates": "lat lon",
                    "coverage_content_type": "modelResult",
                }
            )
            f["uhr_wdir"].setncatts(
                {
                    "units": "degree",
                    "long_name": "unambiguous wind direction",
                    "standard_name": "wind_to_direction",
                    "valid_min": np.float32(-180),
                    "valid_max": np.float32(180),
                    "grid_mapping": "crs",
                    "coordinates": "lat lon",
                    "coverage_content_type": "image",
                }
            )

            f["model_u"].setncatts(
                {
                    "units": "m s-1",
                    "long_name": "model zonal wind",
                    "standard_name": "eastward_wind",
                    "source": "MERRA-2 M2T1NXOCN",
                    "valid_min": np.float32(-50),
                    "valid_max": np.float32(50),
                    "grid_mapping": "crs",
                    "coordinates": "lat lon",
                    "coverage_content_type": "modelResult",
                }
            )
            f["uhr_u"].setncatts(
                {
                    "units": "m s-1",
                    "long_name": "unambiguous zonal wind",
                    "standard_name": "eastward_wind",
                    "valid_min": np.float32(-50),
                    "valid_max": np.float32(50),
                    "grid_mapping": "crs",
                    "coordinates": "lat lon",
                    "coverage_content_type": "image",
                }
            )

            f["model_v"].setncatts(
                {
                    "units": "m s-1",
                    "long_name": "model meridional wind",
                    "standard_name": "northward_wind",
                    "source": "MERRA-2 M2T1NXOCN",
                    "valid_min": np.float32(-50),
                    "valid_max": np.float32(50),
                    "grid_mapping": "crs",
                    "coordinates": "lat lon",
                    "coverage_content_type": "modelResult",
                }
            )
            f["uhr_v"].setncatts(
                {
                    "units": "m s-1",
                    "long_name": "unambiguous meridional wind",
                    "standard_name": "northward_wind",
                    "valid_min": np.float32(-50),
                    "valid_max": np.float32(50),
                    "grid_mapping": "crs",
                    "coordinates": "lat lon",
                    "coverage_content_type": "image",
                }
            )

            f["amb_select"].setncatts(
                {
                    "long_name": "selected ambiguity",
                    "grid_mapping": "crs",
                    "coordinates": "lat lon",
                    "coverage_content_type": "auxillaryInformation",
                }
            )

        # Store the values
        for var in ("model_wspd", "model_wdir", "model_u", "model_v"):
            f[var][:] = model_data[var]
        f["amb_select"][:] = amb_select
        f["uhr_wspd"][:] = unamb_winds["wspd"]
        f["uhr_wdir"][:] = unamb_winds["wdir"]
        f["uhr_u"][:] = unamb_winds["wu"]
        f["uhr_v"][:] = unamb_winds["wv"]


if __name__ == "__main__":
    main()
