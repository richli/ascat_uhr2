#!/usr/bin/env python3
"""Plot ASCAT UHR winds.

Currently only the wind speeds from a UHR file are supported, but in
the future, it is feasible to plot other quantities (e.g., model wind
direction, UHR wind components, streamline plots, etc) and also to
plot data from an AVEWR file.

Currently, only cartopy is used for plotting but in the future GMT is
preferred instead.

"""

import argparse
from pathlib import Path
import logging
import tempfile
import subprocess
import typing

import numpy as np
import netCDF4
import pyproj
import cartopy
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import cartopy.crs as ccrs
import matplotlib.pyplot as plt

from version import __version__

__author__ = "Richard Lindsley"


UhrData = typing.NewType("UhrData", typing.Dict[str, typing.Any])


def main() -> None:
    """Parse arguments and plot UHR winds."""
    parser = argparse.ArgumentParser(description="Plot UHR winds")
    parser.add_argument(
        "uhr_file", type=Path, help="UHR file to plot, netCDF format"
    )

    parser.add_argument(
        "--plt-fmt",
        choices=["png", "pdf"],
        default="png",
        help="Plot format (default: %(default)s)",
    )
    parser.add_argument(
        "--out-dir",
        type=Path,
        default=Path.cwd(),
        help="Output directory (default: %(default)s)",
    )
    parser.add_argument(
        "--backend",
        choices=["cartopy", "gmt"],
        default="cartopy",
        help="Plotting backend to use (default: %(default)s)",
    )

    verb_group = parser.add_mutually_exclusive_group()
    verb_group.add_argument(
        "--quiet", "-q", action="store_true", help="Reduce logging output"
    )
    verb_group.add_argument(
        "--verbose", "-v", action="store_true", help="Increase logging output"
    )

    parser.add_argument(
        "--version",
        action="version",
        version=f"%(prog)s version {__version__}",
    )
    args = parser.parse_args()

    # Set up logging
    if args.quiet:
        log_level = logging.WARNING
    elif args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    # log_fmt = "{asctime} {levelname}: {module}.{funcName} {message}"
    log_fmt = "{levelname}: {message}"
    log_datefmt = "%Y-%m-%d %H:%M:%S%z"
    logging.basicConfig(
        style="{", level=log_level, format=log_fmt, datefmt=log_datefmt
    )
    logger = logging.getLogger(__name__)

    # Read UHR information
    logger.info(f"Reading data from UHR file: {args.uhr_file}")
    uhr_data = load_uhr_data(args.uhr_file)

    # Plot UHR wind speeds
    logger.info(f"Plotting UHR wind speeds using {args.backend}")
    if args.backend == "cartopy":
        plot_map_cartopy(uhr_data, "uhr_wspd", args.plt_fmt, args.out_dir)
    elif args.backend == "gmt":
        plot_map_gmt(uhr_data, "uhr_wspd", args.plt_fmt, args.out_dir)


def load_uhr_data(uhr_file: Path) -> UhrData:
    """Read the data from the UHR file.

    Return a dict with the relevant data.

    """
    in_data = UhrData({})
    region_atts = (
        "geospatial_lat_min",
        "geospatial_lat_max",
        "geospatial_lon_max",
        "geospatial_lon_min",
    )

    with netCDF4.Dataset(uhr_file, "r") as f:
        for var in ("x", "y", "uhr_wspd"):
            in_data[var] = f[var][:]

        in_data["map_crs"] = {
            key: f["crs"].getncattr(key) for key in f["crs"].ncattrs()
        }
        in_data["region"] = {key: f.getncattr(key) for key in region_atts}

    return in_data


def plot_map_cartopy(
    uhr_data: UhrData, key: str, plt_fmt: str, out_dir: Path
) -> None:
    """Plot the data using cartopy.

    uhr_data: dict containing relevant data from a UHR file

    key: which variable to plot

    plt_fmt: either 'png' or 'pdf'

    out_dir: directory where image will be saved

    """
    logger = logging.getLogger(__name__)

    map_proj = ccrs.PlateCarree()
    fig, ax = plt.subplots(figsize=(6, 8), subplot_kw={"projection": map_proj})

    min_lat = uhr_data["region"]["geospatial_lat_min"] - 2
    max_lat = uhr_data["region"]["geospatial_lat_max"] + 2
    min_lon = uhr_data["region"]["geospatial_lon_min"] - 2
    max_lon = uhr_data["region"]["geospatial_lon_max"] + 2
    ax.set_extent((min_lon, max_lon, min_lat, max_lat), ccrs.PlateCarree())

    # Create a feature for land at 1:50m from Natural Earth
    land = cartopy.feature.NaturalEarthFeature(
        category="physical",
        name="land",
        scale="50m",
        facecolor=cartopy.feature.COLORS["land"],
        edgecolor="black",
    )
    ax.add_feature(land, zorder=0)

    # Project the UHR map coordinates to lat/lon (I think I can
    # somehow have cartopy do this automatically for me but I couldn't
    # get it working right)
    proj = pyproj.Proj(uhr_data["map_crs"]["proj4text"])
    uhr_X, uhr_Y = np.meshgrid(uhr_data["x"], uhr_data["y"], indexing="ij")
    uhr_lon, uhr_lat = proj(uhr_X, uhr_Y, inverse=True, errcheck=True)

    # Draw the data and the colorbar
    im = ax.pcolormesh(
        uhr_lon, uhr_lat, uhr_data[key], transform=ccrs.PlateCarree()
    )
    cbar = fig.colorbar(im)

    if key == "uhr_wspd":
        cbar.set_label("m/s")
        ax.set_title("UHR wind speed")

    # Label edges
    gl = ax.gridlines(draw_labels=True, alpha=0.2)
    gl.yformatter = LATITUDE_FORMATTER
    gl.xformatter = LONGITUDE_FORMATTER
    gl.xlabels_top = False
    gl.ylabels_right = False

    # Save figure
    out_file = out_dir / f"{key}.{plt_fmt}"
    logger.info(f"Saving to: {out_file}")
    fig.savefig(str(out_file), bbox_inches="tight")
    plt.close(fig)


def plot_map_gmt(
    uhr_data: UhrData, data_key: str, plt_fmt: str, out_dir: Path
) -> None:
    """Plot the data using GMT.

    uhr_data: dict containing relevant data from a UHR file

    data_key: which variable to plot

    plt_fmt: either 'png' or 'pdf'

    out_dir: directory where image will be saved

    """
    logger = logging.getLogger(__name__)

    min_lat = uhr_data["region"]["geospatial_lat_min"] - 2
    max_lat = uhr_data["region"]["geospatial_lat_max"] + 2
    min_lon = uhr_data["region"]["geospatial_lon_min"] - 2
    max_lon = uhr_data["region"]["geospatial_lon_max"] + 2

    # Map projection: cylindrical equidistant (Plate Carrée), 8 cm high
    proj = "-JQ0/8ch"

    # Region enclosing the data
    reg = f"-R{min_lon}/{max_lon}/{min_lat}/{max_lat}"

    # Prepare temporary directory for GMT (here we're running GMT in
    # "isolation mode", see
    # http://gmt.soest.hawaii.edu/doc/5.4.3/GMT_Docs.html#running-gmt-in-isolation-mode)
    with tempfile.TemporaryDirectory() as tmpdir_name:
        tmpdir = Path(tmpdir_name)

        # Set common GMT options
        gmt_opts = {
            "GMT_COMPATIBILITY": "5",
            "PS_CHAR_ENCODING": "Standard+",
            "FORMAT_GEO_MAP": "DF",
            "FONT_ANNOT_PRIMARY": "10p",
            "FONT_LABEL": "10p",
            "FONT_TITLE": "12p",
            "MAP_TITLE_OFFSET": "-2p",
            "MAP_GRID_PEN_PRIMARY": "thinnest,darkgray,-",
        }
        cmd = ["gmtset"]
        for key, value in gmt_opts.items():
            cmd.extend([key, "=", value])
        call_gmt(cmd, tmpdir)

        # Define colormaps
        cpt = tmpdir / f"{data_key}.cpt"
        # http://colorbrewer2.org/#type=sequential&scheme=Greens&n=9
        base_cpt = ",".join(
            [
                "#f7fcf5",
                "#e5f5e0",
                "#c7e9c0",
                "#a1d99b",
                "#74c476",
                "#41ab5d",
                "#238b45",
                "#006d2c",
                "#00441b",
            ]
        )
        cbar_range = (0, 30)
        cbar_vals = np.linspace(cbar_range[0], cbar_range[1], 9)
        cbar_vals_form = [f"{v:0.4f}" for v in cbar_vals]
        crange_opt = f'-T{",".join(cbar_vals_form)}'

        with cpt.open("wb") as cpt_f:
            cmd = [
                "makecpt",
                f"-C{base_cpt}",
                crange_opt,
                "-D",
                "-M",
                "--COLOR_NAN=lightgray",
                "-Z",
            ]
            call_gmt(cmd, tmpdir, stdout=cpt_f)

        # Prepare output GMT file
        ps_file = tmpdir / f"{data_key}.ps"
        ps_f = ps_file.open("wb")

        # Draw map border and title
        plt_title = f"UHR wind speed"
        cmd = ["psbasemap", proj, reg, f"-BWSne+t{plt_title}", "-Bag2", "-K"]
        call_gmt(cmd, tmpdir, stdout=ps_f)

        # Draw coastlines on top (and some gridlines)
        cmd = [
            "pscoast",
            proj,
            reg,
            "-A1000/0/1",
            "-Di",
            "-Wthinnest,black",
            "-Gkhaki",
            "-O",
            "-K",
        ]
        call_gmt(cmd, tmpdir, stdout=ps_f)

        # Draw colorbar
        unit = "m/s"
        cmd = [
            "psscale",
            proj,
            reg,
            f"-DJRM+w8c/0.5c+o0.4c/0",
            f"-C{str(cpt)}",
            f"-Ba+l{unit}",
            "--MAP_LABEL_OFFSET=12p",
            "-O",
        ]
        call_gmt(cmd, tmpdir, stdout=ps_f)

        # Convert to png or pdf
        ps_f.close()
        out_file = out_dir / ps_file.with_suffix("." + plt_fmt).name
        logger.info(f"Saving to: {out_file}")
        cmd = ["psconvert", str(ps_file), "-A", f"-D{out_dir}", "-P"]
        if plt_fmt == "png":
            cmd.append("-Tg")
            cmd.append("-E200")
        elif plt_fmt == "pdf":
            cmd.append("-Tf")
        else:
            raise NotImplementedError

        call_gmt(cmd, tmpdir)


def call_gmt(
    cmd: typing.Sequence[str],
    tmpdir: Path,
    stdout: typing.Optional[typing.IO] = None,
) -> subprocess.CompletedProcess:
    """Run GMT with the specified arguments.

    cmd: list of arguments to pass to gmt. "gmt" is prepended to cmd
    inside here.

    tmpdir: the working directory for GMT to run in

    stdout: optional file-like object to write stdout to

    """
    gmt_cmd = ["gmt", *cmd]
    return subprocess.run(
        gmt_cmd,
        env={"GMT_TMPDIR": str(tmpdir)},
        stdout=stdout,
        stderr=subprocess.PIPE,
        check=True,
    )


if __name__ == "__main__":
    main()
    plt.show()
