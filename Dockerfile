# ASCAT UHR Dockerfile
#
# Build with:
#
# docker build -t richli/ascat_uhr2:latest \
#	--build-arg=build_date=$(date --utc --date="@${SOURCE_DATE_EPOCH:-$(date +%s)}" --rfc-3339='seconds') \
#	--build-arg=version=$(git rev-parse --short HEAD) \
#	--build-arg=revision=$(git symbolic-ref --short HEAD) \
#   .

# Build the SRF shared library
FROM debian:buster AS build

ENV LANG C.UTF-8
RUN apt-get --quiet update && \
    apt-get --quiet install -y gcc make libgsl-dev

WORKDIR /root
COPY srf/*.c srf/*.h srf/Makefile ./
RUN make ascat_srf_v4.so

# Package up the Python scripts with the SRF library
FROM debian:buster-slim

ENV LANG C.UTF-8
RUN apt-get --quiet update && \
    apt-get --quiet install -y \
        libgsl23 \
        python3 \
        python3-cffi \
        python3-numpy \
        python3-scipy \
        python3-h5py \
        python3-numexpr \
        python3-netcdf4 \
        python3-pyproj \
        python3-dateutil \
        python3-tqdm \
        python3-cartopy

WORKDIR /root/uhr
COPY uhr/*.py ./

COPY --from=build \
	/root/ascat_srf_v4.so \
	/root/srf/

ARG version
ARG revision
ARG build_date

LABEL Maintainer="Richard Lindsley <rich.lindsley@gmail.com>" \
    "org.opencontainers.image.title"="ASCAT UHR processor" \
    "org.opencontainers.image.description"="Produce ASCAT ultra-high resolution wind retrievals" \
    "org.opencontainers.image.created"="$build_date" \
    "org.opencontainers.image.authors"="Richard Lindsley <rich.lindsley@gmail.com>" \
    "org.opencontainers.image.url"="http://gitlab.com/richli/ascat_uhr2" \
    "org.opencontainers.image.source"="http://gitlab.com/richli/ascat_uhr2" \
    "org.opencontainers.image.version"="$version" \
    "org.opencontainers.image.revision"="$revision"

# Use commands like "python3 prepare_uhr.py ..."
CMD ["python3"]