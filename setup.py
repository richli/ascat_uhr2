from setuptools import setup, find_packages

from uhr.version import __version__

setup(
    name="uhr",
    version=__version__,
    description="ASCAT UHR processor",
    url="https://gitlab.com/richli/ascat_uhr2",
    author="Richard Lindsley",
    author_email="rich.lindsley@gmail.com",
    license="MIT",
    # https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.6",
        "Topic :: Scientific/Engineering",
    ],
    packages=find_packages(),
    install_requires=[
        "numpy",
        "scipy",
        "h5py",
        "netcdf4",
        "pyproj",
        "python-dateutil",
        "tqdm",
    ],
    python_requires=">=3.6",
)
