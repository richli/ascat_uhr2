/* (c) copyright 2014-2015 Richard D. Lindsley and David G. Long, Brigham Young University */
/*****************************************************************
  Filename:  test_srf.c

  This is a simple test driver for the ascat_srf library

  v0.1 RDL 2014-01-23:
    Started
  v0.5 RDL 2014-01-23:
    Uses a sample measurement, prints SRF at measurement center
  v1.0 RDL 2014-01-23:
    Pretty-prints the grid of SRF values
  v1.1 RDL 2014-01-23:
    Grid size is via argument now
  v1.2 RDL 2014-01-23:
    Use ANSI escape sequences to make the grid even prettier; add option to turn it off just in case
  v1.3 RDL 2014-01-23:
    Fix off-by-one errors in grid
  v1.4 RDL 2014-09-30:
    Update for new SRF interface
  v1.5 RDL 2014-10-17:
    Add benchmark mode
  v1.6 RDL 2014-10-23:
    Support v1, v2, v3, and v4 SRF interfaces
  v1.7 RDL 2014-12-11:
    For benchmarking, compute mean and standard deviation

******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <assert.h>
#include <unistd.h> // for isatty()
#include <stdbool.h>
#include <time.h>

#include <gsl/gsl_statistics_double.h>

#include "ascat_srf.h"

// Globals
const char *argp_program_bug_address = "<rlindsley@mers.byu.edu>";
const char *argp_program_version = "test_srf v1.7";
unsigned int VERBOSE = 0;

// Structs
typedef struct {
    int grid_n; // The grid size, in number of elements per side
    bool no_pretty; // Whether to force disabling the tty escape sequences
    bool benchmark; // Benchmark mode
} prog_opts_t;

// Helpful macros
#define FAIL { printf("*** fail at line %i\n",__LINE__); (void) exit(1); }
// #define WARN { printf("*** warning at line %i, continuing\n",__LINE__); }
#define FREE(p)   do { free(p); (p) = NULL; } while(0)
// I use C99 math.h for the fmin/fmax functions, but a C90 (ANSI C) version could use macros instead
// https://stackoverflow.com/questions/3437404/min-and-max-in-c

/********************************************************************/
// Function prototypes
static int parse_opt(int key, char *arg, struct argp_state *state);
static inline int ceil_divide(int x, int y);
static void print_grid(double ll_lat, double ll_lon, double ur_lat, double ur_lon, int nx, int ny, bool no_pretty);
static void run_benchmark();

/********************************************************************/
/*                Main function                                    */
/********************************************************************/
int main(int argc, char *argv[]) {
    prog_opts_t prog_opts;
    int stat;
    double cur_lat, cur_lon;
    double tl_lat, tl_lon, tr_lat, tr_lon, br_lat, br_lon, bl_lat, bl_lon;
    // double sig; // unused
    double inc;
    int swath, ascend;
    int node;
    double azi;
    double grid_ll_lat, grid_ll_lon, grid_ur_lat, grid_ur_lon;

    //----
    // Parse arguments
    //----
    struct argp_option argp_options[] = {
        {0,0,0,0,"Output options", 9},
        {0, 'n', "num", 0, "Number of grid elements per side (num x num), defaults to 6", 0},
        {"verbose", 'v', 0, 0, "Verbose output", 0},
        {"benchmark", 'b', 0, 0, "Benchmark mode (no output, but time SRF computation)", 0},
        {"no-pretty", 801, 0, 0, "Don't try any fancy console escaping", 0},
        {0,0,0,0,"Informational options", -1},
        {0, 'h', 0, OPTION_HIDDEN, "Help", 0},
        {0,0,0,0,0,0}
    };
    struct argp argp_in = {argp_options, parse_opt, " ", "Tests the ascat_srf library", 0, 0, 0};
    if (argp_parse(&argp_in, argc, argv, 0, 0, &prog_opts) != 0)
        return(EXIT_FAILURE);

    fprintf(stdout, "%s running\n", argp_program_version);

    if (prog_opts.benchmark) {
        run_benchmark();
        return(EXIT_SUCCESS);
    }

    //----
    // Prep ascat_srf
    //----
    fprintf(stdout, "Preparing ascat_srf\n");
    srf_init();

    //----
    // Try ascat_srf for a few values
    //----
    // These values come from 2011-299, filename: ASCA_SZF_1B_M02_20111026000000Z_20111026000259Z_N_O_20111026002858Z__20111026002952
    // MDR: 0
    // node: 1346 swath: 6 index: 66 lat/lon: -28.530554 133.291197 sigma0: -15.223281 inc: 39.480000 azi: 150.120000
    // node: 1347 swath: 6 index: 67 lat/lon: -28.499347 133.271074 sigma0: -15.601757 inc: 39.680000 azi: 150.130000
    // node: 1348 swath: 6 index: 68 lat/lon: -28.467672 133.250658 sigma0: -16.628581 inc: 39.890000 azi: 150.140000
    //
    // The lat/lon/inc data from L1B files is only single-precision, but I
    // use doubles in ascat_srf.c to minimize any loss in accuracy for all
    // the computations.
    //
    cur_lat = -28.499347;
    cur_lon = 133.271074;
    //sig = -15.601757; // unused
    inc = 39.680000;
    swath = 6; // note 1-based indexing
    ascend = 1; // Just a guess
    node = 67;
    azi = 150.130000;

    // Initialize the SRF for the given measurement
    srf_meas(cur_lat, cur_lon, inc, azi, swath, ascend, node);

    // Find a bounding box for the lat/lon extent we could use. This is big
    // enough to include the SRF 3 dB down from the peak.
    srf_latlon_extent(&tl_lat, &tl_lon, &tr_lat, &tr_lon, &br_lat, &br_lon,
            &bl_lat, &bl_lon, &stat);

    printf("For the sample measurement at lat/lon %f %f:\n", cur_lat, cur_lon);
    printf(" Bounding box is:\n");
    printf(" top-left (%f %f) top-right (%f %f)\n", tl_lat, tl_lon, tr_lat, tr_lon);
    printf(" bottom-left (%f %f) bottom-right (%f %f)\n", bl_lat, bl_lon, br_lat, br_lon);

    // Now return some SRF values
    // How about at the measurement location?
    // (It's fine that it's not 1.0, the SRF values are not normalized here but
    // could be normalized later)
    printf("SRF at measurement location: %f\n", srf_eval_ll(cur_lat, cur_lon));

    // Chop up the box into a grid and return the SRF value for each grid center
    // Note that the bounding box is not exactly north/south, it's tilted, so
    // we make a grid that includes the maximum lat/lon. The grid is defined by
    // the lower-left and upper-right corners and has a spacing of dlat/dlon.
    grid_ll_lat = fmin(fmin(tl_lat, bl_lat), fmin(tr_lat, br_lat));
    grid_ll_lon = fmin(fmin(tl_lon, bl_lon), fmin(tr_lon, br_lon));
    grid_ur_lat = fmax(fmax(tl_lat, bl_lat), fmax(tr_lat, br_lat));
    grid_ur_lon = fmax(fmax(tl_lon, bl_lon), fmax(tr_lon, br_lon));

    print_grid(grid_ll_lat, grid_ll_lon, grid_ur_lat, grid_ur_lon, prog_opts.grid_n, prog_opts.grid_n, prog_opts.no_pretty);

    //----
    // Clean up
    //----
    fprintf(stdout, "Cleaning up ascat_srf\n");
    srf_done();

    return(EXIT_SUCCESS);
}

/********************************************************************/
/*                Main helper functions                            */
/********************************************************************/

static inline int ceil_divide(int x, int y) {
    // Returns the ceiling after integer division
    // https://stackoverflow.com/questions/2745074/fast-ceiling-of-an-integer-division-in-c-c?rq=1
    // Doesn't check for division by zero or anything fancy
    return (x + y - 1) / y;
}

static int parse_opt(int key, char *arg, struct argp_state *state) {
    prog_opts_t *p_opts = state->input;

    switch(key) {
        case ARGP_KEY_INIT:
            p_opts->grid_n = 6;
            p_opts->no_pretty = false;
            p_opts->benchmark = false;
            break;
        case 'v':
            VERBOSE = 1;
            break;
        case 'b':
            p_opts->benchmark = true;
            break;
        case 'h':
            argp_state_help(state, stdout, ARGP_HELP_STD_HELP);
            break;
        case 'n':
            p_opts->grid_n = atoi(arg);
            if (p_opts->grid_n < 0)
                argp_failure(state, 1, 0, "Grid size (%s) must be positive", arg);
            break;
        case 801:
            p_opts->no_pretty = true;
            break;
        case ARGP_KEY_ARG:
            argp_state_help(state, stdout, ARGP_HELP_SHORT_USAGE);
            argp_failure(state, 1, 0, "Too many arguments");
            break;
        case ARGP_KEY_END:
            break;
    }

    return 0;
}

static void print_grid(double ll_lat, double ll_lon, double ur_lat, double ur_lon, int nx, int ny, bool no_pretty) {
    // Print the grid
    //
    // ll_lat/lon:  the location of the lower-left corner
    // ur_lat/lon:  the location of the upper-right corner
    // nx/ny: how many elements in x/y
    // no_pretty: whether to disable terminal escaping or not
    //
    int grid_x, grid_y;
    double grid_lat, grid_lon;
    double dlat, dlon;
    double srf_val;

    dlat = (ur_lat - ll_lat) / (ny - 1);
    dlon = (ur_lon - ll_lon) / (nx - 1);

    if (VERBOSE > 0) {
        // Go through the grid, from top to bottom, left to right (grid location (0,0) is the lower-left corner)
        for (grid_y = ny - 1; grid_y >= 0; grid_y--) {
            for (grid_x = 0; grid_x < nx; grid_x++) {
                // Find the lat/lon of the center of the grid point, not the lower-left corner of the grid box
                grid_lat = ll_lat + (grid_y + 0.5) * dlat;
                grid_lon = ll_lon + (grid_x + 0.5) * dlon;
                srf_val = srf_eval_ll(grid_lat, grid_lon);
                printf("grid: (%d, %d) lat/lon: (%f %f) SRF: %f\n", grid_x, grid_y, grid_lat, grid_lon, srf_val);
            }
        }
    }

    // Do the grid again, but print it pretty this time
    //
    // These ANSI character escape sequences aren't portable, but it's nice if they work
    // https://en.wikipedia.org/wiki/ANSI_escape_code
    char *tty_inverse = "\033[7m";
    //char *tty_bold = "\033[1m";
    char *tty_faint = "\033[2m"; // not always supported
    char *tty_normal = "\033[0m";

    double thresh_val = 0.43; // what SRF value is at the 3 dB threshold: (db2pow(pow2db(max_val) - 3)), maxval = 0.86

    printf("SRF values on a %dx%d grid:\n", nx, ny);
    for (grid_y = ny - 1; grid_y >= 0; grid_y--) {
        for (grid_x = 0; grid_x < nx; grid_x++) {
            // Find the lat/lon of the center of the grid point, not the lower-left corner of the grid box
            grid_lat = ll_lat + (grid_y + 0.5) * dlat;
            grid_lon = ll_lon + (grid_x + 0.5) * dlon;
            srf_val = srf_eval_ll(grid_lat, grid_lon);
            if (srf_val < 1e-4)
                printf("  -  ");
            else {
                // Pretty prints if stdout is a tty unless we've disabled it
                // Print the centermost grid element in inverse mode
                if (isatty(1) && (grid_x + 1 == ceil_divide(nx, 2) && grid_y + 1 == ceil_divide(ny, 2)) && !no_pretty)
                    printf("%s%.2f%s ", tty_inverse, srf_val, tty_normal);
                // Print grid elements above the threshold as normal
                else if (isatty(1) && srf_val > thresh_val && !no_pretty)
                    printf("%s%.2f%s ", tty_normal, srf_val, tty_normal);
                // Print grid elements below the threshold as faint
                else if (isatty(1) && !no_pretty)
                    printf("%s%.2f%s ", tty_faint, srf_val, tty_normal);
                // Fallback mode, just print the value
                else
                    printf("%.2f ", srf_val);
            }
        }
        printf("\n");
    }

    return;
}

static void run_benchmark() {
    // Benchmark timings
    int i;
    const int runs = 50;
    const int runs_long = 1000;
    struct timespec time_start, now;
    double dt;
    double func_time_mean, func_time_std;
    double time_per_meas;
    double time_results[runs];
    double time_results_long[runs_long];

    double lat, lon, inc;
    int beam, ascend;
    double azi;
    int node;
    double tl_lat, tl_lon, tr_lat, tr_lon, bl_lat, bl_lon, br_lat, br_lon;
    int stat;
    double __attribute__ ((unused)) srf_val;

    printf("Benchmark mode using %d runs\n", runs);

    //-------------
    printf(" Timing srf_init/srf_done:");
    for (i=0; i<runs; i++) {
        clock_gettime(CLOCK_MONOTONIC, &time_start);

        srf_init();
        srf_done();

        clock_gettime(CLOCK_MONOTONIC, &now);
        dt = difftime(now.tv_sec, time_start.tv_sec) + (now.tv_nsec - time_start.tv_nsec) / 1.0e9;
        // Store time in microseconds
        time_results[i] = dt * 1e6;
    }
    func_time_mean = gsl_stats_mean(time_results, 1, runs);
    func_time_std = gsl_stats_sd_m(time_results, 1, runs, func_time_mean);
    printf(" %0.4g +/- %0.4g µs/run\n", func_time_mean, func_time_std);

    //-------------
    printf(" Timing srf_meas:");
    lat = -28.499347;
    lon = 133.271074;
    inc = 39.680000;
    beam = 6;
    ascend = 1;
    node = 67;
    azi = 150.130000;
    srf_init();

    for (i=0; i<runs; i++) {
        clock_gettime(CLOCK_MONOTONIC, &time_start);

        srf_meas(lat, lon, inc, azi, beam, ascend, node);

        clock_gettime(CLOCK_MONOTONIC, &now);
        dt = difftime(now.tv_sec, time_start.tv_sec) + (now.tv_nsec - time_start.tv_nsec) / 1.0e9;
        // Store time in milliseconds
        time_results[i] = dt * 1e3;
    }
    func_time_mean = gsl_stats_mean(time_results, 1, runs);
    func_time_std = gsl_stats_sd_m(time_results, 1, runs, func_time_mean);
    printf(" %0.4g +/- %0.4g ms/run\n", func_time_mean, func_time_std);

    // one srf_meas() per measurement (units: ms)
    time_per_meas = func_time_mean;

    // Do it again for the functions below
    srf_meas(lat, lon, inc, azi, beam, ascend, node);

    //-------------
    printf(" Timing srf_latlon_extent:");
    for (i=0; i<runs; i++) {
        clock_gettime(CLOCK_MONOTONIC, &time_start);

        srf_latlon_extent(&tl_lat, &tl_lon, &tr_lat, &tr_lon,
                &br_lat, &br_lon, &bl_lat, &bl_lon, &stat);

        clock_gettime(CLOCK_MONOTONIC, &now);
        dt = difftime(now.tv_sec, time_start.tv_sec) + (now.tv_nsec - time_start.tv_nsec) / 1.0e9;
        // Store time in microseconds
        time_results[i] = dt * 1e6;
    }
    func_time_mean = gsl_stats_mean(time_results, 1, runs);
    func_time_std = gsl_stats_sd_m(time_results, 1, runs, func_time_mean);
    printf(" %0.4g +/- %0.4g µs/run\n", func_time_mean, func_time_std);

    // one srf_latlon_extent() per measurement (units: ms)
    time_per_meas += func_time_mean / 1e3;

    //-------------
    printf(" Timing srf_eval_ll (runs=%d):", runs_long);
    for (i=0; i<runs_long; i++) {
        clock_gettime(CLOCK_MONOTONIC, &time_start);

        srf_val = srf_eval_ll(lat, lon);

        clock_gettime(CLOCK_MONOTONIC, &now);
        dt = difftime(now.tv_sec, time_start.tv_sec) + (now.tv_nsec - time_start.tv_nsec) / 1.0e9;
        // Store time in microseconds
        time_results_long[i] = dt * 1e6;
    }
    func_time_mean = gsl_stats_mean(time_results_long, 1, runs_long);
    func_time_std = gsl_stats_sd_m(time_results_long, 1, runs_long, func_time_mean);
    printf(" %0.4g +/- %0.4g µs/run\n", func_time_mean, func_time_std);

    // 10^2 srf_eval_ll() per measurement (units: ms)
    time_per_meas += 100 * func_time_mean / 1e3;


    //-------------
    printf(" Timing srf_eval_xy (runs=%d):", runs_long);
    for (i=0; i<runs_long; i++) {
        clock_gettime(CLOCK_MONOTONIC, &time_start);

        srf_val = srf_eval_xy(lat, lon);

        clock_gettime(CLOCK_MONOTONIC, &now);
        dt = difftime(now.tv_sec, time_start.tv_sec) + (now.tv_nsec - time_start.tv_nsec) / 1.0e9;
        // Store time in microseconds
        time_results_long[i] = dt * 1e6;
    }
    func_time_mean = gsl_stats_mean(time_results_long, 1, runs_long);
    func_time_std = gsl_stats_sd_m(time_results_long, 1, runs_long, func_time_mean);
    printf(" %0.4g +/- %0.4g µs/run\n", func_time_mean, func_time_std);

    // //-------------
    // runs = 50;
    // // Note the use of C99 variable length arrays
    // int vec_len = 20;
    // double lat_vec[vec_len], lon_vec[vec_len], val_vec[vec_len];
    // for (i=0; i<vec_len; i++) {
    //     lat_vec[i] = lat;
    //     lon_vec[i] = lon;
    // }

    // printf(" Timing srf_compute_vec for len=%d (runs=%d):", vec_len, runs);
    // clock_gettime(CLOCK_MONOTONIC, &time_start);
    // for (i=0; i<runs; i++) {
    //     srf_compute_vec(meas, lat_vec, lon_vec, vec_len, val_vec);
    // }
    // clock_gettime(CLOCK_MONOTONIC, &now);
    // dt = difftime(now.tv_sec, time_start.tv_sec) + (now.tv_nsec - time_start.tv_nsec) / 1.0e9;
    // printf(" %0.4g ms/run (%0.4g ms total)\n", dt/runs*1e3, dt*1e3);

    printf(" Assuming 100 ll lookups, expected time per measurement: %0.4g ms\n", time_per_meas);

    srf_done();
    return;
}

// vim:softtabstop=4:tabstop=4:shiftwidth=4:
