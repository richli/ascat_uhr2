/* (c) copyright 2014-2015 Richard D. Lindsley and David G. Long, Brigham Young University */
/*****************************************************************
Filename:  ascat_srf_v4.c

This is a library meant to return values from the ASCAT spatial response function (SRF)

This is for the version 4 SRF: the SRF is full-valued, the along-beam and
cross-beam responses are from a polynomial fit to the composite (i.e.
incorporating along-track pulse averaging) responses, and the SRF is rotated
to account for Doppler.

Since Doppler rotation is used before along-track pulse averaging, it makes
it a bit harder to predict the SRF size (3 dB extent) and the shape. However,
in the coordinate system where the origin is at the measurement center and
the axes are aligned with and perpendicular to the gradient of the
discriminator frequency, to a very good approximation, the SRF is an ellipse,
separable in the "x" and "y" directions (x and y in this coordinate system,
not the tanplane (northing/easting) or the track coordinate system).

We thus can take 1D "cuts" of the SRF in the along- and cross-disc. freq.
directions. The SRF shape is symmetric around the measurement center and is
modeled here as explained below. The SRF length and width is the 3 dB extent
of the cuts.

The rotation angle, which I call theta, is a function of incidence angle and
node (or along-beam index). This is the net or cumulative rotation angle:

theta = alpha + beta + psi

alpha is the Doppler-induced rotation angle. beta is the angle to go from track
to beam and is therefore a constant with respect to beam number. psi is the
angle between track and North and can be determined from the reported azimuth
angle. Since psi varies quite a bit at high latitudes, it (and beta) are not
modeled. The angle alpha is modeled with a fourth-order polynomial fit:

alpha(node, lat) = a00 + a01 node + a02 node^2 + ... + a10 lat + a11 node * lat + ...

Thus there are (4+1)^2 = 25 coefficients.

The coefficients are separately computed for each of the six beams and for
ascending/descending passes (12 cases total). Before evaluation the node and
latitude are adjusted by scaling and offset (this is how the coefficients were
determined, in order to improve the matrix condition number).

The SRF shape is a function of node (or along-beam index) and latitude.
The model is a two-layered polynomial fit:

SRF_along(node, lat; x) = alpha_0(node, lat) + alpha_2(node, lat) x^2 + alpha_4(node, lat) x^4
alpha_0(n, l) = a00 + a01 n + a02 n^2 + a10 l + a11 nl + a12 nl^2 + a20 l^2 + ...
alpha_4(n, l) = a00 + a01 n + a02 n^2 + a10 l + a11 nl + a12 nl^2 + a20 l^2 + ...
alpha_6(n, l) = a00 + a01 n + a02 n^2 + a10 l + a11 nl + a12 nl^2 + a20 l^2 + ...

Each alpha coefficient is a 2nd order polynomial surface. The SRF is a
biquadratic (4th order with no odd terms) polynomial curve. And similarly for
SRF_cross(node, lat, y). node varies from 33 to 224, lat from -90 to 90
degrees, and x or y is the distance in km from the measurement center in the
along- or cross-directions.

The SRF shapes are modeled better by polynomial fits in dB space than linear
space, due to the "tail" effect in linear space. So the SRF_along(n, l; x) and
SRF_cross(n, l; y) return the SRF value in dB space, not linear. The SRF fit is
constrained so that it only goes down to -15 dB below the peak (center) value,
or about 0.03 in linear space. This is more than adequate for our application
and saves us from requiring a higher order polynomial fit.

While the polynomial fits do not *exactly* represent the SRF, they are a decent
approximation and it is much faster to do the polynomial evaluations than to
compute the full response, rotate with Doppler, incorporate along-track pulse
averaging, etc. Also the C implementation here is much simpler.

Other code is used to exactly compute the SRF and compute the polynomial
fits. The results from those fits are used here.

v0.1 2014-08-20 RDL:
    Started v4 SRF, based on v3 SRF
v0.2 2014-09-12 RDL:
    Revised to use polynomial evaluations throughout
v0.3 2014-10-24 RDL:
    Revised to use new model
v0.4 2014-11-06 RDL:
    Use the parameterized angle alpha (Doppler only) rather than theta (cumulative rotation)

******************************************************************/

/********************************************************************/
// Documentation
//
// This is compiled as a library (static or shared) and called to compute and
// return values from the ASCAT SRF
//
// It provides the public functions:
//
// srf_init(): Prepares the static global data
// srf_meas(): Given the measurement data, get the SRF ready
// srf_latlon_extent(): Determine the extent of the measurement SRF in terms of lat/lon
// srf_eval_ll(): For a given lat/lon, compute the SRF at that location
// srf_eval_xy(): For a given location on the tangent plane, compute the SRF at that location
// srf_done(): Frees the cache
//
// Some private functions exist behind the scenes.
// Also some structs are used to contain all of the relevant data together.
//
// Typical usage is to call srf_init() once, then call srf_meas() and
// srf_latlon_extent() once for each measurement. Call srf_eval_ll() as
// often as needed for each measurement. Finally, call srf_done() at the
// end to clean things up.
//
// NOTE that this library is *not* thread-safe since it relies on a few static
// global variables. This was a compromise for easier Fortran interoperability,
// but with some minor modifications, it could be thread-safe.
//
// Some background on the below computation follows.
//
// A locally tangent plane is created, centered on each measurement. Initially
// this plane is oriented so that the vertical axis is northing and the
// horizontal axis is easting. A rotation angle (theta) is used to rotate from
// this coordinate system to the system that is along- and cross- the
// discriminator frequency gradient.
//
/********************************************************************/

// #define VERBOSE 1 // verbosity levels: {0: quiet 1: verbose 5: debug}

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <math.h>  // Needed for log10, pow, trig functions, etc

// GNU Scientific Library
#include <gsl/gsl_errno.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_poly.h>

#include "ascat_srf.h"

// Coefficient values
#include "v4_interp_vals.h"


// Helpful macros
#define FAIL { printf("*** fail in %s, %s() at line %d\n", __FILE__, __FUNCTION__, __LINE__); exit(EXIT_FAILURE); }
// #define WARN { printf("*** warning at line %i, continuing\n",__LINE__); }
#define FREE(p)   do { free(p); (p) = NULL; } while(0)
#define ARR_LEN( arr ) sizeof( arr ) / sizeof( arr[0] )

/********************************************************************/
// Structures

// Used for latitude/longitude
typedef struct {
    double lat, lon;
} srf_ll;

// Used for tangent plane coords. All the extra data is for inverse mapping to lat/lon.
typedef struct {
    double x, y;
    double R_lat, R_earth;
    srf_ll center_ll;
} srf_tp;

// Saves data that is common to all measurements
// Note that 12 copies of this variable exist: two for each of the six beams,
// one for ascending, one for descending.
// The current measurement (in the node_data variable) points to the
// appropriate one.
typedef struct {
    // Each points to the appropriate array of polynomial coefficients
    const double *alpha_lat_mu;
    const double *alpha_lat_sig;
    const double *alpha_node_mu;
    const double *alpha_node_sig;
    const double *alpha_c;
    const double *lat_mu;
    const double *lat_sig;
    const double *node_mu;
    const double *node_sig;
    const double *along_alpha_0_c;
    const double *along_alpha_2_c;
    const double *along_alpha_4_c;
    const double *cross_alpha_0_c;
    const double *cross_alpha_2_c;
    const double *cross_alpha_4_c;

    // The length of the arrays
    int alpha_poly_len;
    int along_alpha_0_poly_len;
    int along_alpha_2_poly_len;
    int along_alpha_4_poly_len;
    int cross_alpha_0_poly_len;
    int cross_alpha_2_poly_len;
    int cross_alpha_4_poly_len;
} srf_cache;

// Contains all of the internal data needed for the measurement
// This saves re-computing lots of stuff for every function call
// Note that only ONE of these variables/objects is created, and it is created
// as a static global, meas_data
typedef struct {
    srf_ll cur_ll;        // The measurement location in lat/lon
    srf_tp tp;            // The tanplane for the current measurement
    double inc;           // Measurement incidence angle
    double azi;           // Measurement azimuth angle
    int beam;             // Beam is 1-based (1 to 6)
    int ascend;           // Ascending pass (1) or descending pass (0)
    int node;             // Node, or along-beam index. Ideally goes from 1 to 256, but the first and last 32 nodes are not used, so it varies from 33 to 224.
    double theta;         // Rotation angle from N/E to discriminator frequency gradient (radians)
    srf_cache *srf_poly;  // Which srf_cache to use
    // The next two are for normalization purposes
    double srf_along_max; // Maximum value (at SRF center) in the along-di direction
    double srf_cross_max; // Maximum value (at SRF center) in the cross-di direction
    double srf_width, srf_length; // The approximate half-width of the SRF in either direction
    // Cache the SRF shape polynomial cofficients
    double alpha_along[5], alpha_cross[5];
} srf_dat;


/********************************************************************/
// Function prototypes (private functions)

static inline double db2pow(double db);
static inline double pow2db(double pow);
static inline double deg2rad(double x);
static inline double rad2deg(double x);
static inline double sind(double x);
static inline double cosd(double x);
static inline double asind(double x);
static inline double square(double x);
static inline double ipow(double x, int pow);
static double eval_surface(double x, double y, double const * const c, int order);
static void rotate_coords(double x, double y, double theta, double *x_rot, double *y_rot);

// static void Dprintf(const int level, const char* format, ...);

static void find_beam_info();
static double poly_width(double *coef, int c_len);
static srf_tp latlon2tanplane(srf_ll cen_ll, srf_ll p_ll);
static srf_ll tanplane2latlon(srf_tp tp);

static double compute_srf_along(double x);
static double compute_srf_cross(double y);

/********************************************************************/
// Constants
// Rm is the mean equatorial radius of the earth (km)
static const double Rm = 6378.1363;
// epsilon is the flattening constant
static const double epsilon = 1/298.257;
// Nominal altitude of ASCAT (km)
// static const double avgH = 820;

/********************************************************************/
// File-level global variables
static srf_cache *srf_caches; // should be one of these for each of the six beams, asc/des combination (12 total)
static srf_dat meas_data;     // pertinent info for the measurement we're currently on
// #ifdef DUMP_SRFSIZE
// static FILE *srf_ab_side_log, *srf_ab_mid_log, *srf_cb_side_log, *srf_cb_mid_log;
// static gsl_histogram *along_size_side_hist, *cross_size_side_hist, *along_size_mid_hist, *cross_size_mid_hist;
// #endif

/********************************************************************/
// Fortran interface
//
// Prepare the cache by calling srf_init_
// Call srf_meas_ for each node, then call srf_latlon_extent_ and
// srf_eval_ll_ repeatedly.
// When finished, call srf_done_.
//
// NB: Fortran passes by reference, so the fortran interface makes this
// adjustment

void srf_init_() {
    srf_init();
    return;
}

void srf_meas_(double *lat, double *lon, double *inc, double *azi, int *swath, int *ascend, int *node) {

    srf_meas(*lat, *lon, *inc, *azi, *swath, *ascend, *node);
    return;
}

void srf_done_() {
    srf_done();
    return;
}

void srf_latlon_extent_(double *tl_lat, double *tl_lon, double *tr_lat,
        double *tr_lon, double *br_lat, double *br_lon, double *bl_lat, double *bl_lon,
        int *stat) {
    srf_latlon_extent(tl_lat, tl_lon, tr_lat, tr_lon, br_lat, br_lon, bl_lat,
            bl_lon, stat);
    return;
}

double srf_eval_ll_(double *lat, double *lon) {
    return srf_eval_ll(*lat, *lon);
}

double srf_eval_xy_(double *x, double *y) {
    return srf_eval_xy(*x, *y);
}

/********************************************************************/
// Functions

// Some helper functions
static double db2pow(double db) {
    // Given dB value, return power (linear) value
    return pow(10.0, (db/10.0));
}

static double pow2db(double pow) {
    // Given linear power value, return dB value
    return 10.0*log10(pow);
}

static inline double deg2rad(double x) {
    // Convert degrees to radians
    // return x * M_PI/180.0;
    // Avoid a division by precalculating pi/180
    return x * 0.017453292519943295769;
}

static inline double rad2deg(double x) {
    // Convert radians to degrees
    // return x * 180.0/M_PI;
    // Avoid a division by precalculating 180/pi
    return x * 57.29577951308232087679;
}

static inline double sind(double x) {
    // Returns the sine, assuming x is in degrees
    return sin(deg2rad(x));
}

static inline double cosd(double x) {
    // Returns the cosine, assuming x is in degrees
    return cos(deg2rad(x));
}

static inline double asind(double x) {
    // Returns the arcsine in degrees
    return rad2deg(asin(x));
}

static inline double acosd(double x) {
    // Returns the arccosine in degrees
    return rad2deg(acos(x));
}

static inline double square(double x) {
    // The pow() function from math.h can be expensive, so I use this instead
    return x*x;
}

static inline double ipow(double x, int pow) {
    // Side-stepping pow() from math.h if we know we're using integer powers
    int i;
    double val;
    val = 1; // x^0
    for (i=1; i<=pow; i++)
        val *= x;

    return val;
}

static void rotate_coords(double x, double y, double theta, double *x_rot, double *y_rot) {
    // Rotates x,y by theta radians
    // Return values are passed by reference through x_rot, y_rot
    *x_rot = x*cos(theta) - y*sin(theta);
    *y_rot = x*sin(theta) + y*cos(theta);
    return;
}

// static void Dprintf(const int level, const char* format, ...) {
//     // Debug-printf
//     // Only printf's if level >= VERBOSE
//     // Adapted from <http://www.ozzu.com/cpp-tutorials/tutorial-writing-custom-printf-wrapper-function-t89166.html>
//     va_list args;
//     // 5 is debug level
//     if (level <= VERBOSE) {
//         if (level >= 5) {
//             fprintf(stdout, "DEBUG: ");
//         }
//         va_start(args, format);
//         vfprintf(stdout, format, args);
//         va_end(args);
//     }
//     //fprintf(stdout, "\n");
//     //NB, I could use stderr instead of stdout but this does weird things when I pipe to less
//     return;
// }

/*********************************************/
// The tangent plane functions
/*********************************************/
static srf_tp latlon2tanplane(srf_ll cen_ll, srf_ll p_ll) {
    /* Convert a lat/lon coord to tanplane coords
     *
     * Refer to http://www.mers.byu.edu/docs/reports/MERS9904.pdf
     *
     * This returns, in x-y coords (in km), the distance on a locally tangent
     * plane from the center given in cen_ll, to a point at p_ll
     *
     */
    double R_earth, R_lat;
    double A, B, C;
    srf_tp p_tp;

    // local radius of the earth
    R_earth = Rm*(1-epsilon*square(sind(p_ll.lat)));

    // radius of local latitude line
    R_lat = R_earth*cosd(p_ll.lat);
    //Dprintf(5, "Rm=%.18f, epsilon=%.18f, R_earth=%.18f, R_lat=%.18f, sind(lat)=%.18f, cosd(lat)=%.18f\n", Rm, epsilon, R_earth, R_lat, sind(p_ll.lat), cosd(p_ll.lat));

    // calculate A,B,C
    A = R_earth * sind(p_ll.lat-cen_ll.lat);
    B = R_lat * ( 1 - cosd( p_ll.lon-cen_ll.lon ) ) * sind(cen_ll.lat);
    C = R_lat * sind(p_ll.lon-cen_ll.lon);
    //Dprintf(5, "A=%f B=%f C=%f\n", A, B, C);

    // Return tanplane location
    p_tp.x = C;
    p_tp.y = A+B;
    p_tp.R_earth = R_earth;
    p_tp.R_lat = R_lat;
    p_tp.center_ll = cen_ll;

    return p_tp;
}

static srf_ll tanplane2latlon(srf_tp tp) {
    /* Convert a tanplane coord to lat/lon
     *
     * The srf_tp struct holds the center lat/lon and other information needed to
     * reverse the mapping from tanplane to lat/lon
     *
     * We need to know R_lat, but that depends on the final lat. So we
     * iterate a few times until we get a better estimate for R_lat. This is
     * adjusted so it only iterates twice.
     *
     */

    double delta_lon, delta_lat;
    double R_lat, R_earth;
    double new_R_lat, new_R_earth;
    srf_ll p;
    // int it;

    p = tp.center_ll;

    // Initialize with the R_lat from tp
    R_lat = tp.R_lat;
    R_earth = tp.R_earth;

    // for (it=0; it < 2; it++) {
    //     // Calculate delta lat/lon
    //     delta_lon = asind(tp.x/R_lat);
    //     delta_lat = asind( (tp.y - (1-cosd(delta_lon)) *
    //                 sind(tp.center_ll.lat)*R_lat ) / R_earth );

    //     // Compute new R_lat, R_earth
    //     new_R_earth = Rm*(1-epsilon*square(sind(p.lat + delta_lat)));
    //     new_R_lat = new_R_earth*cosd(p.lat + delta_lat);

    //     // This is where I could check for convergence, but I'm iterating a static number of times

    //     R_earth = new_R_earth;
    //     R_lat = new_R_lat;
    // }

    // This version essentially unrolls the loop above but doesn't re-compute R_earth and R_lat at the last iteration
    // Iteration 1
    // Calculate delta lat/lon
    delta_lon = asind(tp.x/R_lat);
    delta_lat = asind( (tp.y - (1-cosd(delta_lon)) *
                sind(tp.center_ll.lat)*R_lat ) / R_earth );

    // Compute new R_lat, R_earth
    new_R_earth = Rm*(1-epsilon*square(sind(p.lat + delta_lat)));
    new_R_lat = new_R_earth*cosd(p.lat + delta_lat);

    R_earth = new_R_earth;
    R_lat = new_R_lat;

    // Iteration 2
    // Calculate delta lat/lon
    delta_lon = asind(tp.x/R_lat);
    delta_lat = asind( (tp.y - (1-cosd(delta_lon)) *
                sind(tp.center_ll.lat)*R_lat ) / R_earth );

    // Done with iterations
    p.lat += delta_lat;
    p.lon += delta_lon;
    return p;
}

static double eval_surface(double x, double y, double const * const c, int order) {
    // Evaluate the polynomial surface
    //
    // The function f(x, y) is evaluated using the coefficients in c.
    // For a fit_order==2, for example:
    // z(x, y) = a00 + a01 x + a02 x^2 +
    //     a10 y + a11 x*y + a12 x^2 y +
    //     a20 y^2 + a21 x*y^2 + a22 x^2 y^2
    // c=[a00, a01, a02, ..., a10, ...]
    //
    // x and y are scalars. The c array is (order+1)^2 elements long.
    //
    // I evaluate the polynomial using a dot product with the coefficient array. Alternately I could use Horner's method. I use the GSL interface to BLAS.

    gsl_vector *A_vec;
    double val;
    int c_len, i, j, m;

    c_len = square(order + 1);
    A_vec = gsl_vector_alloc(c_len);

    gsl_vector_const_view c_vec = gsl_vector_const_view_array(c, c_len);

    m = 0;
    for (j=0; j<order+1; j++) {
        for (i=0; i<order+1; i++) {
            gsl_vector_set(A_vec, m, ipow(x, i) * ipow(y, j));
            m++;
        }
    }

    // Dot product
    gsl_blas_ddot(A_vec, &c_vec.vector, &val);

    gsl_vector_free(A_vec);

    return val;
}

/*********************************************/

// The primary functions
void srf_init() {
    // This function is a bit tedious. All it does it initialize the cache, which just is a bunch of pointers to the correct coefficients for each combination of beam and asc/des.
    //
    // Also it allocates space for a couple of GSL vectors
    //
    // NOTE some values here are hardcoded

    // Initializes the srf_caches variables for each of the size beams, asc/des
    srf_caches = malloc(sizeof(srf_cache) * 6 * 2);

    // Alpha (Doppler rotation) coefficients
    // Beam 1 ascend
    srf_caches[0].alpha_poly_len = 25;
    srf_caches[0].alpha_lat_mu = &b1_asc_alpha_lat_mu;
    srf_caches[0].alpha_lat_sig = &b1_asc_alpha_lat_sig;
    srf_caches[0].alpha_node_mu = &b1_asc_alpha_node_mu;
    srf_caches[0].alpha_node_sig = &b1_asc_alpha_node_sig;
    srf_caches[0].alpha_c = b1_asc_alpha_c;
    // Beam 1 descend
    srf_caches[1].alpha_poly_len = 25;
    srf_caches[1].alpha_lat_mu = &b1_des_alpha_lat_mu;
    srf_caches[1].alpha_lat_sig = &b1_des_alpha_lat_sig;
    srf_caches[1].alpha_node_mu = &b1_des_alpha_node_mu;
    srf_caches[1].alpha_node_sig = &b1_des_alpha_node_sig;
    srf_caches[1].alpha_c = b1_des_alpha_c;
    // Beam 2 ascend
    srf_caches[2].alpha_poly_len = 25;
    srf_caches[2].alpha_lat_mu = &b2_asc_alpha_lat_mu;
    srf_caches[2].alpha_lat_sig = &b2_asc_alpha_lat_sig;
    srf_caches[2].alpha_node_mu = &b2_asc_alpha_node_mu;
    srf_caches[2].alpha_node_sig = &b2_asc_alpha_node_sig;
    srf_caches[2].alpha_c = b2_asc_alpha_c;
    // Beam 2 descend
    srf_caches[3].alpha_poly_len = 25;
    srf_caches[3].alpha_lat_mu = &b2_des_alpha_lat_mu;
    srf_caches[3].alpha_lat_sig = &b2_des_alpha_lat_sig;
    srf_caches[3].alpha_node_mu = &b2_des_alpha_node_mu;
    srf_caches[3].alpha_node_sig = &b2_des_alpha_node_sig;
    srf_caches[3].alpha_c = b2_des_alpha_c;
    // Beam 3 ascend
    srf_caches[4].alpha_poly_len = 25;
    srf_caches[4].alpha_lat_mu = &b3_asc_alpha_lat_mu;
    srf_caches[4].alpha_lat_sig = &b3_asc_alpha_lat_sig;
    srf_caches[4].alpha_node_mu = &b3_asc_alpha_node_mu;
    srf_caches[4].alpha_node_sig = &b3_asc_alpha_node_sig;
    srf_caches[4].alpha_c = b3_asc_alpha_c;
    // Beam 3 descend
    srf_caches[5].alpha_poly_len = 25;
    srf_caches[5].alpha_lat_mu = &b3_des_alpha_lat_mu;
    srf_caches[5].alpha_lat_sig = &b3_des_alpha_lat_sig;
    srf_caches[5].alpha_node_mu = &b3_des_alpha_node_mu;
    srf_caches[5].alpha_node_sig = &b3_des_alpha_node_sig;
    srf_caches[5].alpha_c = b3_des_alpha_c;
    // Beam 4 ascend
    srf_caches[6].alpha_poly_len = 25;
    srf_caches[6].alpha_lat_mu = &b4_asc_alpha_lat_mu;
    srf_caches[6].alpha_lat_sig = &b4_asc_alpha_lat_sig;
    srf_caches[6].alpha_node_mu = &b4_asc_alpha_node_mu;
    srf_caches[6].alpha_node_sig = &b4_asc_alpha_node_sig;
    srf_caches[6].alpha_c = b4_asc_alpha_c;
    // Beam 4 descend
    srf_caches[7].alpha_poly_len = 25;
    srf_caches[7].alpha_lat_mu = &b4_des_alpha_lat_mu;
    srf_caches[7].alpha_lat_sig = &b4_des_alpha_lat_sig;
    srf_caches[7].alpha_node_mu = &b4_des_alpha_node_mu;
    srf_caches[7].alpha_node_sig = &b4_des_alpha_node_sig;
    srf_caches[7].alpha_c = b4_des_alpha_c;
    // Beam 5 ascend
    srf_caches[8].alpha_poly_len = 25;
    srf_caches[8].alpha_lat_mu = &b5_asc_alpha_lat_mu;
    srf_caches[8].alpha_lat_sig = &b5_asc_alpha_lat_sig;
    srf_caches[8].alpha_node_mu = &b5_asc_alpha_node_mu;
    srf_caches[8].alpha_node_sig = &b5_asc_alpha_node_sig;
    srf_caches[8].alpha_c = b5_asc_alpha_c;
    // Beam 5 descend
    srf_caches[9].alpha_poly_len = 25;
    srf_caches[9].alpha_lat_mu = &b5_des_alpha_lat_mu;
    srf_caches[9].alpha_lat_sig = &b5_des_alpha_lat_sig;
    srf_caches[9].alpha_node_mu = &b5_des_alpha_node_mu;
    srf_caches[9].alpha_node_sig = &b5_des_alpha_node_sig;
    srf_caches[9].alpha_c = b5_des_alpha_c;
    // Beam 6 ascend
    srf_caches[10].alpha_poly_len = 25;
    srf_caches[10].alpha_lat_mu = &b6_asc_alpha_lat_mu;
    srf_caches[10].alpha_lat_sig = &b6_asc_alpha_lat_sig;
    srf_caches[10].alpha_node_mu = &b6_asc_alpha_node_mu;
    srf_caches[10].alpha_node_sig = &b6_asc_alpha_node_sig;
    srf_caches[10].alpha_c = b6_asc_alpha_c;
    // Beam 6 descend
    srf_caches[11].alpha_poly_len = 25;
    srf_caches[11].alpha_lat_mu = &b6_des_alpha_lat_mu;
    srf_caches[11].alpha_lat_sig = &b6_des_alpha_lat_sig;
    srf_caches[11].alpha_node_mu = &b6_des_alpha_node_mu;
    srf_caches[11].alpha_node_sig = &b6_des_alpha_node_sig;
    srf_caches[11].alpha_c = b6_des_alpha_c;

    // Shape coefficients
    // Beam 1 ascend
    srf_caches[0].along_alpha_0_poly_len = 9;
    srf_caches[0].cross_alpha_0_poly_len = 9;
    srf_caches[0].along_alpha_2_poly_len = 9;
    srf_caches[0].cross_alpha_2_poly_len = 9;
    srf_caches[0].along_alpha_4_poly_len = 9;
    srf_caches[0].cross_alpha_4_poly_len = 9;
    srf_caches[0].lat_mu = &b1_asc_lat_mu;
    srf_caches[0].lat_sig = &b1_asc_lat_sig;
    srf_caches[0].node_mu = &b1_asc_node_mu;
    srf_caches[0].node_sig = &b1_asc_node_sig;
    srf_caches[0].along_alpha_0_c = b1_asc_along_alpha_0_c;
    srf_caches[0].along_alpha_2_c = b1_asc_along_alpha_2_c;
    srf_caches[0].along_alpha_4_c = b1_asc_along_alpha_4_c;
    srf_caches[0].cross_alpha_0_c = b1_asc_cross_alpha_0_c;
    srf_caches[0].cross_alpha_2_c = b1_asc_cross_alpha_2_c;
    srf_caches[0].cross_alpha_4_c = b1_asc_cross_alpha_4_c;
    // Beam 1 descend
    srf_caches[1].along_alpha_0_poly_len = 9;
    srf_caches[1].cross_alpha_0_poly_len = 9;
    srf_caches[1].along_alpha_2_poly_len = 9;
    srf_caches[1].cross_alpha_2_poly_len = 9;
    srf_caches[1].along_alpha_4_poly_len = 9;
    srf_caches[1].cross_alpha_4_poly_len = 9;
    srf_caches[1].lat_mu = &b1_des_lat_mu;
    srf_caches[1].lat_sig = &b1_des_lat_sig;
    srf_caches[1].node_mu = &b1_des_node_mu;
    srf_caches[1].node_sig = &b1_des_node_sig;
    srf_caches[1].along_alpha_0_c = b1_des_along_alpha_0_c;
    srf_caches[1].along_alpha_2_c = b1_des_along_alpha_2_c;
    srf_caches[1].along_alpha_4_c = b1_des_along_alpha_4_c;
    srf_caches[1].cross_alpha_0_c = b1_des_cross_alpha_0_c;
    srf_caches[1].cross_alpha_2_c = b1_des_cross_alpha_2_c;
    srf_caches[1].cross_alpha_4_c = b1_des_cross_alpha_4_c;
    // Beam 2 ascend
    srf_caches[2].along_alpha_0_poly_len = 9;
    srf_caches[2].cross_alpha_0_poly_len = 9;
    srf_caches[2].along_alpha_2_poly_len = 9;
    srf_caches[2].cross_alpha_2_poly_len = 9;
    srf_caches[2].along_alpha_4_poly_len = 9;
    srf_caches[2].cross_alpha_4_poly_len = 9;
    srf_caches[2].lat_mu = &b2_asc_lat_mu;
    srf_caches[2].lat_sig = &b2_asc_lat_sig;
    srf_caches[2].node_mu = &b2_asc_node_mu;
    srf_caches[2].node_sig = &b2_asc_node_sig;
    srf_caches[2].along_alpha_0_c = b2_asc_along_alpha_0_c;
    srf_caches[2].along_alpha_2_c = b2_asc_along_alpha_2_c;
    srf_caches[2].along_alpha_4_c = b2_asc_along_alpha_4_c;
    srf_caches[2].cross_alpha_0_c = b2_asc_cross_alpha_0_c;
    srf_caches[2].cross_alpha_2_c = b2_asc_cross_alpha_2_c;
    srf_caches[2].cross_alpha_4_c = b2_asc_cross_alpha_4_c;
    // Beam 2 descend
    srf_caches[3].along_alpha_0_poly_len = 9;
    srf_caches[3].cross_alpha_0_poly_len = 9;
    srf_caches[3].along_alpha_2_poly_len = 9;
    srf_caches[3].cross_alpha_2_poly_len = 9;
    srf_caches[3].along_alpha_4_poly_len = 9;
    srf_caches[3].cross_alpha_4_poly_len = 9;
    srf_caches[3].lat_mu = &b2_des_lat_mu;
    srf_caches[3].lat_sig = &b2_des_lat_sig;
    srf_caches[3].node_mu = &b2_des_node_mu;
    srf_caches[3].node_sig = &b2_des_node_sig;
    srf_caches[3].along_alpha_0_c = b2_des_along_alpha_0_c;
    srf_caches[3].along_alpha_2_c = b2_des_along_alpha_2_c;
    srf_caches[3].along_alpha_4_c = b2_des_along_alpha_4_c;
    srf_caches[3].cross_alpha_0_c = b2_des_cross_alpha_0_c;
    srf_caches[3].cross_alpha_2_c = b2_des_cross_alpha_2_c;
    srf_caches[3].cross_alpha_4_c = b2_des_cross_alpha_4_c;
    // Beam 3 ascend
    srf_caches[4].along_alpha_0_poly_len = 9;
    srf_caches[4].cross_alpha_0_poly_len = 9;
    srf_caches[4].along_alpha_2_poly_len = 9;
    srf_caches[4].cross_alpha_2_poly_len = 9;
    srf_caches[4].along_alpha_4_poly_len = 9;
    srf_caches[4].cross_alpha_4_poly_len = 9;
    srf_caches[4].lat_mu = &b3_asc_lat_mu;
    srf_caches[4].lat_sig = &b3_asc_lat_sig;
    srf_caches[4].node_mu = &b3_asc_node_mu;
    srf_caches[4].node_sig = &b3_asc_node_sig;
    srf_caches[4].along_alpha_0_c = b3_asc_along_alpha_0_c;
    srf_caches[4].along_alpha_2_c = b3_asc_along_alpha_2_c;
    srf_caches[4].along_alpha_4_c = b3_asc_along_alpha_4_c;
    srf_caches[4].cross_alpha_0_c = b3_asc_cross_alpha_0_c;
    srf_caches[4].cross_alpha_2_c = b3_asc_cross_alpha_2_c;
    srf_caches[4].cross_alpha_4_c = b3_asc_cross_alpha_4_c;
    // Beam 3 descend
    srf_caches[5].along_alpha_0_poly_len = 9;
    srf_caches[5].cross_alpha_0_poly_len = 9;
    srf_caches[5].along_alpha_2_poly_len = 9;
    srf_caches[5].cross_alpha_2_poly_len = 9;
    srf_caches[5].along_alpha_4_poly_len = 9;
    srf_caches[5].cross_alpha_4_poly_len = 9;
    srf_caches[5].lat_mu = &b3_des_lat_mu;
    srf_caches[5].lat_sig = &b3_des_lat_sig;
    srf_caches[5].node_mu = &b3_des_node_mu;
    srf_caches[5].node_sig = &b3_des_node_sig;
    srf_caches[5].along_alpha_0_c = b3_des_along_alpha_0_c;
    srf_caches[5].along_alpha_2_c = b3_des_along_alpha_2_c;
    srf_caches[5].along_alpha_4_c = b3_des_along_alpha_4_c;
    srf_caches[5].cross_alpha_0_c = b3_des_cross_alpha_0_c;
    srf_caches[5].cross_alpha_2_c = b3_des_cross_alpha_2_c;
    srf_caches[5].cross_alpha_4_c = b3_des_cross_alpha_4_c;
    // Beam 4 ascend
    srf_caches[6].along_alpha_0_poly_len = 9;
    srf_caches[6].cross_alpha_0_poly_len = 9;
    srf_caches[6].along_alpha_2_poly_len = 9;
    srf_caches[6].cross_alpha_2_poly_len = 9;
    srf_caches[6].along_alpha_4_poly_len = 9;
    srf_caches[6].cross_alpha_4_poly_len = 9;
    srf_caches[6].lat_mu = &b4_asc_lat_mu;
    srf_caches[6].lat_sig = &b4_asc_lat_sig;
    srf_caches[6].node_mu = &b4_asc_node_mu;
    srf_caches[6].node_sig = &b4_asc_node_sig;
    srf_caches[6].along_alpha_0_c = b4_asc_along_alpha_0_c;
    srf_caches[6].along_alpha_2_c = b4_asc_along_alpha_2_c;
    srf_caches[6].along_alpha_4_c = b4_asc_along_alpha_4_c;
    srf_caches[6].cross_alpha_0_c = b4_asc_cross_alpha_0_c;
    srf_caches[6].cross_alpha_2_c = b4_asc_cross_alpha_2_c;
    srf_caches[6].cross_alpha_4_c = b4_asc_cross_alpha_4_c;
    // Beam 4 descend
    srf_caches[7].along_alpha_0_poly_len = 9;
    srf_caches[7].cross_alpha_0_poly_len = 9;
    srf_caches[7].along_alpha_2_poly_len = 9;
    srf_caches[7].cross_alpha_2_poly_len = 9;
    srf_caches[7].along_alpha_4_poly_len = 9;
    srf_caches[7].cross_alpha_4_poly_len = 9;
    srf_caches[7].lat_mu = &b4_des_lat_mu;
    srf_caches[7].lat_sig = &b4_des_lat_sig;
    srf_caches[7].node_mu = &b4_des_node_mu;
    srf_caches[7].node_sig = &b4_des_node_sig;
    srf_caches[7].along_alpha_0_c = b4_des_along_alpha_0_c;
    srf_caches[7].along_alpha_2_c = b4_des_along_alpha_2_c;
    srf_caches[7].along_alpha_4_c = b4_des_along_alpha_4_c;
    srf_caches[7].cross_alpha_0_c = b4_des_cross_alpha_0_c;
    srf_caches[7].cross_alpha_2_c = b4_des_cross_alpha_2_c;
    srf_caches[7].cross_alpha_4_c = b4_des_cross_alpha_4_c;
    // Beam 5 ascend
    srf_caches[8].along_alpha_0_poly_len = 9;
    srf_caches[8].cross_alpha_0_poly_len = 9;
    srf_caches[8].along_alpha_2_poly_len = 9;
    srf_caches[8].cross_alpha_2_poly_len = 9;
    srf_caches[8].along_alpha_4_poly_len = 9;
    srf_caches[8].cross_alpha_4_poly_len = 9;
    srf_caches[8].lat_mu = &b5_asc_lat_mu;
    srf_caches[8].lat_sig = &b5_asc_lat_sig;
    srf_caches[8].node_mu = &b5_asc_node_mu;
    srf_caches[8].node_sig = &b5_asc_node_sig;
    srf_caches[8].along_alpha_0_c = b5_asc_along_alpha_0_c;
    srf_caches[8].along_alpha_2_c = b5_asc_along_alpha_2_c;
    srf_caches[8].along_alpha_4_c = b5_asc_along_alpha_4_c;
    srf_caches[8].cross_alpha_0_c = b5_asc_cross_alpha_0_c;
    srf_caches[8].cross_alpha_2_c = b5_asc_cross_alpha_2_c;
    srf_caches[8].cross_alpha_4_c = b5_asc_cross_alpha_4_c;
    // Beam 5 descend
    srf_caches[9].along_alpha_0_poly_len = 9;
    srf_caches[9].cross_alpha_0_poly_len = 9;
    srf_caches[9].along_alpha_2_poly_len = 9;
    srf_caches[9].cross_alpha_2_poly_len = 9;
    srf_caches[9].along_alpha_4_poly_len = 9;
    srf_caches[9].cross_alpha_4_poly_len = 9;
    srf_caches[9].lat_mu = &b5_des_lat_mu;
    srf_caches[9].lat_sig = &b5_des_lat_sig;
    srf_caches[9].node_mu = &b5_des_node_mu;
    srf_caches[9].node_sig = &b5_des_node_sig;
    srf_caches[9].along_alpha_0_c = b5_des_along_alpha_0_c;
    srf_caches[9].along_alpha_2_c = b5_des_along_alpha_2_c;
    srf_caches[9].along_alpha_4_c = b5_des_along_alpha_4_c;
    srf_caches[9].cross_alpha_0_c = b5_des_cross_alpha_0_c;
    srf_caches[9].cross_alpha_2_c = b5_des_cross_alpha_2_c;
    srf_caches[9].cross_alpha_4_c = b5_des_cross_alpha_4_c;
    // Beam 6 ascend
    srf_caches[10].along_alpha_0_poly_len = 9;
    srf_caches[10].cross_alpha_0_poly_len = 9;
    srf_caches[10].along_alpha_2_poly_len = 9;
    srf_caches[10].cross_alpha_2_poly_len = 9;
    srf_caches[10].along_alpha_4_poly_len = 9;
    srf_caches[10].cross_alpha_4_poly_len = 9;
    srf_caches[10].lat_mu = &b6_asc_lat_mu;
    srf_caches[10].lat_sig = &b6_asc_lat_sig;
    srf_caches[10].node_mu = &b6_asc_node_mu;
    srf_caches[10].node_sig = &b6_asc_node_sig;
    srf_caches[10].along_alpha_0_c = b6_asc_along_alpha_0_c;
    srf_caches[10].along_alpha_2_c = b6_asc_along_alpha_2_c;
    srf_caches[10].along_alpha_4_c = b6_asc_along_alpha_4_c;
    srf_caches[10].cross_alpha_0_c = b6_asc_cross_alpha_0_c;
    srf_caches[10].cross_alpha_2_c = b6_asc_cross_alpha_2_c;
    srf_caches[10].cross_alpha_4_c = b6_asc_cross_alpha_4_c;
    // Beam 6 descend
    srf_caches[11].along_alpha_0_poly_len = 9;
    srf_caches[11].cross_alpha_0_poly_len = 9;
    srf_caches[11].along_alpha_2_poly_len = 9;
    srf_caches[11].cross_alpha_2_poly_len = 9;
    srf_caches[11].along_alpha_4_poly_len = 9;
    srf_caches[11].cross_alpha_4_poly_len = 9;
    srf_caches[11].lat_mu = &b6_des_lat_mu;
    srf_caches[11].lat_sig = &b6_des_lat_sig;
    srf_caches[11].node_mu = &b6_des_node_mu;
    srf_caches[11].node_sig = &b6_des_node_sig;
    srf_caches[11].along_alpha_0_c = b6_des_along_alpha_0_c;
    srf_caches[11].along_alpha_2_c = b6_des_along_alpha_2_c;
    srf_caches[11].along_alpha_4_c = b6_des_along_alpha_4_c;
    srf_caches[11].cross_alpha_0_c = b6_des_cross_alpha_0_c;
    srf_caches[11].cross_alpha_2_c = b6_des_cross_alpha_2_c;
    srf_caches[11].cross_alpha_4_c = b6_des_cross_alpha_4_c;

    return;
}

void srf_done() {
    // Clear the caches and free any memory
    FREE(srf_caches);
    return;
}

void srf_meas(double lat, double lon, double inc, double azi, int beam, int ascend, int node) {
    // Updates the meas_data global srf_dat structure, which is needed elsewhere
    //
    // lat,lon: the location for the current measurement
    // inc: the measurement incidence angle
    // azi: the measurement azimuth angle
    // beam: the beam (1-based indexing)
    // ascend: whether ascending pass (1) or descending pass (0)
    // node: which along-beam node index (1-based, from 33 to 224)

    srf_ll cur;
    int na; // not ascend
    double node_adj, lat_adj;

    // Store inc/swath, etc
    meas_data.beam = beam;
    meas_data.inc = inc;
    meas_data.azi = azi;
    meas_data.ascend = ascend;
    meas_data.node = node;

    // Don't use beam edges (should only be an issue with v11 format L1B data)
    if (beam == 2 || beam == 5) {
        if (node < 33 || node > 224) {
            fprintf(stderr, "*** ERROR: %d is an invalid node\n", node);
            FAIL;
        }
    } else if (beam == 1 || beam == 3 || beam == 4 || beam == 6) {
        if (node < 39 || node > 230) {
            fprintf(stderr, "*** ERROR: %d is an invalid node\n", node);
            FAIL;
        }
    }

    // Store lat/lons
    cur.lat = lat;
    cur.lon = lon;
    meas_data.cur_ll = cur;

    // Define a locally tangent plane at the measurement center. This creates a tanplane struct. Its x and y will be zero, but I'll need its other variables later.
    meas_data.tp = latlon2tanplane(cur, cur);

    // Choose cache
    if (beam < 1 || beam > 6) {
        fprintf(stderr, "*** ERROR: %d is an invalid beam number\n", beam);
        FAIL;
    }
    if (ascend == 1)
        na = 0;
    else if (ascend == 0)
        na = 1;
    else {
        fprintf(stderr, "*** ERROR: %d is an invalid ascend\n", ascend);
        FAIL;
    }
    meas_data.srf_poly = &srf_caches[(beam - 1) * 2 + na];
    //Dprintf(5, "lat/lon: %f %f inc: %f azi: %f beam: %d\n", lat, lon, inc, azi, beam);

    // Find the transformation between the coordinate systems
    find_beam_info();

    // Normalize lat/node for evaluation below
    lat_adj = meas_data.cur_ll.lat;
    lat_adj -= *meas_data.srf_poly->lat_mu;
    lat_adj /= *meas_data.srf_poly->lat_sig;

    node_adj = meas_data.node;
    node_adj -= *meas_data.srf_poly->node_mu;
    node_adj /= *meas_data.srf_poly->node_sig;

    // Also the polynomial cofficients for SRF shape can be computed once,
    // here. Each of them are functions of node and latitude.
    meas_data.alpha_along[0] = eval_surface(node_adj, lat_adj, meas_data.srf_poly->along_alpha_0_c, 2);
    meas_data.alpha_along[1] = 0;
    meas_data.alpha_along[2] = eval_surface(node_adj, lat_adj, meas_data.srf_poly->along_alpha_2_c, 2);
    meas_data.alpha_along[3] = 0;
    meas_data.alpha_along[4] = eval_surface(node_adj, lat_adj, meas_data.srf_poly->along_alpha_4_c, 2);

    meas_data.alpha_cross[0] = eval_surface(node_adj, lat_adj, meas_data.srf_poly->cross_alpha_0_c, 2);
    meas_data.alpha_cross[1] = 0;
    meas_data.alpha_cross[2] = eval_surface(node_adj, lat_adj, meas_data.srf_poly->cross_alpha_2_c, 2);
    meas_data.alpha_cross[3] = 0;
    meas_data.alpha_cross[4] = eval_surface(node_adj, lat_adj, meas_data.srf_poly->cross_alpha_4_c, 2);

    // For normalization, find the SRF value for the measurement center
    meas_data.srf_along_max = NAN;
    meas_data.srf_cross_max = NAN;
    meas_data.srf_along_max = compute_srf_along(0);
    meas_data.srf_cross_max = compute_srf_cross(0);

    // Finally, we get a rough estimate of the 3 dB width in either direction
    meas_data.srf_width = poly_width(meas_data.alpha_along, ARR_LEN(meas_data.alpha_along));
    meas_data.srf_length = poly_width(meas_data.alpha_cross, ARR_LEN(meas_data.alpha_cross));

    return;
}

static void find_beam_info() {
    /*
     * Find transformation info on the beam coordinate system
     *
     * All this does is evaluate theta, the rotation angle from the N/E
     * tanplane coordinates to the discriminator frequency gradient coordinates.
     *
     * Note that the rotation angles are all in radians.
     *
     */

    double node_adj, lat_adj;
    int fit_order = 4; // this is hard-coded, but maybe could be adjustable
    double alpha, beta=0, psi=0;

    // We normalize the node (along-beam index) and latitude before evaluation
    lat_adj = meas_data.cur_ll.lat;
    lat_adj -= *meas_data.srf_poly->alpha_lat_mu;
    lat_adj /= *meas_data.srf_poly->alpha_lat_sig;

    node_adj = meas_data.node;
    node_adj -= *meas_data.srf_poly->alpha_node_mu;
    node_adj /= *meas_data.srf_poly->alpha_node_sig;

    // Note that a fourth order fit is used and that the coefficients are stored in c as:
    // (this is for a second order fit, with x=node, y=lat, z=theta; c=[a00,
    // a01, a02, ..., a10, ...]
    // z(x, y) = a00 + a01 x + a02 x^2 +
    //     a10 y + a11 x*y + a12 x^2 y +
    //     a20 y^2 + a21 x*y^2 + a22 x^2 y^2
    //
    // I evaluate the polynomial using a dot product with the coefficient array. Alternately I could use Horner's method. I use the GSL interface to BLAS.
    //
    alpha = eval_surface(node_adj, lat_adj, meas_data.srf_poly->alpha_c, fit_order);

    // Why these values? I explain in disc_freq_grad.py
    if (meas_data.beam == 1) {
        beta = 45;
        psi = meas_data.azi - 135;
    } else if (meas_data.beam == 2) {
        beta = 90;
        psi = meas_data.azi - 90;
    } else if (meas_data.beam == 3) {
        beta = 135;
        psi = meas_data.azi - 45;
    } else if (meas_data.beam == 4) {
        beta = -45;
        psi = meas_data.azi + 135;
    } else if (meas_data.beam == 5) {
        beta = -90;
        psi = meas_data.azi + 90;
    } else if (meas_data.beam == 6) {
        beta = -135;
        psi = meas_data.azi + 45;
    } else {
        FAIL;
    }
    beta = deg2rad(beta);
    psi = deg2rad(-psi);

    meas_data.theta = alpha + beta + psi;

    return;
}

static double poly_width(double *coef, int c_len) {
    // This is a rough estimate of the width of the polynomial
    //
    // I'm assuming the peak is at x=0 and that it tapers off from there. I'm also assuming that it is symmetric.
    // By "width" I mean the 3 dB half-width. This is a rough estimate because I don't try to find the (shifted) polynomial roots or anything, I just sample it and find the first place that it goes below the 3 dB level.
    //
    // ALSO, note that the polynomial we're working with is already in dB space!

    // Units are in km from measurement center
    int len = 100;
    double x_min = 0;
    double x_max = 40;
    double x_arr[len];
    double y_val;
    double step;
    double width = -1;
    int i;
    double val_max, val_3db;

    step = (x_max - x_min) / (float) (len - 1);

    // Construct the x values
    for (i=0; i<len; i++) {
        // x_arr = arange(len)
        x_arr[i] = i;
        // x_arr *= step
        x_arr[i] *= step;
        // x_arr += x_min
        x_arr[i] += x_min;
    }
    // x_arr[-1] = x_max
    x_arr[len-1] = x_max;

    // We assume max value is at x=0
    val_max = gsl_poly_eval(coef, c_len, x_arr[0]);
    val_3db = val_max - 3;

    // Evaluate the polynomial to find y=poly(x). We start from x=0 (x_min) and
    // go along the array. For each element we compare against our 3 dB value.
    // Once we've crossed it we stop, since there's no need to evaluate the
    // rest of the array.
    for (i=1; i<len; i++) {
        y_val = gsl_poly_eval(coef, c_len, x_arr[i]);
        // printf("y_val: %f 3db value: %f\n", y_val, val_3db);
        if (y_val < val_3db) {
            width = x_arr[i];
            break;
        }
    }
    if (width == -1) {
        fprintf(stderr, "ERROR: Cannot find polynomial width");
        FAIL;
    }
    // printf("width: %f\n", width);

    return width;
}

void srf_latlon_extent(double * const tl_lat, double * const tl_lon, double * const tr_lat, double * const tr_lon,
        double * const br_lat, double * const br_lon, double * const bl_lat, double * const bl_lon, int *stat) {
    /* Finds the lat/lon extent of the SRF
     *
     * Given the srf_dat global structure, returns by reference the lat/lon for
     * the top left, top right, bottom right, and bottom left corners.
     *
     * Stat is a status return variable.
     * 4: all good (4 corners)
     * 0: bad (probably because too close to the poles)
     */
    srf_tp top_left, top_right, bot_right, bot_left;
    srf_ll tl_ll, tr_ll, br_ll, bl_ll;
    double rot_x, rot_y;

    // First, if too close to the poles, return early
    if ( fabs(meas_data.cur_ll.lat) > 89.5 ) {
        *stat = 0;
        return;
    }

    // Copy by value tanplane data so they're initialized to the node center
    top_left = meas_data.tp;
    top_right = meas_data.tp;
    bot_right = meas_data.tp;
    bot_left = meas_data.tp;

    // The SRF length/width were already computed in srf_meas()

    // Coordinates of top/bottom left/right corners. These are (x,y) in
    // discriminator coords.
    top_left.x -= meas_data.srf_width;
    bot_left.x -= meas_data.srf_width;
    top_right.x += meas_data.srf_width;
    bot_right.x += meas_data.srf_width;

    top_left.y += meas_data.srf_length;
    bot_left.y -= meas_data.srf_length;
    top_right.y += meas_data.srf_length;
    bot_right.y -= meas_data.srf_length;

    // Rotate from disc freq to N/E
    rotate_coords(top_left.x, top_left.y, meas_data.theta, &rot_x, &rot_y);
    top_left.x = rot_x;
    top_left.y = rot_y;
    rotate_coords(top_right.x, top_right.y, meas_data.theta, &rot_x, &rot_y);
    top_right.x = rot_x;
    top_right.y = rot_y;
    rotate_coords(bot_left.x, bot_left.y, meas_data.theta, &rot_x, &rot_y);
    bot_left.x = rot_x;
    bot_left.y = rot_y;
    rotate_coords(bot_right.x, bot_right.y, meas_data.theta, &rot_x, &rot_y);
    bot_right.x = rot_x;
    bot_right.y = rot_y;

    // Convert to lat/lon
    tl_ll = tanplane2latlon(top_left);
    tr_ll = tanplane2latlon(top_right);
    bl_ll = tanplane2latlon(bot_left);
    br_ll = tanplane2latlon(bot_right);

    // Return the results
    *tl_lat = tl_ll.lat;
    *tl_lon = tl_ll.lon;
    *tr_lat = tr_ll.lat;
    *tr_lon = tr_ll.lon;
    *bl_lat = bl_ll.lat;
    *bl_lon = bl_ll.lon;
    *br_lat = br_ll.lat;
    *br_lon = br_ll.lon;
    *stat = 4;
    return;
}

static double compute_srf_along(double x) {
    /* Computes the SRF in the along-disc. freq. direction at a given location x
     *
     * x is in units km from measurement center
     *
     * Uses the GNU Scientific Library (GSL) for polynomial evaluation
     *
     * Note that the polynomial evaluation is a two-step process
     *
     */

    double val;

    // If we're too far out, then assume we're zero
    // (this is more of a problem for the along direction than cross direction)
    // if (fabs(x) > 2.5 * meas_data.srf_width)
    //     return 0;

    // int i;
    // for (i=0; i<cache->shape_poly_b_len; i++)
    //     printf(" alpha_0: beta[%d]=%f\n", i, cache->shape_along_a0_b_coef[i]);
    // printf("alpha[0] = %f\n", alpha[0]);
    // printf("alpha[2] = %f\n", alpha[2]);
    // printf("alpha[4] = %f\n", alpha[4]);

    // Polynomial evaluation
    val = gsl_poly_eval(meas_data.alpha_along, ARR_LEN(meas_data.alpha_along), x);
    // printf("val = %f\n", val);

    // If we're in a good range, convert the dB value to linear
    // (technically I want the upper limit to be 0, but I allow a little bit of
    // error due to rounding and such)
    if (val > -15 && val <= 0.5) {
        val = db2pow(val);
        // Normalize, if available
        if (!isnan(meas_data.srf_along_max))
            val /= meas_data.srf_along_max;
        return val;
    } else
        return 0;
}

static double compute_srf_cross(double y) {
    /* Computes the SRF in the cross-disc. freq. direction at a given location y
     *
     * y is in units km from measurement center
     *
     * Uses the GNU Scientific Library (GSL) for polynomial evaluation
     *
     * Note that the polynomial evaluation is a two-step process
     *
     */

    double val;

    // // If we're too far out, then assume we're zero
    // if (fabs(y) > 3 * meas_data.srf_length)
    //     return 0;

    // int i;
    // for (i=0; i<cache->shape_poly_b_len; i++)
    //     printf(" alpha_0: beta[%d]=%f\n", i, cache->shape_cross_a0_b_coef[i]);
    // int i;
    // for (i=0; i<sizeof(alpha)/sizeof(*alpha); i++)
    //     printf(" alpha[%d]: %f\n", i, alpha[i]);

    // Polynomial evaluation
    val = gsl_poly_eval(meas_data.alpha_cross, ARR_LEN(meas_data.alpha_cross), y);
    // printf("polyval at %f=%f\n", y, val);

    // If we're in a good range, convert the dB value to linear
    // (technically I want the upper limit to be 0, but I allow a little bit of
    // error due to rounding and such)
    if (val > -15 && val <= 0.5) {
        val = db2pow(val);
        // Normalize, if available
        if (!isnan(meas_data.srf_cross_max))
            val /= meas_data.srf_cross_max;
        return val;
    } else
        return 0;
}

double srf_eval_ll(double lat, double lon) {
    /* For the given location, return the SRF value
     *
     * Note that srf_meas() needs to have been called first, and
     * srf_latlon_extent() can be used to find how far out to go
     *
     * */
    srf_tp p_tp;

    // Translate lat/lon to tanplane (y,x = northing,easting)
    // printf("srf_at: lat=%f lon=%f\n", lat, lon);
    srf_ll p_ll = {.lat = lat, .lon = lon};
    p_tp = latlon2tanplane(meas_data.cur_ll, p_ll);
    // printf("srf_at: p_tp.x=%f p_tp.y=%f\n", p_tp.x, p_tp.y);

    return srf_eval_xy(p_tp.x, p_tp.y);
}

double srf_eval_xy(double x, double y) {
    /* For the given location, return the SRF value
     *
     * Note that srf_meas() needs to have been called first, and
     * srf_latlon_extent() can be used to find how far out to go
     *
     * */

    double along_srf, cross_srf;
    double x_rot, y_rot;

    // Rotate to discriminator frequency gradient (x, y = along-di, cross-di)
    rotate_coords(x, y, -meas_data.theta, &x_rot, &y_rot);
    // printf("srf_at: (tanplane) x=%f y=%f (disc freq) x=%f y=%f\n", x, y, x_rot, y_rot);

    // Find along-di SRF value:
    // printf("srf_at: along_srf_x=%f km\n", x);
    along_srf = compute_srf_along(x_rot);
    // printf("srf_at: along_srf=%f\n", along_srf);

    // Find cross-di SRF value:
    // printf("srf_at: cross_srf_y=%f km\n", y);
    cross_srf = compute_srf_cross(y_rot);
    // printf("srf_at: cross_srf=%f\n", cross_srf);

    // Clip negative values
    cross_srf = (cross_srf > 0 ? cross_srf : 0);
    along_srf = (along_srf > 0 ? along_srf : 0);

    // DEBUG
    // printf("SRF peak value: %f along * %f cross = %f; SRF value at coord: %f along * %f cross = %f\n",
    //     compute_srf_along(0), compute_srf_cross(0),
    //     compute_srf_along(0) * compute_srf_cross(0),
    //     along_srf, cross_srf, along_srf * cross_srf);

    // Return the net SRF
    return along_srf * cross_srf;
}

// vim:softtabstop=4:shiftwidth=4:

