/* (c) copyright 2013-2015 Richard D. Lindsley and David G. Long, Brigham Young University */
/*****************************************************************
Filename:  ascat_srf.h

This is a library meant to return values from the ASCAT spatial response function (SRF)

 ******************************************************************/
#pragma once

/********************************************************************/
// Documentation
//
// This is compiled as a library (static or shared) and called to compute and
// return values from the ASCAT SRF
//
// It provides the public functions:
//
// srf_init(): Prepares the interpolating functions
// srf_meas(): Given the measurement info, initialize data for the next two functions
// srf_latlon_extent(): Determine the extent of the measurement SRF in terms of lat/lon
// srf_eval_ll(): For a given lat/lon, compute the SRF at that location
// srf_eval_xy(): For a given x/y position on the tangent plane, compute the SRF at that location
// srf_done(): Frees the interpolating cache
//
// Some private functions exist behind the scenes.
// Also some structs are used to contain all of the relevant data together.
//
// Typical usage is to call srf_init() once, then call srf_meas() and
// srf_latlon_extent() once for each measurement. Call srf_eval_ll() as
// often as needed for each measurement. Finally, call srf_done() at the
// end to clean things up.
//
// NOTE that this library is *not* thread-safe since it relies on a few static
// global variables. This was a compromise for easier Fortran interoperability,
// but with some minor modifications, it could be thread-safe.
//
/********************************************************************/

/********************************************************************/
// Function prototypes

void srf_init();
void srf_done();
void srf_meas(double lat, double lon, double inc, double azi, int swath, int ascend, int node);
void srf_latlon_extent(double * const tl_lat, double * const tl_lon,
        double * const tr_lat, double * const tr_lon,
        double * const br_lat, double * const br_lon,
        double * const bl_lat, double * const bl_lon, int *stat);
double srf_eval_ll(double lat, double lon);
double srf_eval_xy(double x, double y);

// vim:softtabstop=4:shiftwidth=4:

