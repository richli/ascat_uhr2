{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Wind retrieval using GMF inversion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook tries out some ASCAT wind retrieval by inverting the GMF. It's a brute-force optimization where for a measurement geometry (azimuth and incidence angles), all possible wind speeds/directions are input to the GMF to find the corresponding modeled normalized radar cross-section, or $\\sigma^\\circ$.\n",
    "\n",
    "All possible modeled $\\sigma^\\circ$ values are compared to the observed $\\sigma^\\circ$. Least-squares is used to choose the most likely wind speed/direction based on minimizing the L2 norm of the $\\sigma^\\circ$ residuals. In other words:\n",
    "\n",
    "\\begin{equation}\n",
    "\\hat{s}, \\hat{d} = {\\arg \\min}_{s,d} \\sum_{i=1}^3 [ \\sigma^\\circ_i - \\mathcal{M}(s, d, \\theta_i, \\phi_i) ]^2\n",
    "\\end{equation}\n",
    "\n",
    "There are typically three look directions available: fore, mid, and aft. The GMF $\\mathcal{M}$ takes as input the wind speed ($s$), wind direction ($d$), incidence angle ($\\theta$) and azimuth angle ($\\phi$)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.colors as mpc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start with some test data taken from ASCAT-A on 2017-08-23 for a pass near Hurricane Harvey. (This is sometime between 3:30 and 5:20 UTC. The L2 file is `ascat_20170823_033600_metopa_56266_eps_o_coa_2401_ovw.l2.nc`. The wind near this location is 6.1 m/s, 290.2 degrees.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sig = np.array([-28.008, -21.262, -23.844])\n",
    "inc = np.array([62.643, 51.245, 62.543])\n",
    "azi = np.array([-145.370, -99.560, -53.637])\n",
    "lon, lat = -89.673, 26.477"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import gmf\n",
    "cmod5n = gmf.cmod5('cmod5n')\n",
    "cmod5 = gmf.cmod5('cmod5')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pow2db(x):\n",
    "    return 10 * np.log10(x.clip(min=1e-5))\n",
    "def db2pow(x):\n",
    "    return 10 ** (x / 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are some plots evaluating the GMF. At each look (fore, mid, aft), the GMF is evaluated at the inc/azi angle for a range of wind speeds and directions to obtain $\\sigma^\\circ$ values. The squared differences between the measured and modeled $\\sigma^\\circ$ values is also shown. And the cumulative residuals for all looks. In this case there are two possible ambiguities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# These are the winds to evaluate the GMF with\n",
    "wspds_tested = np.linspace(0.5, 25, 100)\n",
    "wdirs_tested = np.linspace(-180, 180, 90)\n",
    "gmf_wd, gmf_ws = np.meshgrid(wdirs_tested, wspds_tested, indexing='xy')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def eval_and_plot_gmfs(cmod5n, gmf_ws, gmf_wd, sig, inc, azi, look):\n",
    "    num_looks = len(look)\n",
    "    \n",
    "    sig_m = np.zeros((num_looks, gmf_ws.shape[0], gmf_ws.shape[1]))\n",
    "    \n",
    "    for i in range(num_looks):    \n",
    "        sig_m[i, :, :] = cmod5n.call(gmf_ws, gmf_wd, np.full_like(gmf_ws, inc[i]), np.full_like(gmf_ws, azi[i]))\n",
    "    sig_m_db = pow2db(sig_m)\n",
    "    \n",
    "    # -----------------------------------------------------\n",
    "    fig, ax = plt.subplots(num_looks, 2, sharex=True, sharey=True, figsize=(14, 12))\n",
    "    \n",
    "    for i in range(num_looks):\n",
    "        im = ax[i, 0].pcolormesh(wdirs_tested, wspds_tested, sig_m_db[i, :, :], cmap='magma')\n",
    "        ax[i, 0].contour(wdirs_tested, wspds_tested, sig_m_db[i, :, :])\n",
    "        ax[i, 0].contour(wdirs_tested, wspds_tested, sig_m_db[i, :, :], (sig[i], ), linestyles='--')\n",
    "        cbar = fig.colorbar(im, ax=ax[i, 0])\n",
    "        cbar.set_label(\"dB\")\n",
    "        \n",
    "        # sig_res = (db2pow(sig[i]) - sig_m[i, :, :])**2\n",
    "        sig_res = (sig[i] - sig_m_db[i, :, :])**2\n",
    "        im = ax[i, 1].pcolormesh(wdirs_tested, wspds_tested, sig_res, cmap='magma', norm=mpc.LogNorm())\n",
    "        ax[i, 1].contour(wdirs_tested, wspds_tested, sig_res)\n",
    "        cbar = fig.colorbar(im, ax=ax[i, 1])\n",
    "            \n",
    "    for i in range(num_looks):\n",
    "        ax[i, 0].set_title(f\"Modeled $\\sigma^\\circ$ [dB] ({look[i]})\")\n",
    "        ax[i, 1].set_title(f\"Residual $\\sigma^\\circ$ [dB] ({look[i]})\")\n",
    "        ax[i, 0].set_ylabel(\"Wind speed [m/s]\")\n",
    "    \n",
    "    ax[-1, 0].set_xlabel(\"Wind direction [deg]\")\n",
    "    ax[-1, 1].set_xlabel(\"Wind direction [deg]\")\n",
    "    \n",
    "    # -----------------------------------------------------\n",
    "    fig, ax = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(14, 4))\n",
    "    for i in range(num_looks):\n",
    "        ax[0].contour(wdirs_tested, wspds_tested, sig_m_db[i, :, :], (sig[i], ), \n",
    "                      linestyles='--', colors=f'C{i}', label=look[i])\n",
    "    \n",
    "    sig_res_tot = np.add.reduce([(sig[i] - sig_m_db[i, :, :])**2 for i in range(num_looks)])\n",
    "    im = ax[1].pcolormesh(wdirs_tested, wspds_tested, sig_res_tot, cmap='magma', norm=mpc.LogNorm())\n",
    "    ax[1].contour(wdirs_tested, wspds_tested, sig_res_tot)\n",
    "    cbar = fig.colorbar(im, ax=ax[1])\n",
    "    \n",
    "    # ax[0].legend()\n",
    "    ax[0].set_xlabel(\"Wind direction [deg]\")\n",
    "    ax[1].set_xlabel(\"Wind direction [deg]\")\n",
    "    ax[0].set_ylabel(\"Wind speed [m/s]\")\n",
    "    ax[0].set_title(\"Measured $\\sigma^\\circ$\")\n",
    "    ax[1].set_title(\"MLE value\")\n",
    "    \n",
    "    min_loc = np.argmin(sig_res_tot)\n",
    "    min_loc_xy = np.unravel_index(min_loc, sig_res_tot.shape)\n",
    "    print(f\"Minimum at: {min_loc_xy}\")\n",
    "    print(f\"Wind speed: {gmf_ws[min_loc_xy]:0.4f} m/s\")\n",
    "    print(f\"Wind direction: {gmf_wd[min_loc_xy]:0.2f} deg\")\n",
    "    \n",
    "    # -----------------------------------------------------\n",
    "    fig, ax = plt.subplots(1, 2, sharex=True, figsize=(14, 4))\n",
    "    \n",
    "    # How do we find the local minima? The GMF seems to be well behaved when \n",
    "    # wind speed varies but wind direction is held constant. It's when the \n",
    "    # wind direction varies that multiple local minima can occur. Usually just\n",
    "    # two (as in this case), but sometimes 3 or 4 are possible.    \n",
    "    \n",
    "    # mle_sorter = np.argmin(sig_res_tot, axis=0)\n",
    "    inds = [np.argmin(sig_res_tot, axis=0), np.arange(len(wdirs_tested))]\n",
    "    mle_1 = sig_res_tot[inds]\n",
    "    wspd_1 = gmf_ws[inds]\n",
    "    wdir_1 = gmf_wd[inds]\n",
    "    \n",
    "    # A local minimum (in the 1D sense, which is what we now have) is defined as\n",
    "    # the value when an element is less than its neighboring elements. Before \n",
    "    # finding this, prepend and append values to wrap along the direction domain.\n",
    "    mle_1_wrap = np.concatenate([mle_1[-1, None], mle_1, mle_1[0, None]])\n",
    "    local_mask = (mle_1_wrap[1:-1] < mle_1_wrap[0:-2]) & (mle_1_wrap[1:-1] < mle_1_wrap[2:])\n",
    "    \n",
    "    amb_mle = mle_1[local_mask]\n",
    "    amb_wspd = wspd_1[local_mask]\n",
    "    amb_wdir = wdir_1[local_mask]\n",
    "\n",
    "    ax[0].plot(wdir_1, wspd_1, marker='.')\n",
    "    ax[1].plot(wdir_1, mle_1, marker='.')\n",
    "    \n",
    "    ax[0].scatter(amb_wdir, amb_wspd, 500, marker='*', color='C2')\n",
    "    ax[1].scatter(amb_wdir, amb_mle, 500, marker='*', color='C2')\n",
    "    \n",
    "    ax[0].set_xlabel(\"Wind direction [deg]\")\n",
    "    ax[1].set_xlabel(\"Wind direction [deg]\")\n",
    "    ax[0].set_ylabel(\"Wind speed [m/s]\")\n",
    "    ax[1].set_ylabel(\"MLE term\")\n",
    "    \n",
    "    for m, s, d in zip(amb_mle, amb_wspd, amb_wdir):\n",
    "        print(f\"MLE: {m:0.4f}, wspd: {s:0.4f}, wdir: {d:0.2f}\")\n",
    "\n",
    "    # return (sig_res_tot, gmf_ws, gmf_wd)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "look = [\"fore\", \"mid\", \"aft\"]\n",
    "eval_and_plot_gmfs(cmod5n, gmf_ws, gmf_wd, sig, inc, azi, look)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "look = [\"fore\", \"mid\", \"aft\"]\n",
    "eval_and_plot_gmfs(cmod5, gmf_ws, gmf_wd, sig, inc, azi, look)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eval_and_plot_gmfs(cmod5n, gmf_ws, gmf_wd, \n",
    "                   sig=np.array([-7.368466854095459, -12.92438793182373]), \n",
    "                   inc=np.array([23.869998931884766, 32.190093994140625]), \n",
    "                   azi=np.array([-101.97999572753906, -57.015525817871094]), \n",
    "                   look=[\"mid\", \"aft\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
